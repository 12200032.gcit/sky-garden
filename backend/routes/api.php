<?php

use App\Http\Controllers\AnnouncementController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\AuthController;
use App\Http\Controllers\EventsController;
use App\Http\Controllers\TableController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\FoodItemsController;
use GuzzleHttp\Middleware;
use App\Http\Controllers\DiscountController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\VideoController;
use App\Models\FoodItems;

Route::middleware('auth:sanctum')->group(function () {
    // Protected routes...
    Route::get('/user', function (Request $request) {
        return $request->user();
    });
});

Route::post('/login', [AuthController::class, 'login']);
Route::post('/register', [AuthController::class, 'register']);
Route::get('/get-admin-profile', [UserController::class, 'adminProfile']);
Route::put('/update-profile-setting', [UserController::class, 'updateSetting']);
Route::post('/send-otp', [UserController::class, 'sendOtp']);
Route::post('/verify-otp', [UserController::class, 'verifyOtp']);
Route::put('/set-new-password', [UserController::class, 'setNewPassword']);

// manager
Route::get('/get-all-manager', [UserController::class, 'getAllManager']);
Route::delete('/delete-manager/{id}', [UserController::class, 'deleteManager']);
Route::post('/add-manager', [UserController::class, 'addManager']);

Route::post('/logout', [AuthController::class, 'logout']);
Route::post('/refresh', [AuthController::class, 'refresh']);
Route::post('/add-events', [EventsController::class, 'addEvents']);
Route::get('/get-all-events', [EventsController::class, 'getEvents']);
Route::delete('/delete-events/{id}', [EventsController::class, 'delete_events']);
Route::get('/get-events/{id}', [EventsController::class, 'get_events']);
Route::post('/update-events/{id}', [EventsController::class, 'updateEvents']);

Route::post('/table-request', [TableController::class, 'tableRequest']);
Route::get('/get-pending-data', [TableController::class, 'getPendingRequests']);
Route::get('/get-accepted-data', [TableController::class, 'getAcceptedRequests']);
Route::put('/accept-request/{id}', [TableController::class, 'acceptRequest']);
Route::delete('/reject-request/{id}', [TableController::class, 'rejectRequest']);
Route::delete('/delete-marked-data', [TableController::class, 'deleteMarkedData']);
Route::delete('/delete-all-data', [TableController::class, 'deleteAcceptedData']);
Route::post('/verify-table-otp', [TableController::class, 'verifyTableOtp']);
Route::post('/send-table-otp', [TableController::class, 'sendTableOtp']);

Route::post('/addcategory', [CategoryController::class, 'addCategory']);
Route::post('/edit_category/{id}', [CategoryController::class, 'editCategory']);
Route::get('/categorylist', [CategoryController::class, 'categoryList']);
Route::get('/dinningIn', [CategoryController::class, 'dinningIn']);
Route::get('/getCategory', [CategoryController::class, 'getCategories']);
Route::get('/get_Category/{id}', [CategoryController::class, 'get_category']);
Route::delete('/deleteCategory/{id}', [CategoryController::class, 'deleteCategories']);

Route::post('/addfooditem', [FoodItemsController::class, 'addFoodItem']);
Route::get('/getfooditem', [FoodItemsController::class, 'getAllFoodItems']);
Route::delete('/deleteFoodItem/{id}', [FoodItemsController::class, 'deleteFoodItem']);
Route::get('/get-food-details/{id}', [FoodItemsController::class, 'get_Food_Details']);
Route::get('/get_food_items/{id}', [FoodItemsController::class, 'get_foodItems']);
Route::post('/edit_food_item/{id}', [FoodItemsController::class, 'editFoodItem']);


//Discount

Route::get('/getdiscount', [DiscountController::class, 'getAllDiscount']);
Route::patch('/update-discounts', [DiscountController::class, 'updateDiscounts']);
Route::get('/get-discounts-offer', [DiscountController::class, 'getAllDiscountOffered']);
Route::put('/delete-offer/{id}', [DiscountController::class, 'deleteOffer']);
Route::put('/delete-all-marked-offer', [DiscountController::class, 'deleteMarkedOffer']);
Route::put('/delete-all-offer', [DiscountController::class, 'deleteAllOffer']);
Route::get('/get_details/{id}', [DiscountController::class, 'getDetails']);
//

Route::get('/search/{key}', [FoodItemsController::class, 'search']);

// video
Route::post('/upload-special-video', [VideoController::class, 'uploadSpecialVideo']);
Route::post('/upload-promotional-video', [VideoController::class, 'uploadPromotionalVideo']);
Route::get('/get-videos', [VideoController::class, 'getVideos']);

// announcement
Route::get('/get-announcements', [AnnouncementController::class, 'getAnnouncement']);
Route::post('/store-announcements', [AnnouncementController::class, 'storeAnnouncement']);
Route::put('/update-announcements/{id}/status', [AnnouncementController::class, 'updateStatus']);
Route::delete('/delete-announcement/{id}', [AnnouncementController::class, 'deleteAnnouncement']);
Route::get('/getActiveAnnouncement', [AnnouncementController::class, 'getActiveAnnouncement']);

