import React, { useState, useRef } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCloudArrowUp, faArrowLeft } from '@fortawesome/free-solid-svg-icons';
import SideBar from './SideBar';
import "./css/Form.css";
import { Link } from 'react-router-dom';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Topbar from '../Topbar';

function AddEventOffer() {
  const [eventTitle, setEventTitle] = useState('');
  const [date, setDate] = useState('');
  const [time, setTime] = useState('');
  const [description, setDescription] = useState('');
  const [previewURL, setPreviewURL] = useState(null);
  const [loading, setLoading] = useState(false);
  const fileInputRef = useRef(null);

  const getTodayDate = () => {
    const now = new Date();
    const year = now.getFullYear();
    const month = String(now.getMonth() + 1).padStart(2, '0');
    const day = String(now.getDate()).padStart(2, '0');
    return `${year}-${month}-${day}`;
  };

  const getCurrentTime = () => {
    const now = new Date();
    return now.toTimeString().slice(0, 5);
  };

  const today = getTodayDate();
  const currentTime = getCurrentTime();

  const handleFileChange = (event) => {
    const file = event.target.files[0];
    previewFile(file);
  };

  const previewFile = (file) => {
    if (file) {
      const filePreviewURL = URL.createObjectURL(file);
      setPreviewURL(filePreviewURL);
    } else {
      setPreviewURL(null);
    }
  };

  const handleDrop = (event) => {
    event.preventDefault();
    const file = event.dataTransfer.files[0];
    previewFile(file);
  };

  const handleReset = () => {
    setEventTitle('');
    setDate('');
    setTime('00:00');
    setDescription('');
    setPreviewURL(null);
    fileInputRef.current.value = '';
  };

  const handleAddEvent = async (e) => {
    e.preventDefault();
    setLoading(true);
    try {
      if (!eventTitle || !date || !time || !description) {
        throw new Error('Please fill in all fields');
      }

      const file = fileInputRef.current.files[0];
      if (!file) {
        throw new Error('Please select an image');
      }
      if (!file.type.startsWith('image/')) {
        throw new Error('Please select an image file');
      }

      const formData = new FormData();
      formData.append('title', eventTitle);
      formData.append('date', date);
      formData.append('time', time);
      formData.append('description', description);
      formData.append('image', file);

      const response = await fetch('https://sky-garden.ugyen1234.serv00.net/api/add-events', {
        method: 'POST',
        body: formData
      });

      if (!response.ok) {
        throw new Error('Failed to add event');
      }

      toast.success("Announcement added successfully!", {
        autoClose: 3000,
      });

      // Empty all the fields
      handleReset();

    } catch (error) {
      toast.error(error.message, {
        autoClose: 3000,
      });
    } finally {
      setLoading(false);
    }
  };

  return (
    <div className='d-flex'>
      <SideBar />
      <div className='p-4' style={{ width: '93%' }}>
        <Topbar />
        <div className='border title_bar'>
          <p>Add announcement</p>
        </div>
        <form className='form' onSubmit={handleAddEvent}>
          <div className="row">
            <div className="col-md-8">
              <div className='d-flex'>
                <div className='col-md-12' >
                  <label htmlFor="eventTitle">Event Title <span style={{ color: "red" }}>*</span></label>
                  <input
                    type="text"
                    autoFocus
                    id="eventTitle"
                    value={eventTitle}
                    placeholder='Enter title of the events'
                    onChange={(e) => setEventTitle(e.target.value)}
                    className="form-control"
                    required
                  />
                </div>

              </div>
              <div className='d-flex col-md-12 pt-3'>

                <div className='col-md-6' style={{ paddingRight: "15px" }}>
                  <label htmlFor="date">Date <span style={{ color: "red" }}>*</span></label>
                  <input
                    type='date'
                    id="date"
                    value={date}
                    onChange={(e) => setDate(e.target.value)}
                    className="form-control"
                    required
                    min={today}
                  />
                </div>
                <div className='col-md-6' style={{ paddingLeft: "15px" }}>
                  <label htmlFor="time">Time <span style={{ color: "red" }}>*</span></label>
                  <input
                    type="time"
                    id="time"
                    value={time}
                    onChange={(e) => setTime(e.target.value)}
                    className="form-control"
                    required
                    min={date === today ? currentTime : "00:00"}
                  />
                </div>
              </div>
              <label htmlFor="description">Description <span style={{ color: "red" }}>*</span></label>
              <textarea
                id="description"
                value={description}
                placeholder='Write the description of the event'
                onChange={(e) => setDescription(e.target.value)}
                className="form-control"
                required
              ></textarea>
            </div>
            <div className="col-md-4" style={{ paddingLeft: "70px", paddingBlock: "20px" }}>
              <label htmlFor="imageUpload">Upload Image <span style={{ color: "red" }}>*</span></label>
              <div
                className="upload-area "
                onDrop={handleDrop}
                style={{ cursor: "pointer" }}
                onDragOver={(event) => event.preventDefault()}
                onClick={() => fileInputRef.current.click()}
              >
                {previewURL ? (
                  <img src={previewURL} alt="Preview" className="preview-image" />
                ) : (
                  <div style={{ textAlign: "center" }}>
                    <p className='upload-text'><FontAwesomeIcon icon={faCloudArrowUp} className='upload-icon' /><br />Drag and drop or click here to select image</p>
                  </div>
                )}
                <input
                  type="file"
                  id="imageUpload"
                  ref={fileInputRef}
                  accept="image/*"
                  onChange={handleFileChange}
                  style={{ display: 'none' }}
                />
              </div>
            </div>
          </div>
          <div className="row mt-4">
            <div className="col-md-9">
              <Link to="/events-and-anouncement" style={{ textDecorationLine: "none" }}>
                <span className='go-back'>
                  <FontAwesomeIcon className='icon' icon={faArrowLeft} />
                  Go back
                </span>
              </Link>
            </div>
            <div className="col-md-3 d-flex justify-content-between">
              <button className="btn reset" onClick={handleReset}>Reset</button>
              <button className="btn add" type='submit' disabled={loading}>
                {loading ? 'Adding...' : 'Add event'}
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
}

export default AddEventOffer;
