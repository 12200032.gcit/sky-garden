import React, { useState, useEffect } from "react";
import SideBar from "../SideBar";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faDeleteLeft, faQuestion } from "@fortawesome/free-solid-svg-icons";

import "../css/Table.css";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import Topbar from "../../Topbar";

function DiscountTable() {

  const [foodCategory, setFoodCategory] = useState([]);
  const [totalCategories, setTotalCategories] = useState([]);
  const [pendingRequestsData, setPendingRequestsData] = useState([]);
  const [discountOfferedData, setDiscountOfferedData] = useState([]);
  const [loading, setLoading] = useState(false);
  const [selectedData, setSelectedData] = useState(null);

  useEffect(() => {
    fetchData();
    getData();
    fetchDataOffered();
  }, []);

  async function fetchData() {
    try {
      let response = await fetch("https://sky-garden.ugyen1234.serv00.net/api/getdiscount");
      if (!response.ok) {
        throw new Error('Failed to fetch data');
      }
      let result = await response.json();
      setPendingRequestsData(result);
    } catch (error) {
      console.error('Error fetching data:', error);
    }
  }

  async function fetchDataOffered() {
    try {
      let response = await fetch("https://sky-garden.ugyen1234.serv00.net/api/get-discounts-offer");
      if (!response.ok) {
        throw new Error('Failed to fetch data');
      }
      let result = await response.json();
      setDiscountOfferedData(result);
    } catch (error) {
      console.error('Error fetching data:', error);
    }
  }

  async function getData() {
    try {
      let response = await fetch("https://sky-garden.ugyen1234.serv00.net/api/getCategory");
      if (!response.ok) {
        throw new Error('Failed to fetch data');
      }
      let result = await response.json();
      setTotalCategories(result);
      if (result.length > 0) {
        setFoodCategory(result[0]);
      }
    } catch (error) {
      console.error('Error fetching data:', error);
      // Handle error, show error message or retry fetching data
    }
  }

  const offerDiscount = async () => {
    setLoading(true);

    try {
      const response = await fetch('https://sky-garden.ugyen1234.serv00.net/api/update-discounts', {
        method: 'PATCH',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ data: pendingRequestsData }),
      });

      if (!response.ok) {
        throw new Error("Failed to offer discount");
      }

      if (response.ok) {
        toast.success(`${discount}% discount offer applied successfully to ${selectedItemsHome.length} food items!`, {
          autoClose: 3000,
        });
        handleClose();
        fetchData();
        setSelectedItemsHome([]);
        setDiscount('');
        fetchDataOffered();
      }
    } catch (error) {
      toast.error("Failed to offer discount")
      handleClose();
    }
    finally {
      setLoading(false);
    }
  };

  const [selectAllHome, setSelectAllHome] = useState(false);
  const [selectedItemsHome, setSelectedItemsHome] = useState([]);
  const [selectAllProfile, setSelectAllProfile] = useState(false);
  const [selectedItemsProfile, setSelectedItemsProfile] = useState([]);

  const [showDeleteAllMarkModal, setShowDeleteAllMarkModal] = useState(false);
  const [showDeleteAllModal, setShowDeleteAllModal] = useState(false);
  const [showDeleteModal, setShowDeleteModal] = useState(false);
  const [showOfferModal, setShowOfferModal] = useState(false);

  const [discount, setDiscount] = useState('');

  const handleClose = () => {
    setShowOfferModal(false);
    setShowDeleteAllMarkModal(false);
    setShowDeleteAllModal(false);
    setShowDeleteModal(false);
    setShowOfferModal(false);
  };

  const handleDeleteAllMarkShow = () => setShowDeleteAllMarkModal(true);
  const handleDeleteAllShow = () => setShowDeleteAllModal(true);
  const handleDeleteShow = () => setShowDeleteModal(true);
  const handleOfferShow = () => setShowOfferModal(true);

  const toggleSelectAllHome = () => {
    const newValue = !selectAllHome;
    setSelectAllHome(newValue);
    if (newValue) {
      // Get the selected category
      const selectedCategoryItems = pendingRequestsData.filter(request => request.food_category === foodCategory);
      // Update the discount for all items in the selected category
      const updatedData = pendingRequestsData.map((item) => {
        if (selectedCategoryItems.some(selectedItem => selectedItem.id === item.id)) {
          // Apply the discount
          const afterDiscount = Math.round((item.actual_price - (item.actual_price * parseInt(discount)) / 100) * 100) / 100;
          return { ...item, discount: parseInt(discount), after_discount: afterDiscount };
        }
        return item;
      });
      setPendingRequestsData(updatedData);
      // Update selectedItemsHome to include all items in the selected category
      setSelectedItemsHome(selectedCategoryItems.map(item => item.id));
    } else {
      // If "Mark All" checkbox is unchecked, reset the discount for all items
      const updatedData = pendingRequestsData.map((item) => {
        return { ...item, discount: 0, after_discount: item.actual_price }; // Reset the discount and after_discount
      });
      setPendingRequestsData(updatedData);
      setSelectedItemsHome([]); // Clear selected items
    }
  };


  const toggleSelectAllProfile = () => {
    const newValue = !selectAllProfile;
    setSelectAllProfile(newValue);
    if (newValue) {
      setSelectedItemsProfile(discountOfferedData.map((request) => request.id));
    } else {
      setSelectedItemsProfile([]);
    }
  };

  const handleCheckboxChangeHome = (id) => {
    const index = selectedItemsHome.indexOf(id);
    if (index === -1) {
      setSelectedItemsHome([...selectedItemsHome, id]);
      // Apply the discount percent only if the checkbox is checked
      updateDiscountValues(id, discount);
    } else {
      setSelectedItemsHome(selectedItemsHome.filter((itemId) => itemId !== id));
      // Reset the discount percent and set after_discount to actual_price when unchecked
      updateDiscountValues(id, 0, pendingRequestsData.find((item) => item.id === id).actual_price);
    }
  };
  const handleCheckboxChangeProfile = (id) => {
    const index = selectedItemsProfile.indexOf(id);
    if (index === -1) {
      setSelectedItemsProfile([...selectedItemsProfile, id]);
    } else {
      setSelectedItemsProfile(selectedItemsProfile.filter((itemId) => itemId !== id));
    }
  };

  const isCheckedHome = (id) => {
    return selectedItemsHome.includes(id);
  };

  const isCheckedProfile = (id) => {
    return selectedItemsProfile.includes(id);
  };

  const handleResetHome = () => {
    setSelectedItemsHome([]);
    setSelectAllHome(false);
  };

  const handleDiscountChange = (e) => {
    const input = e.target.value;
    // Regular expression to allow only numeric characters
    const numericInput = input.replace(/\D/g, ''); // Remove non-numeric characters
    setDiscount(numericInput);

    // Update DiscountPercent and afterDiscount for selected items if checkbox is checked
    const updatedData = pendingRequestsData.map((item) => {
      if (isCheckedHome(item.id)) {
        const discountPercent = parseInt(numericInput);
        const actualPrice = item.actual_price;
        const afterDiscount = Math.round(actualPrice - (actualPrice * discountPercent) / 100);

        return { ...item, discount: discountPercent, after_discount: afterDiscount };
      }
      return item;
    });
    setPendingRequestsData(updatedData);
  };

  const updateDiscountValues = (itemId, newDiscount) => {
    const updatedData = pendingRequestsData.map((item) => {
      if (item.id === itemId) {
        const discountPercent = Number.isNaN(parseInt(newDiscount)) ? item.DiscountPercent : parseInt(newDiscount);
        const actualPrice = item.actual_price;
        const afterDiscount = Math.round(actualPrice - (actualPrice * discountPercent) / 100);
        return { ...item, discount: discountPercent, after_discount: afterDiscount };
      }
      return item;
    });
    setPendingRequestsData(updatedData);
  };

  const handleButtonClick = () => {
    if (!discount || discount === "0") {
      toast.error('Please enter a valid discount.');
    }
    else if (!selectedItemsHome.length) {
      toast.error('Please select atleast one food items');
    } else {
      handleOfferShow();
    }
  };

  const deleteOffer = async () => {
    setLoading(true);

    try {
      const response = await fetch(`https://sky-garden.ugyen1234.serv00.net/api/delete-offer/${selectedData}`, {
        method: "put",
      });

      if (!response.ok) {
        throw new Error("Failed to reject request");
      }

      const data = await response.json();
      if (response.ok) {
        toast.success(data.message);
        handleClose();
        fetchDataOffered();
        fetchData();
      }
    } catch (error) {
      toast.error('Failed to remove offer', {
        autoClose: 3000,
      });
      handleClose();
    }
    finally {
      setLoading(false);
    }
  };

  const deleteAllMarked = async () => {
    setLoading(true);
    try {
      const response = await fetch('https://sky-garden.ugyen1234.serv00.net/api/delete-all-marked-offer', {
        method: 'put',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ ids: selectedItemsProfile })
      });
      const data = await response.json();
      if (!response.ok) {
        throw new Error('Failed to remove offer');
      }
      if (response.ok) {
        toast.success(data.message, {
          autoClose: 3000,
        });
        handleClose();
        fetchDataOffered();
        fetchData();
      }
    } catch (error) {
      toast.error('Failed to remove offer', {
        autoClose: 3000,
      });
      handleClose();
    }
    finally {
      setLoading(false);
    }
  };


  const deleteAll = async () => {
    setLoading(true);
    try {
      const response = await fetch('https://sky-garden.ugyen1234.serv00.net/api/delete-all-offer', {
        method: 'put',
      });
      if (!response.ok) {
        throw new Error('Failed to remove offer');
      }
      const data = await response.json();
      if (response.ok) {
        toast.success(data.message, {
          autoClose: 3000,
        });
        handleClose();
        fetchDataOffered();
        fetchData();
      }
    } catch (error) {
      toast.error('Failed to remove offer', {
        autoClose: 3000,
      });
      handleClose();
    }
    finally {
      setLoading(false);
    }
  };

  return (
    <div className="d-flex">
      <SideBar />
      <div className="p-4" style={{ width: "93%" }}>
        <Topbar />
        <div className="title_bar">
          <p>Offer Discount</p>
        </div>
        <div>
          <nav>
            <div class="nav nav-tabs" id="nav-tab" role="tablist">
              <button class="nav-link active" id="nav-home-tab" data-bs-toggle="tab" data-bs-target="#nav-home" type="button" role="tab" aria-controls="nav-home" aria-selected="true">Offer Discount</button>
              <button class="nav-link" id="nav-profile-tab" data-bs-toggle="tab" data-bs-target="#nav-profile" type="button" role="tab" aria-controls="nav-profile" aria-selected="false">Discount Offered Items</button>
            </div>
          </nav>

          <div className="tab-content p-3" id="nav-tabContent">
            <div className="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">


              <div style={{ marginTop: "20px" }}>

                <div className="row">
                  <div className='d-flex'>


                    <div className='col-md-6' style={{ paddingRight: "15px", position: "relative" }}>
                      <label htmlFor="category" style={{ fontWeight: "600", color: "#555555" }}>Category</label>
                      <div style={{ position: "relative" }}>
                        <select
                          id="category"
                          value={foodCategory}
                          onChange={(e) => setFoodCategory(e.target.value)}
                          className="form-control"
                          required
                          style={{ marginTop: "20px" }}
                        >
                          <option value="" style={{ fontWeight: "600", color: "#7E7E84" }}>Select Category</option>
                          {totalCategories.map((category, index) => (
                            <option key={index} value={category}>{category}</option>
                          ))}



                        </select>
                        <div style={{ position: "absolute", top: "50%", right: "10px", transform: "translateY(-50%)" }}>
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="16"
                            height="16"
                            fill="currentColor"
                            className="bi bi-chevron-down"
                            viewBox="0 0 16 16"
                          >
                            <path d="M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 1 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z" />
                          </svg>
                        </div>
                      </div>
                    </div>

                    <div className='col-md-6' style={{ paddingRight: "15px" }}>
                      <label htmlFor="discount" style={{ fontWeight: "600", color: "#555555" }}>Discount (in percent %)</label>
                      <input
                        type="text"
                        id="discount"
                        placeholder='Enter the discount in percent'
                        value={discount}
                        onChange={handleDiscountChange}
                        className="form-control"
                        required
                        style={{ marginTop: "20px" }}
                      />
                    </div>

                  </div>
                </div>
              </div>
              <table className="pending-requests-table" style={{ marginTop: "30px" }}>
                <thead>
                  <tr>
                    <th>ID</th>
                    <th className="d-flex">
                      <label htmlFor="checkAllHome" >  Food Item </label>
                      <input type="checkbox" id="checkAllHome" onChange={toggleSelectAllHome} style={{ width: "20px", height: "20px", marginLeft: "10px" }} />
                    </th>
                    <th>Discount (%)</th>
                    <th>Actual Price</th>
                    <th>After Discount</th>
                  </tr>
                </thead>
                <tbody>
                  {pendingRequestsData
                    .filter(request => request.food_category === foodCategory && !request.offer_status) // Filter menu items based on selected category and offerStatus being false
                    .map((request, index) => ( // Adding index parameter to map function
                      <tr key={request.id}>
                        <td>{index + 1}</td> {/* Displaying index plus 1 as slno */}
                        <td className="d-flex">
                          <input type="checkbox" checked={isCheckedHome(request.id)} onChange={() => handleCheckboxChangeHome(request.id)} style={{ width: "20px", height: "20px", marginRight: "10px" }} />
                          <label>{request.food_name}</label>
                        </td>
                        <td>{request.discount}%</td>
                        <td>{request.actual_price}</td>
                        <td>{isCheckedHome(request.id) ? request.after_discount : request.actual_price}</td> {/* Display either after_discount or actual_price based on checkbox status */}
                      </tr>
                    ))}

                </tbody>


              </table>

              <div className="pending-requests-table p-4 d-flex justify-content-end">
                <button onClick={handleResetHome}>Reset</button>
                <button onClick={handleButtonClick} >Offer</button>
              </div>
            </div>



            <div className="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
              <table className="pending-requests-table">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Category</th>
                    <th>Menu Item</th>
                    <th>Discount (%)</th>
                    <th>Actual Price</th>
                    <th>After Discount</th>
                    <th>
                      Mark All
                      <input type="checkbox" id="checkAllProfile" onChange={toggleSelectAllProfile} style={{ width: "20px", height: "20px", marginLeft: "10px" }} />
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {discountOfferedData.map((request,index) => (
                    <tr key={request.id}>
                      <td>{index+1}</td>
                      <td>{request.food_category}</td>
                      <td>{request.food_name}</td>
                      <td>{request.discount}%</td>
                      <td>{request.actual_price}</td>
                      <td>{request.after_discount}</td>
                      <td>
                        <input type="checkbox" checked={isCheckedProfile(request.id)} onChange={() => handleCheckboxChangeProfile(request.id)} style={{ width: "20px", height: "20px", marginLeft: "10px" }} />
                        <FontAwesomeIcon
                          style={{ cursor: "pointer", fontSize: "20px", marginLeft: "20px", color: "rgba(255, 0, 0, 1)" }}
                          icon={faDeleteLeft}
                          onClick={() => { handleDeleteShow(request.id); setSelectedData(request.id); }}
                        />
                      </td>

                    </tr>
                  ))}
                </tbody>
              </table>
              <div className="pending-requests-table p-4 d-flex justify-content-end">
                <button onClick={handleDeleteAllMarkShow}>Delete all mark</button>
                <button onClick={handleDeleteAllShow}>Delete All</button>
              </div>
            </div>
          </div>
          <div
            className="d-flex justify-content-center align-items-center"
          >
            <Modal
              show={showOfferModal}
              onHide={handleClose}
              centered
              backdrop="static"
              keyboard={false}
              size="sm"
            >
              <Modal.Body
                className="text-center"
                style={{ paddingInline: "30px", paddingTop: "40px" }}
              >
                <FontAwesomeIcon
                  style={{ color: "rgba(0, 160, 177, 1)", fontSize: "50px" }}
                  className="pb-3"
                  icon={faQuestion}
                />
                <p
                  className="confirm-text">
                  Are you sure you want to offer a {discount}% discount on {selectedItemsHome.length} food items?
                </p>
              </Modal.Body>
              <Modal.Footer
                className="d-flex justify-content-evenly pt-0"
                style={{ border: "none", paddingBottom: "25px" }}
              >
                <Button className="model-button" onClick={handleClose}>
                  Cancel
                </Button>
                <Button className="model-button" onClick={offerDiscount} disabled={loading}>
                  {loading ? 'Offering...' : 'Offer'}
                </Button>
              </Modal.Footer>
            </Modal>
          </div>
          <div
            className="d-flex justify-content-center align-items-center"
          >
            <Modal
              show={showDeleteModal}
              onHide={handleClose}
              centered
              backdrop="static"
              keyboard={false}
              size="sm"
            >
              <Modal.Body
                className="text-center"
                style={{ paddingInline: "30px", paddingTop: "40px" }}
              >
                <FontAwesomeIcon
                  style={{ color: "rgba(0, 160, 177, 1)", fontSize: "50px" }}
                  className="pb-3"
                  icon={faQuestion}
                />
                <p
                  className="confirm-text"                >
                  Are you sure you want to remove this offer?
                </p>
              </Modal.Body>
              <Modal.Footer
                className="d-flex justify-content-evenly pt-0"
                style={{ border: "none", paddingBottom: "25px" }}
              >
                <Button className="model-button" onClick={handleClose}>
                  Cancel
                </Button>
                <Button className="model-button" onClick={deleteOffer} disabled={loading}>
                  {loading ? 'Removing...' : 'Remove'}
                </Button>
              </Modal.Footer>
            </Modal>
          </div>
          <div
            className="d-flex justify-content-center align-items-center"
          >
            <Modal
              show={showDeleteAllMarkModal}
              onHide={handleClose}
              centered
              backdrop="static"
              keyboard={false}
              size="sm"
            >
              <Modal.Body
                className="text-center"
                style={{ paddingInline: "30px", paddingTop: "40px" }}
              >
                <FontAwesomeIcon
                  style={{ color: "rgba(0, 160, 177, 1)", fontSize: "50px" }}
                  className="pb-3"
                  icon={faQuestion}
                />
                <p
                  className="confirm-text"                >
                  Are you certain you wish to remove all selected offers?
                </p>
              </Modal.Body>
              <Modal.Footer
                className="d-flex justify-content-evenly pt-0"
                style={{ border: "none", paddingBottom: "25px" }}
              >
                <Button className="model-button" onClick={handleClose}>
                  Cancel
                </Button>
                <Button className="model-button" onClick={deleteAllMarked} disabled={loading}>
                  {loading ? 'Removing...' : 'Remove'}
                </Button>
              </Modal.Footer>
            </Modal>
          </div>
          <div className="d-flex justify-content-center align-items-center">
            <Modal show={showDeleteAllModal} onHide={handleClose} centered backdrop="static" keyboard={false} size="sm">
              <Modal.Body className="text-center" style={{ paddingInline: "30px", paddingTop: "40px" }}>
                <FontAwesomeIcon style={{ color: "rgba(0, 160, 177, 1)", fontSize: "50px" }} className="pb-3" icon={faQuestion} />
                <p className="confirm-text">
                  Are you sure you want to remove all the offer?
                </p>
              </Modal.Body>
              <Modal.Footer className="d-flex justify-content-evenly pt-0" style={{ border: "none", paddingBottom: "25px" }}>
                <Button className="model-button" onClick={handleClose}>
                  Cancel
                </Button>
                <Button className="model-button" onClick={deleteAll} disabled={loading}>
                  {loading ? 'Removing...' : 'Remove'}
                </Button>
              </Modal.Footer>
            </Modal>
          </div>
        </div>
      </div>
    </div>
  );
}

export default DiscountTable;
