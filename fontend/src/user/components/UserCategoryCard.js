import React from 'react';
// import "./styles/category.css"
import { Link} from 'react-router-dom';
import 'primereact/resources/primereact.min.css';
import "primereact/resources/themes/lara-light-cyan/theme.css";
import { useState} from 'react';
import "react-toastify/dist/ReactToastify.css";

const UserCategoriesCard = (props) => {
  const { category, active, onClick, image, id } = props;
  const truncatedCategory = category.length > 10 ? category.slice(0, 7) + "..." : category;
  const [data, setData] = useState([]);

  async function getData() {
    try {
      let response = await fetch("https://sky-garden.ugyen1234.serv00.net/api/getfooditem");
      if (!response.ok) {
        throw new Error('Failed to fetch data');
      }
      let result = await response.json();
      setData(result);
    } catch (error) {
      console.error('Error fetching data:', error);
    }
  }

  return (
    <div
      className={`icon-container-main ${active ? 'active' : ''}`}
      onClick={() => onClick(category)}
    >
      <div className="card categoryCard ">
        <div className='icon-container'>
          {/* <img className="card-img-top icon-img" src={require(`./image/${category}.png`)} alt={category} /> */}
          <img className="card-img-top icon-img" src={"https://sky-garden.ugyen1234.serv00.net/storage/"+ image} />
          
        </div>
        <div className="card-body-content p-0">
          <p className="card-text  ">{truncatedCategory}</p>
        </div>
        {active && <Link to="/menu"><button className="edit-button">View All</button></Link>}

      </div>
    </div>
  );
};
export default UserCategoriesCard;
