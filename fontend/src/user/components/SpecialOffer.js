
import Card from 'react-bootstrap/Card';
import "../styles/specialOffer.css";


function SpecialOffer() {
    return (
        <div className='special-offer-main'>
            <div className='special-offer-text'><p>We also offer unique services for your events</p></div>
            <div className='d-flex'>
                <Card style={{ width: '18rem', border: "none", marginTop: "5vw" }}>
                    <Card.Img variant="top" src="/img/Birthday.png" />
                    <Card.Body>
                        <Card.Title className='special-title'>Birthday</Card.Title>
                        <Card.Text className='special-text'>
                            Make Your Birthday Dreams a Reality! Tell us how we can customize your celebration to perfection.
                        </Card.Text>
                    </Card.Body>
                </Card>


                <Card style={{ width: '18rem', border: "none", marginTop: "5vw", marginLeft: "1vw" }}>
                    <Card.Img variant="top" src="/img/Events.png" />
                    <Card.Body>
                        <Card.Title className='special-title'>Events</Card.Title>
                        <Card.Text className='special-text'>
                            From concept to celebration, we orchestrate every detail. Let's craft an event that exceeds your expectations.
                        </Card.Text>
                    </Card.Body>
                </Card>


                <Card style={{ width: '18rem', border: "none", marginTop: "5vw", marginLeft: "1vw" }}>
                    <Card.Img variant="top" src="/img/wedding.png" />
                    <Card.Body>
                        <Card.Title className='special-title'>Wedding</Card.Title>
                        <Card.Text className='special-text'>
                            Turning Your Wedding Vision into Reality! Share your dreams, and let us weave them into an unforgettable celebration of love.
                        </Card.Text>
                    </Card.Body>
                </Card>

                <Card style={{ width: '18rem', border: "none", marginTop: "5vw", marginLeft: "1vw" }}>
                    <Card.Img variant="top" src="/img/Cattering.png"/>
                    <Card.Body>
                        <Card.Title className='special-title'>Catering</Card.Title>
                        <Card.Text className='special-text'>
                            Savor the Moment! Share your culinary desires, and let us create a delectable experience tailored just for you.
                        </Card.Text>
                    </Card.Body>
                </Card>
            </div>


        </div>
    );
}

export default SpecialOffer;