import React from "react";
import "../styles/nav.css";
import Navbar from "./Navbar";
import MobileNavbar from "./MoblieNavbar";
import Marquee from "./SlidingWord";
import MenuMark from "./MenuMark";
import MenuHome from "./MenuHome";
import Whatsapp from "./WhatsAppIcon";


const ContainerMenu = () => {
  return (
    <div>
      <Marquee />
      <Navbar />
      <MobileNavbar />
      <div style={{ position: "fixed", top: "40vw", right: "7vw", zIndex: "50" }}>
        <Whatsapp />
      </div>
      <MenuMark/>
      <MenuHome/>
    </div>
  );
};
export default ContainerMenu;
