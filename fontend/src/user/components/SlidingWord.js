import React, { useRef, useEffect, useState } from "react";
import { gsap } from "gsap";
import "../styles/Marquee.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faVolumeHigh } from "@fortawesome/free-solid-svg-icons";

const Marquee = () => {
    const [marqueeTexts, setMarqueeTexts] = useState([]);
    const marqueeContainer = useRef(null);
    const [screenWidth, setScreenWidth] = useState(window.innerWidth);
    const marqueeTween = useRef(null);

    useEffect(() => {
        resizeHandler();
        window.addEventListener("resize", resizeHandler);
        return () => {
            window.removeEventListener("resize", resizeHandler);
            marqueeTween.current && marqueeTween.current.pause().kill();
        };
    }, []);

    useEffect(() => {
        if (marqueeContainer.current) {
            const textWidth = marqueeContainer.current.scrollWidth;
            const containerWidth = marqueeContainer.current.offsetWidth;
            const duration = textWidth / containerWidth * 10;

            marqueeTween.current = gsap.fromTo(marqueeContainer.current, 
                { x: containerWidth }, 
                {
                    x: -textWidth,
                    ease: "none",
                    duration: duration,
                    repeat: -1,
                    onComplete: () => {
                        marqueeTween.current.restart();
                    }
                }
            );
        }
    }, [screenWidth, marqueeTexts]);

    const resizeHandler = () => {
        setScreenWidth(window.innerWidth);
    };

    const getData = async () => {
        try {
            let response = await fetch("https://sky-garden.ugyen1234.serv00.net/api/getActiveAnnouncement");
            if (!response.ok) {
                throw new Error('Failed to fetch data');
            }
            let result = await response.json();
            setMarqueeTexts(result);
        } catch (error) {
            console.error('Error fetching data');
        }
    }

    useEffect(() => {
        getData();
    }, []);

    return (
        <div className="d-flex">
            <div className="mic-icon">
                <span className="label">ANNOUNCEMENTS</span>
                <FontAwesomeIcon icon={faVolumeHigh} style={{ fontSize: "24px", color: "white", marginTop: "-5px" }} />
            </div>

            <div className="relative w-screen py-2 bg-green-600 text-gray-200 flex overflow-hidden items-center scrolling-word">
                <div ref={marqueeContainer} style={{ whiteSpace: "nowrap", display: 'inline-flex' }}>
                    {marqueeTexts.map((text) => (
                        <span key={text.id} className="marquee-item" style={{ padding: '0 2rem' }}>
                            {text.announcement}
                        </span>
                    ))}
                </div>
            </div>
        </div>
    );
};

export default Marquee;
