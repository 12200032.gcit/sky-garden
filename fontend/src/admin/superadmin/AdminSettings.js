import React, { useState, useEffect } from 'react';
import SideBar from './AdminSidebar';
import "../manager/css/Form.css";
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Topbar from '../Topbar';

function AdminSetting() {
  const [name, setName] = useState('');
  const [contact, setContact] = useState('');
  const [email, setEmail] = useState('');
  const [oldPassword, setOldPassword] = useState('');
  const [newPassword, setNewPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    // Fetch initial values from localStorage
    try {
      const user = JSON.parse(localStorage.getItem('user'));
      if (user) {
        setName(user.name);
        setContact(user.contact);
        setEmail(user.email);
      }
    } catch (error) {
      toast.error('Error fetching user data from localStorage', {
        autoClose: 3000,
      });
    }
  }, []);


  const handleReset = () => {
    setName(name);
    setContact(contact);
    setEmail(email);
    setOldPassword('');
    setNewPassword('');
    setConfirmPassword('');
  };

  const handleUpdateProfile = async (e) => {
    e.preventDefault();
    setLoading(true);
    try {

      if (!email || !oldPassword || !newPassword || !confirmPassword) {
        throw new Error('Please fill in all required fields');
      }
      if (newPassword.length < 6 || !/[a-zA-Z]/.test(newPassword) || !/[0-9]/.test(newPassword)) {
        throw new Error('New password must be at least 6 characters long and contain both letters and numbers');
      }

      if (newPassword !== confirmPassword) {
        throw new Error('New password and confirm password do not match');
      }

      if (!/^(17|77)\d{6}$/.test(contact)) {
        throw new Error('Contact number must be 8 digits and start with 17 or 77');
      }
      const response = await fetch(`https://sky-garden.ugyen1234.serv00.net/api/update-profile-setting`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          name,
          contact,
          email,
          old_password: oldPassword,
          new_password: newPassword,
        }),
      });

      const data = await response.json();
      if (response.status === 401) {
        throw new Error('Incorrect old password. Please enter the correct password');
      }
      if (response.ok) {
        localStorage.setItem('user', JSON.stringify(data.user));
        toast.success("Profile updated successfully")
        handleReset();
      }
    } catch (error) {
      toast.error(error.message);
    }
    finally {
      setLoading(false);
    }
  };

  return (
    <div className='d-flex'>
      <SideBar />
      <div className='p-4' style={{ width: "93%" }}>
        <Topbar />
        <div className='border title_bar'>
          <p>Setting</p>
        </div>
        <form onSubmit={handleUpdateProfile}>
          <div className="form">
            <div className="row">
              <div className="col-md-12">
                <div className='d-flex'>
                  <div className='col-md-6' style={{ paddingRight: "15px" }}>
                    <label htmlFor="name">Name</label>
                    <input
                      type="text"
                      id="name"
                      placeholder='Enter name'
                      value={name}
                      onChange={(e) => setName(e.target.value)}
                      className="form-control"
                    />
                  </div>
                  <div className='col-md-6' style={{ paddingLeft: "15px" }}>
                    <label htmlFor="contact">Contact</label>
                    <input
                      type="text"
                      id="contact"
                      placeholder='Enter contact'
                      value={contact}
                      onChange={(e) => setContact(e.target.value)}
                      className="form-control"
                    />
                  </div>
                </div>
                <div className='d-flex'>
                  <div className='col-md-6' style={{ paddingRight: "15px" }}>
                    <label htmlFor="email">Email <span style={{ color: "red" }}>*</span></label>
                    <input
                      type="email"
                      id="email"
                      placeholder='Enter email'
                      value={email}
                      onChange={(e) => setEmail(e.target.value)}
                      className="form-control"
                      required
                      readOnly
                    />
                  </div>
                  <div className='col-md-6' style={{ paddingLeft: "15px" }}>
                    <label htmlFor="oldPassword">Old Password <span style={{ color: "red" }}>*</span></label>
                    <input
                      type="password"
                      id="oldPassword"
                      placeholder='Enter old password'
                      value={oldPassword}
                      onChange={(e) => setOldPassword(e.target.value)}
                      className="form-control"
                      autoFocus
                      required
                    />
                  </div>
                </div>
                <div className='d-flex'>
                  <div className='col-md-6' style={{ paddingRight: "15px" }}>
                    <label htmlFor="newPassword">New Password <span style={{ color: "red" }}>*</span></label>
                    <input
                      type="password"
                      id="newPassword"
                      placeholder='Enter new password'
                      value={newPassword}
                      onChange={(e) => setNewPassword(e.target.value)}
                      className="form-control"
                      required
                    />
                  </div>
                  <div className='col-md-6' style={{ paddingLeft: "15px" }}>
                    <label htmlFor="confirmPassword">Confirm Password <span style={{ color: "red" }}>*</span></label>
                    <input
                      type="password"
                      id="confirmPassword"
                      placeholder='Enter confirm password'
                      value={confirmPassword}
                      onChange={(e) => setConfirmPassword(e.target.value)}
                      className="form-control"
                      required
                    />
                  </div>
                </div>
              </div>
            </div>
            <div className="row mt-4">
              <div className="col-md-9"></div>
              <div className="col-md-3 d-flex justify-content-between">
                <button className="btn reset" type="button" onClick={handleReset}>Reset</button>
                <button className="btn add" type="submit" disabled={loading}>{loading ? 'Saving...' : 'Save'}</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
}

export default AdminSetting;
