import React, { useState, useEffect } from 'react';
import ClearIcon from '@mui/icons-material/Clear'; // Import Clear icon from Material-UI Icons
import "./css/Video.css";
import SideBar from './SideBar';
import Topbar from "../Topbar";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faQuestion } from "@fortawesome/free-solid-svg-icons";
import "./css/Table.css"

const Video = () => {
  const [selectedFile1, setSelectedFile1] = useState(null);
  const [videoUrl1, setVideoUrl1] = useState(null);
  const [selectedFile2, setSelectedFile2] = useState(null);
  const [videoUrl2, setVideoUrl2] = useState(null);
  const [loading, setLoading] = useState(false);
  const [showPromotionalModal, setShowPromotionalModal] = useState(false);
  const [showSpecialModal, setShowSpecialModal] = useState(false);
  const [specialVideoUrl, setSpecialVideoUrl] = useState(null);
  const [promotionalVideoUrl, setPromotionalVideoUrl] = useState(null);


  const handleClose = () => {
    setShowPromotionalModal(false);
    setShowSpecialModal(false);
  };

  const handlePromotionalShow = () => setShowPromotionalModal(true);
  const handleSpecialShow = () => setShowSpecialModal(true);

  const handleFileChange1 = (event) => {
    const file = event.target.files[0];
    setSelectedFile1(file);
    setVideoUrl1(URL.createObjectURL(file));
  };

  const handleFileChange2 = (event) => {
    const file = event.target.files[0];
    setSelectedFile2(file);
    setVideoUrl2(URL.createObjectURL(file));
  };

  const handleClearFile1 = () => {
    setSelectedFile1(null);
    setSpecialVideoUrl(null);
    setVideoUrl1(null);
  };

  const handleClearFile2 = () => {
    setSelectedFile2(null);
    setPromotionalVideoUrl(null);
    setVideoUrl2(null);
  };

  useEffect(() => {
    fetchVideos();
  }, []);

  const fetchVideos = async () => {
    try {
      const response = await fetch("https://sky-garden.ugyen1234.serv00.net/api/get-videos");
      if (response.ok) {
        const data = await response.json();
        setSpecialVideoUrl(data.special_video);
        setPromotionalVideoUrl(data.promotion_video);
      } else {
        throw new Error('Failed to fetch videos.');
      }
    } catch (error) {
      console.error('Failed to fetch videos. Please try again.');
    }
  };


  const handleSubmit1 = async () => {
    if (!selectedFile1) {
      toast.error('Please select a video to upload.');
      handleClose();
      return;
    }

    setLoading(true);

    try {
      const formData = new FormData();
      formData.append('video', selectedFile1);

      const response = await fetch("https://sky-garden.ugyen1234.serv00.net/api/upload-special-video", {
        method: 'POST',
        body: formData,
        headers: {
          'Accept': 'application/json',
        },
      });
      if (response.ok) {
        toast.success("Video uploaded successfully!", {
          autoClose: 3000,
        });
        handleClose();
        fetchVideos();
        setSelectedFile1(null);
        setVideoUrl1(null);
      } else {
        throw new Error('Failed to upload video.');
      }
    } catch (error) {
      toast.error('Failed to upload video. Please try again.');
      handleClose();
    } finally {
      setLoading(false);
    }
  };

  const handleSubmit2 = async () => {
    if (!selectedFile2) {
      toast.error('Please select a video to upload.');
      handleClose()
      return;
    }

    setLoading(true);

    try {
      const formData = new FormData();
      formData.append('video', selectedFile2);

      const response = await fetch("https://sky-garden.ugyen1234.serv00.net/api/upload-promotional-video", {
        method: 'POST',
        body: formData,
        headers: {
          'Accept': 'application/json',
        },
      });
      if (response.ok) {
        toast.success('Video uploaded successfully!', {
          autoClose: 3000,
        });
        handleClose();
        fetchVideos();
        setSelectedFile2(null);
        setVideoUrl2(null);
      } else {
        throw new Error('Failed to upload video.');
      }
    } catch (error) {
      toast.error('Failed to upload video. Please try again.');
      handleClose();
    } finally {
      setLoading(false);
    }
  };

  return (
    <div>
      <div className='d-flex'>
        <SideBar />
        <div className='p-4 ' style={{ width: "93%" }}>
          <Topbar />
          <div className='border title_bar'>
            <p>Upload Video</p>
          </div>
          <div className='video-main  p-5 col-lg-12'>
            <p className='title'>Daily Special Video</p>
            <div className='col-lg-12 d-flex'>
              <div className='video-container col-lg-8'>
                {(videoUrl1 || specialVideoUrl) && (
                  <div>
                    <video autoPlay
                      muted
                      controls src={videoUrl1 || `http://localhost:8000${specialVideoUrl}`} style={{ width: '100%', height: '100%' }} />
                    <ClearIcon onClick={handleClearFile1} style={{ position: 'absolute', top: 15, right: 15, cursor: 'pointer', color: 'white', fontSize: 40 }} />
                  </div>
                )}
                {!videoUrl1 && !specialVideoUrl && (
                  <label htmlFor="video-upload1" className="custom-file-upload">
                    <span className='button-text1'>Click here to </span><span className='button-text2'>select daily special Video</span>
                    <input id="video-upload1" type="file" accept="video/*" onChange={handleFileChange1} style={{ display: 'none' }} />
                  </label>
                )}
              </div>
              <div className='col-lg-4 button'>
                <button className="upload-button" onClick={handleSpecialShow}>Upload</button>
              </div>
            </div>
          </div>

          <div className='video-main  p-5 col-lg-12'>
            <p className='title'>Promotional Video</p>
            <div className='col-lg-12 d-flex'>
              <div className='video-container col-lg-8'>
                {(videoUrl2 || promotionalVideoUrl) && (
                  <div>
                    <video controls src={videoUrl2 || `http://localhost:8000${promotionalVideoUrl}`} style={{ width: '100%', height: '100%' }} />
                    <ClearIcon onClick={handleClearFile2} style={{ position: 'absolute', top: 15, right: 15, cursor: 'pointer', color: 'white', fontSize: 40 }} />
                  </div>
                )}
                {!videoUrl2 && !promotionalVideoUrl && (
                  <label htmlFor="video-upload2" className="custom-file-upload">
                    <span className='button-text1'>Click here to </span><span className='button-text2'>select Promotional Video</span>
                    <input id="video-upload2" type="file" accept="video/*" onChange={handleFileChange2} style={{ display: 'none' }} />
                  </label>
                )}
              </div>
              <div className='col-lg-4 button'>
                <button className="upload-button" onClick={handlePromotionalShow}>Upload</button>
              </div>
            </div>
          </div>

          <div
            className="d-flex justify-content-center align-items-center"
          >
            <Modal
              show={showSpecialModal}
              onHide={handleClose}
              centered
              backdrop="static"
              keyboard={false}
              size="sm"
            >
              <Modal.Body
                className="text-center"
                style={{ paddingInline: "30px", paddingTop: "40px" }}
              >
                <FontAwesomeIcon
                  style={{ color: "rgba(0, 160, 177, 1)", fontSize: "50px" }}
                  className="pb-3"
                  icon={faQuestion}
                />
                <p
                  className="confirm-text"                >
                  Are you sure you want to upload this special video? This may take some time!
                </p>
              </Modal.Body>
              <Modal.Footer
                className="d-flex justify-content-evenly pt-0"
                style={{ border: "none", paddingBottom: "25px" }}
              >
                <Button className="model-button" onClick={handleClose}>
                  Cancel
                </Button>
                <Button className="model-button" onClick={handleSubmit1} disabled={loading}>
                  {loading ? 'Uploading...' : 'Upload'}
                </Button>
              </Modal.Footer>
            </Modal>
          </div>
          <div
            className="d-flex justify-content-center align-items-center"
          >
            <Modal
              show={showPromotionalModal}
              onHide={handleClose}
              centered
              backdrop="static"
              keyboard={false}
              size="sm"
            >
              <Modal.Body
                className="text-center"
                style={{ paddingInline: "30px", paddingTop: "40px" }}
              >
                <FontAwesomeIcon
                  style={{ color: "rgba(0, 160, 177, 1)", fontSize: "50px" }}
                  className="pb-3"
                  icon={faQuestion}
                />
                <p
                  className="confirm-text">
                  Are you sure you want to upload this promotional video? This may take some time!
                </p>
              </Modal.Body>
              <Modal.Footer
                className="d-flex justify-content-evenly pt-0"
                style={{ border: "none", paddingBottom: "25px" }}
              >
                <Button className="model-button" onClick={handleClose}>
                  Cancel
                </Button>
                <Button className="model-button" onClick={handleSubmit2} disabled={loading}>
                  {loading ? 'Uploading...' : 'Upload'}
                </Button>
              </Modal.Footer>
            </Modal>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Video;
