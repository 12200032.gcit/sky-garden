import React from 'react';
import { Link, useLocation } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBowlRice, faCalendarCheck, faPersonDotsFromLine, faHandHoldingDollar, faGear } from '@fortawesome/free-solid-svg-icons';
import "./css/SideBar.css";

function SideBar() {
    const location = useLocation();

    // Function to determine if the given path matches the current URL
    const isActive = (path) => {
        return location.pathname === path;
    };
    const eventId = localStorage.getItem('eventId');
    return (
        <div className='sidebar'>
            <ul style={{ listStyle: 'none', paddingInline: "14px" }}>
                <div className='pb-4 pt-3'>
                    <img style={{ width: "75px" }} alt='sky garden' src='/img/logo.jpg'></img>
                </div>
                <Link to="/food" className="sidebar-link">
                    <li className={isActive("/food") || isActive("/food/edit") || isActive("/fooditem/edit") || isActive("/addfoodcategory") || isActive("/addfoodItem") ? "active" : ""}>
                        <FontAwesomeIcon className='icon' icon={faBowlRice} /><br />Food
                    </li>
                </Link>


                <Link to="/events-and-anouncement" className="sidebar-link">
                    <li className={(isActive("/events-and-anouncement") || isActive("/add-event-and-offer") || isActive("/announcement") || isActive(`/edit-event-and-offer/${eventId}`)) ? "active" : ""}>
                        <FontAwesomeIcon className='icon' icon={faCalendarCheck} /><br />E&A
                    </li>
                </Link>
                <Link to="/table" className="sidebar-link">
                    <li className={isActive("/table") ? "active" : ""}>
                        <FontAwesomeIcon className='icon' icon={faPersonDotsFromLine} /><br />Table
                    </li>
                </Link>
                <Link to="/discount" className="sidebar-link">
                    <li className={isActive("/discount") ? "active" : ""}>
                        <FontAwesomeIcon className='icon' icon={faHandHoldingDollar} /><br />Discount
                    </li>
                </Link>
                <Link to="/profile-setting" className="sidebar-link">
                    <li className={isActive("/profile-setting") ? "active" : ""}>
                        <FontAwesomeIcon className='icon' icon={faGear} /><br />Settings
                    </li>
                </Link>
            </ul>
            <div className='video'>
                <p>want to upload a new video?</p>
                <Link to="/video" className="sidebar-link">Click here</Link>
            </div>
        </div>
    )
}

export default SideBar;
