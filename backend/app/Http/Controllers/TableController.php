<?php

namespace App\Http\Controllers;

use App\Mail\AcceptedRequestEmail;
use App\Mail\OtptableEmail;
use App\Mail\RejectedRequestEmail;
use App\Models\Otp;
use App\Models\Tables;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class TableController extends Controller
{
    public function tableRequest(Request $request)
    {
        try {
            $request->validate([
                'name' => 'required|string',
                'email' => 'required|email',
                'date' => 'required|date',
                'people' => 'required|integer',
                'phone' => 'required|string',
                'status' => 'nullable|in:pending,accepted', // Validate status if provided
            ]);

            $table = new Tables;
            $table->name = $request->input('name');
            $table->email = $request->input('email');
            $table->date = $request->input('date');
            $table->num_of_people = $request->input('people');
            $table->contact = $request->input('phone');
            $table->message = $request->input('request');
            $table->status = $request->input('status', 'pending'); // Set default status if not provided
            $table->save();

            return $table;
        } catch (\Exception $e) {
            // Log or handle the error
            return response()->json(['error' => 'Failed to process the request. Please try again.'], 500);
        }
    }


    function getPendingRequests()
    {
        return Tables::where('status', 'pending')->get();
    }

    function getAcceptedRequests()
    {
        return Tables::where('status', 'accepted')->get();
    }

    public function acceptRequest($id)
    {
        try {
            // Find the record by ID
            $table = Tables::findOrFail($id);
            $table->status = 'accepted';
            $table->save();

            Mail::to($table->email)->send(new AcceptedRequestEmail($table));

            return response()->json(['message' => 'Request accepted successfully'], 200);
        } catch (\Exception $e) {
            // Handle exception, e.g., record not found
            return response()->json(['message' => 'Failed to accept request'], 500);
        }
    }

    public function rejectRequest(Request $request, $id)
    {
        try {
            $table = Tables::findOrFail($id);
            // Get the rejection reason from the request
            $reason = $request->input('reason');

            // Send the email notification with the reason (if required)
            Mail::to($table->email)->send(new RejectedRequestEmail($table, $reason));

            $table->delete();
            return response()->json(['message' => 'Request rejected successfully'], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Failed to decline request'], 500);
        }
    }

    public function deleteMarkedData(Request $request)
    {
        $ids = $request->input('ids');

        try {
            $existingIds = Tables::find($ids)->pluck('id')->toArray();

            $nonExistingIds = array_diff($ids, $existingIds);
            if (!empty($nonExistingIds)) {
                return response()->json(['message' => 'Some IDs do not exist'], 404);
            }

            if (!empty($existingIds)) {
                Tables::whereIn('id', $existingIds)->delete();
                return response()->json(['message' => 'All marked data have been deleted successfully'], 200);
            } else {
                return response()->json(['message' => 'No data found to delete'], 200);
            }
        } catch (\Exception $e) {
            return response()->json(['message' => 'Failed to delete data'], 500);
        }
    }

    public function deleteAcceptedData()
    {
        try {
            $deletedCount = Tables::where('status', 'accepted')->delete();
            if ($deletedCount > 0) {
                return response()->json(['message' => 'All data have been deleted successfully.'], 200);
            } else {
                return response()->json(['message' => 'No data found to delete'], 200);
            }
        } catch (\Exception $e) {
            return response()->json(['message' => 'Failed to delete data'], 500);
        }
    }

    public function sendTableOtp(Request $request)
    {


        // Remove expired OTP data
        Otp::where('expires_at', '<', Carbon::now())->delete();

        $email = $request->input('email');
        $name = $request->input('name');
        $otp = mt_rand(100000, 999999);

        // Calculate expiration time
        $expiresAt = Carbon::now()->addMinutes(5);

        // Store OTP in database
        Otp::create([
            'email' => $email,
            'otp' => $otp,
            'expires_at' => $expiresAt,
        ]);

        // Send email with OTP
        Mail::to($email)->send(new OtptableEmail($name, $otp));

        return response()->json(['message' => 'OTP has been sent successfully.']);
    }
    public function verifyTableOtp(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'otp' => 'required|string',
        ]);

        $email = $request->input('email');
        $otp = $request->input('otp');

        // Find OTP record without considering expiration
        $otpRecord = Otp::where('email', $email)
            ->where('otp', $otp)
            ->first();

        if ($otpRecord) {
            // Check if OTP is expired
            if (Carbon::now()->greaterThan($otpRecord->expires_at)) {
                return response()->json(['message' => 'The OTP has expired. Please request a new one.'], 400);
            } else {
                // OTP is valid and not expired, delete the record
                $otpRecord->delete();
                return response()->json(['message' => 'The OTP has been successfully verified.']);
            }
        }

        return response()->json(['message' => 'The OTP is invalid. Please check the code and try again.'], 400);
    }
}
