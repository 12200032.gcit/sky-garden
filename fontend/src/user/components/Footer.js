import React from 'react';
import {
  MDBFooter,
  MDBContainer,
  MDBCol,
  MDBRow,
} from 'mdb-react-ui-kit';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFacebook, faSquareInstagram } from '@fortawesome/free-brands-svg-icons';

import { NavLink } from 'react-router-dom';

import '../styles/footer.css'; // Make sure to import your CSS file

export default function Footer() {
  return (
    <div style={{ marginLeft: "-2vw", marginRight: "-3vw", marginTop: "5vw" }}>
      <MDBFooter className='text-white' style={{ backgroundColor: "#474747" }}>
        <MDBContainer className='p-4'>
          <MDBRow style={{ marginTop: "5vw" }}>
            <MDBCol lg="4" md="10" className='mb-4 mb-md-0'>
              <h5 style={{ marginLeft: "2vw", fontFamily: " 'Playfair Display', serif", fontStyle: "italic", fontWeight: "600", fontSize: "2vw" }}>SkyGarden</h5>
              <p style={{ width: "70%", fontFamily: " 'Playfair Display', serif", color: "#ADB29E", fontSize: "1vw" }}>
                The restaurant is located on a rooftop terrace, providing stunning views of the surrounding area.
              </p>
              <div className='d-flex'>
                <div style={{ marginLeft: "2vw", fontSize: "2vw" }}>
                  <a href='https://www.facebook.com/skygardenbhutan' className='icon-link'>
                    <FontAwesomeIcon icon={faFacebook} />
                  </a>
                </div>
                <div style={{ marginLeft: "2vw", fontSize: "2vw" }}>
                  <a href='https://www.instagram.com/sky_garden_level_6/' className='icon-link'>
                    <FontAwesomeIcon icon={faSquareInstagram} />
                  </a>
                </div>
              </div>
            </MDBCol>

            <MDBCol lg="4" md="6" className='mb-4 mb-md-0'>
              <ul className='list-unstyled'>
                <li>
                  <NavLink to="/nav" className='nav-link'>Home</NavLink>
                </li>
                <li>
                  <NavLink to="/menu" className='nav-link'>Menu</NavLink>
                </li>
                <li>
                  <NavLink to="/event" className='nav-link'>Events</NavLink>
                </li>
                <li>
                  <NavLink to="/book-table" className='nav-link'>Book Table</NavLink>
                </li>
              </ul>
            </MDBCol>

            <MDBCol lg="3" md="6" className='mb-4 mb-md-0'>
              <h5 style={{ fontFamily: " 'Playfair Display', serif", fontSize: "2vw" }}>Gallery</h5>
              <div className='d-flex mt-4'>
                <div style={{ width: "50%", height: "50%" }}>
                  <img style={{ width: "90%", height: "90%" }} src="/img/Footer1.png"  />
                </div>
                <div style={{ width: "50%", height: "50%" }}>
                  <img style={{ width: "90%", height: "90%" }} src="/img/Footer2.png" />
                </div>
              </div>

              <div className='d-flex mt-4'>
                <div style={{ width: "50%", height: "50%" }}>
                  <img style={{ width: "90%", height: "90%" }} src="/img/Footer3.png"/>
                </div>
                <div style={{ width: "50%", height: "50%" }}>
                  <img style={{ width: "90%", height: "90%" }} src="/img/Footer4.png"/>
                </div>
              </div>
            </MDBCol>
          </MDBRow>
        </MDBContainer>

        <div className='text-center p-3' style={{ backgroundColor: 'rgba(0, 0, 0, 0.2)' }}>
          © 2024 Copyright:
          <a className='text-white' href='https://yarkaygroup.com/'>
            yarkaygroup
          </a>
        </div>
      </MDBFooter>
    </div>
  );
}
