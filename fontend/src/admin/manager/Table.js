import React, { useState, useEffect } from "react";
import SideBar from "./SideBar";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faDeleteLeft, faCircleInfo, faQuestion } from "@fortawesome/free-solid-svg-icons";
import "./css/Table.css"
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import Topbar from "../Topbar";

function Table() {

  const [showCircleInfoModal, setShowCircleInfoModal] = useState(false);
  const [selectedData, setSelectedData] = useState(null);
  const [showAcceptModal, setShowAcceptModal] = useState(false);
  const [showRejectModal, setShowRejectModal] = useState(false);
  const [showDeleteAllMarkModal, setShowDeleteAllMarkModal] = useState(false);
  const [showDeleteAllModal, setShowDeleteAllModal] = useState(false);
  const [showDeleteModal, setShowDeleteModal] = useState(false);
  const [pendingData, setPendingData] = useState([]);
  const [acceptedData, setAcceptedData] = useState([]);
  const [functionOption, setFunctionOption] = useState();
  const [rejectionReason, setRejectionReason] = useState('');
  const [loading, setLoading] = useState(false);

  const handleClose = () => {
    setShowCircleInfoModal(false);
    setShowAcceptModal(false);
    setShowRejectModal(false);
    setShowDeleteAllMarkModal(false);
    setShowDeleteAllModal(false);
    setShowDeleteModal(false);
  };

  const handleCircleInfoShow = () => setShowCircleInfoModal(true);
  const handleAcceptShow = () => setShowAcceptModal(true);
  const handleRejectShow = () => setShowRejectModal(true);
  const handleDeleteAllMarkShow = () => setShowDeleteAllMarkModal(true);
  const handleDeleteAllShow = () => setShowDeleteAllModal(true);
  const handleDeleteShow = () => setShowDeleteModal(true);

  useEffect(() => {
    fetchTableBookingRequest();
  }, [pendingData]);
  useEffect(() => {
    fetchAcceptedRequest();
  }, []);

  const fetchTableBookingRequest = async () => {
    try {
      const response = await fetch("https://sky-garden.ugyen1234.serv00.net/api/get-pending-data");
      if (!response.ok) {
        throw new Error("Failed to fetch data");
      }
      const data = await response.json();
      // Format dates and times
      const formattedData = data.map((event) => {
        const date = new Date(event.date).toLocaleDateString("en-US", {
          year: "numeric",
          month: "short",
          day: "numeric",
        });

        return {
          ...event,
          date,
        };
      });
      setPendingData(formattedData);
    } catch (error) {
      console.error("Failed to fetch data");
    }
  };

  const fetchAcceptedRequest = async () => {
    try {
      const response = await fetch("https://sky-garden.ugyen1234.serv00.net/api/get-accepted-data");
      if (!response.ok) {
        throw new Error("Failed to fetch data");
      }
      const data = await response.json();
      // Format dates and times
      const formattedData = data.map((event) => {
        const date = new Date(event.date).toLocaleDateString("en-US", {
          year: "numeric",
          month: "short",
          day: "numeric",
        });

        return {
          ...event,
          date,
        };
      });
      if (response.ok) {
        setAcceptedData(formattedData);
      }

    } catch (error) {
      console.error("Failed to fetch data")
    }
  };


  const acceptRequest = async (e) => {
    e.preventDefault();
    setLoading(true);

    try {
      const response = await fetch(`https://sky-garden.ugyen1234.serv00.net/api/accept-request/${selectedData}`, {
        method: "PUT",
      });

      if (!response.ok) {
        throw new Error("Failed to accept request");
      }

      const data = await response.json();

      if (data.message === 'Request accepted successfully') {
        toast.success("Accepted successfully", {
          autoClose: 3000,
        });
        handleClose();
        fetchTableBookingRequest();
        fetchAcceptedRequest();
      }
    } catch (error) {
      toast.error("Failed to accept request")
      handleClose();
    }
    finally {
      setLoading(false);
    }
  };

  const rejectRequest = async () => {
    // Check if the rejection reason is empty for decline function option
    if ((!rejectionReason.trim()) && (functionOption === "decline")) {
      toast.error("Rejection reason cannot be empty", {
        autoClose: 3000
      });
      return;
    }
    setLoading(true);

    try {
      const response = await fetch(`https://sky-garden.ugyen1234.serv00.net/api/reject-request/${selectedData}`, {
        method: "DELETE", // Use DELETE method
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify({ reason: rejectionReason }) // Sending the rejection reason
      });

      if (!response.ok) {
        throw new Error("Failed to reject request");
      }

      const data = await response.json();
      if ((data.message === 'Request rejected successfully') && (functionOption === "decline")) {
        toast.success("Rejected successfully", {
          autoClose: 3000,
        })
        handleClose();
        fetchAcceptedRequest();
        fetchTableBookingRequest();

      } else if ((data.message === 'Request rejected successfully') && (functionOption === "delete")) {
        toast.success("Deleted successfully.", {
          autoClose: 3000,
        });
        handleClose();
        setSelectedItems([]);
        fetchAcceptedRequest();
      } else {
        toast.error("Failed to decline request", {
          autoClose: 3000,
        });
        handleClose();
      }
    } catch (error) {
      if (functionOption === "decline") {
        toast.error("Failed to decline request", {
          autoClose: 3000,
        });
        handleClose();
      } else {
        toast.error("Failed to delete data", {
          autoClose: 3000,
        });
        handleClose();
      }
    }
    finally {
      setLoading(false);
    }
  };


  const [selectAll, setSelectAll] = useState(false);
  const [selectedItems, setSelectedItems] = useState([]);

  const deleteAllMarked = async () => {
    setLoading(true);
    try {
      const response = await fetch('https://sky-garden.ugyen1234.serv00.net/api/delete-marked-data', {
        method: 'DELETE',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ ids: selectedItems })
      });
      const data = await response.json();
      if (!response.ok) {
        throw new Error('Failed to delete data');
      }
      if (response.ok) {
        toast.success(data.message, {
          autoClose: 3000,
        });
        handleClose();
        setSelectedItems([])
        fetchAcceptedRequest();
      }
    } catch (error) {
      toast.error('Failed to delete data', {
        autoClose: 3000,
      });
      handleClose();
    }
    finally {
      setLoading(false);
    }
  };

  const deleteAll = async () => {
    setLoading(true);
    try {
      const response = await fetch('https://sky-garden.ugyen1234.serv00.net/api/delete-all-data', {
        method: 'DELETE',
      });
      if (!response.ok) {
        throw new Error('Failed to delete data');
      }
      const data = await response.json();
      toast.success(data.message, {
        autoClose: 3000,
      });
      handleClose();
      fetchAcceptedRequest();
    } catch (error) {
      toast.error('Failed to delete data', {
        autoClose: 3000,
      });
      handleClose();
    }
    finally {
      setLoading(false);
    }
  };


  const toggleSelectAll = () => {
    const allIds = acceptedData.map(request => request.id);
    setSelectedItems(!selectAll ? allIds : []);
    setSelectAll(!selectAll);
  };

  const handleCheckboxChange = (id) => {
    const index = selectedItems.indexOf(id);
    if (index === -1) {
      setSelectedItems([...selectedItems, id]);
    } else {
      setSelectedItems(selectedItems.filter((itemId) => itemId !== id));
    }
  };

  const isChecked = (id) => {
    return selectedItems.includes(id);
  };

  return (
    <div className="d-flex">
      <SideBar />
      <div className="p-4" style={{ width: "93%" }}>
        <Topbar />
        <div className="title_bar">
          <p>Table</p>
        </div>
        <div>
          <nav>
            <div class="nav nav-tabs" id="nav-tab" role="tablist">
              <button class="nav-link active" id="nav-home-tab" data-bs-toggle="tab" data-bs-target="#nav-home" type="button" role="tab" aria-controls="nav-home" aria-selected="true">Pending Requests</button>
              <button class="nav-link" id="nav-profile-tab" data-bs-toggle="tab" data-bs-target="#nav-profile" type="button" role="tab" aria-controls="nav-profile" aria-selected="false">Accepted Requests</button>
            </div>
          </nav>
          <div className="tab-content p-3" id="nav-tabContent">
            <div className="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
              <table className="pending-requests-table">
                <thead>
                  <tr>
                    <th>Sl.no</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Date</th>
                    <th>No. of People</th>
                    <th>Contact</th>
                    <th style={{ textAlign: "center" }}>Message</th>
                    <th style={{ textAlign: "center" }}>Action</th>
                  </tr>
                </thead>
                <tbody>
                  {pendingData.slice().sort((a, b) => new Date(a.date) - new Date(b.date)).map((request, index) => (
                    <tr key={request.id}>
                      <td>{index + 1}</td> {/* Serial number starts from 1 */}
                      <td>{request.name}</td>
                      <td>{request.email}</td>
                      <td>{request.date}</td>
                      <td>{request.num_of_people}</td>
                      <td>{request.contact}</td>
                      <td style={{ textAlign: "center", color: "rgba(244, 155, 63, 1)" }}>
                        <FontAwesomeIcon style={{ cursor: "pointer", fontSize: "20px" }} icon={faCircleInfo} onClick={() => { handleCircleInfoShow(request.message); setSelectedData(request.message) }} />
                      </td>
                      <td style={{ textAlign: "center" }}>
                        <button onClick={() => { handleAcceptShow(request.id); setSelectedData(request.id) }}>
                          Accept
                        </button>
                        <button onClick={() => { handleRejectShow(request.id); setSelectedData(request.id); setFunctionOption("decline") }}>
                          Decline
                        </button>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
            <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
              <table className="pending-requests-table">
                <thead>
                  <tr>
                    <th>Sl.no</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Date</th>
                    <th>No. of People</th>
                    <th>Contact</th>
                    <th style={{ textAlign: "center" }}>Message</th>
                    <th style={{ textAlign: "center" }} className="d-flex justify-content-center align-items-center">
                      Check all
                      <input
                        type="checkbox"
                        style={{ width: "20px", height: "20px", marginLeft: "10px" }}
                        checked={selectAll}
                        onChange={toggleSelectAll}
                      />
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {acceptedData.slice().sort((a, b) => new Date(a.date) - new Date(b.date)).map((request, index) => (
                    <tr key={request.id}>
                      <td>{index + 1}</td> {/* Serial number starts from 1 */}
                      <td>{request.name}</td>
                      <td>{request.email}</td>
                      <td>{request.date}</td>
                      <td>{request.num_of_people}</td>
                      <td>{request.contact}</td>
                      <td style={{ textAlign: "center", color: "rgba(244, 155, 63, 1)" }}>
                        <FontAwesomeIcon
                          style={{ cursor: "pointer", fontSize: "20px" }}
                          icon={faCircleInfo}
                          onClick={() => { handleCircleInfoShow(request.id); setSelectedData(request.message) }}
                        />
                      </td>
                      <td style={{ textAlign: "center" }}>
                        <input
                          type="checkbox"
                          style={{ width: "17px", height: "17px" }}
                          checked={isChecked(request.id)}
                          onChange={() => handleCheckboxChange(request.id)}
                        />
                        <FontAwesomeIcon
                          style={{ cursor: "pointer", fontSize: "20px", marginLeft: "20px", color: "rgba(255, 0, 0, 1)" }}
                          icon={faDeleteLeft}
                          onClick={() => { handleDeleteShow(request.id); setSelectedData(request.id); setFunctionOption("delete") }}
                        />
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
              <div className="pending-requests-table p-4 d-flex justify-content-end">
                <button onClick={handleDeleteAllMarkShow}>
                  Delete all marked
                </button>
                <button onClick={handleDeleteAllShow}>
                  Delete All
                </button>
              </div>
            </div>
          </div>


          <div className="d-flex justify-content-center align-items-center">
            <Modal show={showCircleInfoModal} onHide={handleClose} centered keyboard={false} size="md">
              <Modal.Header className="p-3" style={{ border: "none" }} closeButton />
              <Modal.Body className="pt-1" style={{ paddingInline: "30px" }}>
                <p style={{ color: "rgba(160, 152, 174, 1)", fontSize: "13px", fontWeight: "600", letterSpacing: "1px" }}>
                  {selectedData}
                </p>
              </Modal.Body>
            </Modal>
          </div>
          <div
            className="d-flex justify-content-center align-items-center"
          >
            <Modal
              show={showAcceptModal}
              onHide={handleClose}
              centered
              backdrop="static"
              keyboard={false}
              size="sm"
            >
              <Modal.Body
                className="text-center"
                style={{ paddingInline: "30px", paddingTop: "40px" }}
              >
                <FontAwesomeIcon
                  style={{ color: "rgba(0, 160, 177, 1)", fontSize: "50px" }}
                  className="pb-3"
                  icon={faQuestion}
                />
                <p
                  className="confirm-text"                >
                  Are you sure you want to Accept this request?
                </p>
              </Modal.Body>
              <Modal.Footer
                className="d-flex justify-content-evenly pt-0"
                style={{ border: "none", paddingBottom: "25px" }}
              >
                <Button className="model-button" onClick={handleClose}>
                  Cancel
                </Button>
                <Button className="model-button" onClick={acceptRequest} disabled={loading}>
                  {loading ? 'Accepting...' : 'Accept'}
                </Button>
              </Modal.Footer>
            </Modal>
          </div>
          <div className="d-flex justify-content-center align-items-center">
            <Modal
              show={showRejectModal}
              onHide={handleClose}
              centered
              backdrop="static"
              keyboard={false}
              size="md"
            >
              <Modal.Body
                className=""
                style={{ paddingInline: "30px", paddingTop: "40px" }}
              >
                <p className="confirm-text text-center">
                  Please provide a reason for rejection:
                </p>
                <textarea
                  className="form-control my-3"
                  placeholder="Enter reason for rejection"
                  value={rejectionReason}
                  onChange={(e) => setRejectionReason(e.target.value)}
                  style={{
                    border: '1px solid #ccc',
                    borderRadius: '4px',
                    padding: '10px',
                    width: '100%',
                    height: '100px',
                    resize: 'none',
                  }}
                />
              </Modal.Body>
              <Modal.Footer
                className="d-flex justify-content-evenly pt-0"
                style={{ border: "none", paddingBottom: "25px" }}
              >
                <Button className="model-button" onClick={handleClose}>
                  Cancel
                </Button>
                <Button className="model-button" onClick={rejectRequest} disabled={loading}>
                  {loading ? 'Rejecting...' : 'Reject'}
                </Button>
              </Modal.Footer>
            </Modal>
          </div>
          <div
            className="d-flex justify-content-center align-items-center"
          >
            <Modal
              show={showDeleteModal}
              onHide={handleClose}
              centered
              backdrop="static"
              keyboard={false}
              size="sm"
            >
              <Modal.Body
                className="text-center"
                style={{ paddingInline: "30px", paddingTop: "40px" }}
              >
                <FontAwesomeIcon
                  style={{ color: "rgba(0, 160, 177, 1)", fontSize: "50px" }}
                  className="pb-3"
                  icon={faQuestion}
                />
                <p
                  className="confirm-text"                >
                  Are you sure you want to remove this data?
                </p>
              </Modal.Body>
              <Modal.Footer
                className="d-flex justify-content-evenly pt-0"
                style={{ border: "none", paddingBottom: "25px" }}
              >
                <Button className="model-button" onClick={handleClose}>
                  Cancel
                </Button>
                <Button className="model-button" onClick={rejectRequest} disabled={loading}>
                  {loading ? 'Removing...' : 'Remove'}
                </Button>
              </Modal.Footer>
            </Modal>
          </div>
          <div
            className="d-flex justify-content-center align-items-center"
          >
            <Modal
              show={showDeleteAllMarkModal}
              onHide={handleClose}
              centered
              backdrop="static"
              keyboard={false}
              size="sm"
            >
              <Modal.Body
                className="text-center"
                style={{ paddingInline: "30px", paddingTop: "40px" }}
              >
                <FontAwesomeIcon
                  style={{ color: "rgba(0, 160, 177, 1)", fontSize: "50px" }}
                  className="pb-3"
                  icon={faQuestion}
                />
                <p
                  className="confirm-text"                >
                  Are you certain you wish to remove all selected data?
                </p>
              </Modal.Body>
              <Modal.Footer
                className="d-flex justify-content-evenly pt-0"
                style={{ border: "none", paddingBottom: "25px" }}
              >
                <Button className="model-button" onClick={handleClose}>
                  Cancel
                </Button>
                <Button className="model-button" onClick={deleteAllMarked} disabled={loading}>
                  {loading ? 'Removing...' : 'Remove'}
                </Button>
              </Modal.Footer>
            </Modal>
          </div>
          <div className="d-flex justify-content-center align-items-center">
            <Modal show={showDeleteAllModal} onHide={handleClose} centered backdrop="static" keyboard={false} size="sm">
              <Modal.Body className="text-center" style={{ paddingInline: "30px", paddingTop: "40px" }}>
                <FontAwesomeIcon style={{ color: "rgba(0, 160, 177, 1)", fontSize: "50px" }} className="pb-3" icon={faQuestion} />
                <p className="confirm-text">
                  Are you sure you want to remove all the data?
                </p>
              </Modal.Body>
              <Modal.Footer className="d-flex justify-content-evenly pt-0" style={{ border: "none", paddingBottom: "25px" }}>
                <Button className="model-button" onClick={handleClose}>
                  Cancel
                </Button>
                <Button className="model-button" onClick={deleteAll} disabled={loading}>
                  {loading ? 'Removing...' : 'Remove'}
                </Button>
              </Modal.Footer>
            </Modal>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Table;