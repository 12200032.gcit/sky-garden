import React from 'react';
import UserTabs from './UserTabs';
import "../styles/userTab.css";
import MenuUserTabs from './MenuTabs';



function MenuHome() {
  return (
    <div>
      <div className='d-flex user-tab'>
        <div className='p-4' style={{ width: '100%' }}>
          <div><MenuUserTabs /></div>
        </div>

      </div>

    </div>
  );
}
export default MenuHome;
