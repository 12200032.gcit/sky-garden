import React, { useState, useEffect } from "react";
import { useParams } from 'react-router-dom';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import ContainerFoodDetails from "./ContainerFoodDetails";
import SingleFoodCard from "./SingleFoodCard";

function FoodDetails() {
    const { id } = useParams();
    const [data, setData] = useState({});
    const fetchEventData = async () => {
        const toastId = toast.loading("Fetching data...");
        try {
            const response = await fetch(`https://sky-garden.ugyen1234.serv00.net/api/get-food-details/${id}`);

            if (!response.ok) {
                throw new Error('Failed to fetch data');
            }
            const foodData = await response.json();
            setData(foodData);
            toast.update(toastId, {
                render: "Data fetched successfully",
                type: "success",
                isLoading: false,
                autoClose: 3000,
            });
        } catch (error) {
            toast.update(toastId, {
                render: "Failed to fetch data",
                type: "error",
                isLoading: false,
                autoClose: 3000,
            });
        }
    };

    useEffect(() => {
        fetchEventData();
    }, [id]);

    return (
        <>
            <ContainerFoodDetails itemName={data.food_category}/>
            <SingleFoodCard data={data} />
        </>
    )
}

export default FoodDetails;
