import React from 'react';
import "./styles/category.css"
import { Link} from 'react-router-dom';
import 'primereact/resources/primereact.min.css';
import "primereact/resources/themes/lara-light-cyan/theme.css";
import { useState} from 'react';
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import {
  

  faQuestion,
} from "@fortawesome/free-solid-svg-icons";
// import './styles/carousel.css'

const CategoriesCard = (props) => {
  const { category, active, onClick, image, id } = props;
  const truncatedCategory = category.length > 10 ? category.slice(0, 7) + "..." : category;

  const [show, setShow] = useState(false);
  const [cateogryIdToDelete, setCateogryIdToDelete] = useState(null);
  const handleClose = () => setShow(false);
  const [data, setData] = useState([]);

  const handleShow = (id) => {
    setShow(true);
    setCateogryIdToDelete(id);
  };


  const handleRemove = async () => {
    try {
      const response = await fetch(`https://sky-garden.ugyen1234.serv00.net/api/deleteCategory/${cateogryIdToDelete}`, {
        method: "DELETE",
      });
      if (!response.ok) {
        throw new Error("Failed to delete category");
      }
      const result = await response.json();
      console.log(result); // Log the result object
      if (result && result.result === 'Category and related food items have been deleted') {
        toast.success("Category has been deleted successfully", {
          autoClose: 1000,
          onClose: () => {
            handleClose();
            window.location.reload();
          }
        });
        getData();
      } else {
        toast.error("Failed to delete", {
          autoClose: 3000,
        });
      }
    } catch (error) {
      toast.error("Failed to delete category", {
        autoClose: 3000,
      });
    }
  };
  



  async function getData() {
    try {
      let response = await fetch("https://sky-garden.ugyen1234.serv00.net/api/getfooditem");
      if (!response.ok) {
        throw new Error('Failed to fetch data');
      }
      let result = await response.json();
      setData(result);
    } catch (error) {
      console.error('failed to fetch data');
    }
  }
  // useEffect(() => {
  //   getData();
  // }, []
  // }, []);

  return (
    <div
      className={`icon-container-main ${active ? 'active' : ''}`}
      onClick={() => onClick(category)}
    >
      <div className="card categoryCard ">
        <div className='icon-container'>
          {/* <img className="card-img-top icon-img" src={require(`./image/${category}.png`)} alt={category} /> */}
          <img className="card-img-top icon-img" src={"https://sky-garden.ugyen1234.serv00.net/storage/" + image} alt='food img'/>
          {active && <button className="delete-button2" onClick={() => handleShow(id)}>&times;</button>}
        </div>
        <div className="card-body-content p-0">
          <p className="card-text  ">{truncatedCategory}</p>
        </div>
        {active && <Link to={`/category-edit/${props.id}`}><button className="edit-button">Edit</button></Link>}
      </div>


      <Modal
            show={show}
            onHide={handleClose}
            centered
            backdrop="static"
            keyboard={false}
            size="sm"
          >
            <Modal.Body
              className="text-center"
              style={{ paddingInline: "30px", paddingTop: "40px" }}
            >
              <FontAwesomeIcon
                style={{ color: "rgba(0, 160, 177, 1)", fontSize: "50px" }}
                className="pb-3"
                icon={faQuestion}
              />
              <p
                style={{
                  color: "rgba(160, 152, 174, 1)",
                  fontSize: "13px",
                  fontWeight: "600",
                  letterSpacing: "1px",
                }}
              >
                Are you sure you want to remove this Catergory. <br/><span style={{ color:"rgba(235, 87, 87, 1)",fontSize:"11px"}}>All the Food Items Related to this Categeory will be also removed?</span>
              </p>
            </Modal.Body>
            <Modal.Footer
              className="d-flex justify-content-evenly pt-0"
              style={{ border: "none", paddingBottom: "25px" }}
            >
              <Button
                style={{
                  backgroundColor: "rgba(244, 155, 63, 1)",
                  borderRadius: "20px",
                  border: "none",
                  fontSize: "13px",
                  fontWeight: "600",
                  letterSpacing: "1px",
                  paddingInline: "15px",
                  paddingBlock: "8px",
                }}
                onClick={handleClose}
              >
                Cancel
              </Button>
              <Button
                style={{
                  backgroundColor: "rgba(244, 155, 63, 1)",
                  borderRadius: "20px",
                  border: "none",
                  fontSize: "13px",
                  fontWeight: "600",
                  letterSpacing: "1px",
                  paddingInline: "15px",
                  paddingBlock: "8px",
                }}
                onClick={handleRemove}
              >
                Remove
              </Button>
            </Modal.Footer>
      
          </Modal>




    </div>
  );
};

export default CategoriesCard;
