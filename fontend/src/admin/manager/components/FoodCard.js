import React, { useState, useEffect } from 'react';
import "./styles/foodcard.css";
import { Link } from 'react-router-dom';
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faQuestion } from "@fortawesome/free-solid-svg-icons";

const FoodCard = (props) => {
  const [show, setShow] = useState(false);
  const [foodItemIdToDelete, setFoodItemIdToDelete] = useState(null);
  const [data, setData] = useState([]);

  const handleClose = () => setShow(false);

  const truncateText = (text, maxLength) => {
    if (text.length <= maxLength) {
      return text;
    }
    return text.substring(0, maxLength) + "...";
  };

  const handleShow = (id) => {
    setShow(true);
    setFoodItemIdToDelete(id);
  };

  const handleRemove = async () => {
    try {
      const response = await fetch(`https://sky-garden.ugyen1234.serv00.net/api/deleteFoodItem/${foodItemIdToDelete}`, {
        method: "DELETE",
      });
      if (!response.ok) {
        throw new Error("Failed to delete FoodItem");
      }
      const result = await response.json();

      if (result && result.result === 'Food item has been deleted') {
        toast.success("Food Item deleted successfully", {
          autoClose: 1000,
          onClose: () => {
            handleClose();
            window.location.reload();
          }
        });
        getData();
      } else {
        toast.error("Failed to delete", {
          autoClose: 3000,
        });
        handleClose()
      }
    } catch (error) {
      toast.error("Failed to delete food item", {
        autoClose: 3000,
      });
      handleClose()
    }
  };

  const getData = async () => {
    try {
      let response = await fetch("https://sky-garden.ugyen1234.serv00.net/api/getfooditem");
      if (!response.ok) {
        throw new Error('Failed to fetch data');
      }
      let result = await response.json();
      setData(result);
    } catch (error) {
      console.error('Error fetching data');
    }
  }

  useEffect(() => {
    getData();
  }, []);

  return (
    <div className="card foodCard" style={{ backgroundColor: 'white' }}>
      <div className="food-card">

        <button className="delete-button" onClick={() => handleShow(props.id)}>&times;</button>
        {props.status ? (
          <p
            className=''
            style={{
              position: 'absolute',
              top: 25,
              fontWeight: "500",
              letterSpacing: "1px",
              borderTopRightRadius: "10px",
              borderBottomRightRadius: "10px",
              backgroundColor: 'rgba(235, 87, 87, 1)',
              padding: '5px 10px',
              color: "#fff",
              zIndex: 1
            }}
          >
            {props.discount}% off
          </p>
        ):(
          <p></p>
        )}
        <img className="card-img-top food-img" alt='food img' width={100} src={"https://sky-garden.ugyen1234.serv00.net/storage/" + props.image} />
        <div className="card-body">
          <h5 className="card-title food-title">{props.foodName}</h5>
          <p className="card-text food-description">{truncateText(props.description, 25)}</p>

          {props.status ? (
            <div>
              {/* <p className="card-cost">
                <s>Nu: {props.price}</s>
              </p> */}
              <p className="card-cost">
                Nu: {props.after_discount}
              </p>
            </div>
          ) : (
            <p className="card-cost">Nu: {props.price}</p>
          )}

          <Link to={`/edit-food-items/${props.id}`} className="btn link">Edit</Link>
        </div>
      </div>

      <Modal
        show={show}
        onHide={handleClose}
        centered
        backdrop="static"
        keyboard={false}
        size="sm"
      >
        <Modal.Body
          className="text-center"
          style={{ paddingInline: "30px", paddingTop: "40px" }}
        >
          <FontAwesomeIcon
            style={{ color: "rgba(0, 160, 177, 1)", fontSize: "50px" }}
            className="pb-3"
            icon={faQuestion}
          />
          <p
            style={{
              color: "rgba(160, 152, 174, 1)",
              fontSize: "13px",
              fontWeight: "600",
              letterSpacing: "1px",
            }}
          >
            Are you sure you want to remove {props.foodName} from menu?
          </p>
        </Modal.Body>
        <Modal.Footer
          className="d-flex justify-content-evenly pt-0"
          style={{ border: "none", paddingBottom: "25px" }}
        >
          <Button
            style={{
              backgroundColor: "rgba(244, 155, 63, 1)",
              borderRadius: "20px",
              border: "none",
              fontSize: "13px",
              fontWeight: "600",
              letterSpacing: "1px",
              paddingInline: "15px",
              paddingBlock: "8px",
            }}
            onClick={handleClose}
          >
            Cancel
          </Button>
          <Button
            style={{
              backgroundColor: "rgba(244, 155, 63, 1)",
              borderRadius: "20px",
              border: "none",
              fontSize: "13px",
              fontWeight: "600",
              letterSpacing: "1px",
              paddingInline: "15px",
              paddingBlock: "8px",
            }}
            onClick={handleRemove}
          >
            Remove
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
};

export default FoodCard;
