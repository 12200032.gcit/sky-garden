import React from 'react';
import { Card, CardImg, CardBody, CardTitle, CardText } from 'reactstrap';
import "../styles/horizontalCard.css";
import '../styles/description.css';


function DescriptionCard() {

    return (
       
            <Card className="my-2 d-flex flex-row" style={{ height: '40vw', border: 'none', backgroundColor: 'white', paddingTop:"100px" }}>
                <CardImg
                    alt="Card image cap"
                    src="/img/img.png" 
                    style={{
                        width: '40%',
                        height: '90%',
                        objectFit: 'cover',
                        marginLeft: "9vw",


                    }}
                />
                <CardBody className="d-flex flex-column justify-content-between" style={{ marginLeft: '3vw', paddingTop:"6vw", width:"60%",paddingRight:"10vw"}}>
                    <div>
                        <CardTitle className='description-title'>We provide healthy food for your family.</CardTitle>
                        <CardText className='description-sub-title'>Our story began with a vision to create a
                            unique dining experience that merges fine dining, exceptional service, and a
                            vibrant ambiance. Rooted in city's rich culinary culture, we aim to honor our
                            local roots while infusing a global palate.
                        </CardText>
                        <CardText className='description-sub-title'>At place, we believe that dining is not just about food, but also
                            about the overall experience. Our staff, renowned for their warmth and dedication, strives to make
                            every visit an unforgettable event.
                        </CardText>
                    </div>
                </CardBody>
            </Card>
   

    );
}

export default DescriptionCard;
