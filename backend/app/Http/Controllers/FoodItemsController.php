<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\FoodItems;
use App\Models\Discount;

class FoodItemsController extends Controller
{
    function addFoodItem(Request $req)
    {

        // Create a new food item
        $foodItem = new FoodItems();
        $foodItem->food_category = $req->input('food_category');
        $foodItem->food_name = $req->input('food_name');
        $foodItem->price = $req->input('price');
        $foodItem->description = $req->input('description');
        // Save the food item

        if ($req->hasFile('image')) {
            $foodItem->image = $req->file('image')->store('FoodItemImage', 'public');
        }

        $foodItem->save();

        // Create a new discount record with the same price for actual_price and after_discount
        $discount = new Discount();
        $discount->food_category = $req->input('food_category');
        $discount->food_name = $req->input('food_name');
        $discount->discount = 0; // Set default discount to 0
        $discount->actual_price = $req->input('price'); // Set actual_price to the same as price
        $discount->after_discount = $req->input('price'); // Set after_discount to the same as price
        $discount->offer_status = false; // Set offer_status to false by default
        // Save the discount record
        $discount->save();
    }

    public function getAllFoodItems()
    {
        $foodItems = FoodItems::leftJoin('discounts', function ($join) {
            $join->on('food_items.food_name', '=', 'discounts.food_name')
                ->on('food_items.food_category', '=', 'discounts.food_category');
        })
            ->select('food_items.*', 'discounts.discount', 'discounts.after_discount', 'discounts.offer_status')
            ->get();

        return $foodItems;
    }

    public function deleteFoodItem($id)
    {
        $foodItem = FoodItems::find($id);

        if (!$foodItem) {
            return response()->json(['message' => 'Food item not found'], 404);
        }

        $imagePath = storage_path('app/public/' . $foodItem->image);
        if (file_exists($imagePath)) {
            unlink($imagePath);
        }

        $discount = Discount::where('food_name', $foodItem->food_name)
            ->where('food_category', $foodItem->food_category)
            ->first();
        if ($discount) {
            $discount->delete();
        }
        $foodItem->delete();
        return response()->json(['result' => 'Food item has been deleted'], 200);
    }


    public function search($key)
    {
        return FoodItems::where('food_name', '=', $key)->get();
    }

    public function get_Food_Details($id)
    {
        $food = FoodItems::find($id);

        if (!$food) {
            return response()->json(['message' => 'Food item not found'], 404);
        }

        $foodItem = FoodItems::leftJoin('discounts', function ($join) {
            $join->on('food_items.food_name', '=', 'discounts.food_name')
                ->on('food_items.food_category', '=', 'discounts.food_category');
        })
            ->select('food_items.*', 'discounts.discount', 'discounts.after_discount', 'discounts.offer_status')
            ->where('food_items.id', $food->id)
            ->first();

        return response()->json($foodItem);
    }

    public function get_foodItems($id)
    {
        $foodItems = FoodItems::find($id);

        if (!$foodItems) {
            return response()->json(['message' => 'food Items not found'], 404);
        }
        return $foodItems;
    }
    function editFoodItem(Request $req, $id)
    {

        // Create a new food item
        $foodItem = FoodItems::findOrFail($id);
        $foodItem->food_category = $req->input('food_category');
        $foodItem->food_name = $req->input('food_name');
        $foodItem->price = $req->input('price');
        $foodItem->description = $req->input('description');
        // Save the food item
        if ($req->hasFile('image')) {
            error_log("has image");
            // Delete the old image if it exists
            if ($foodItem->image) {
                $imagePath = storage_path('app/public/' . $foodItem->image);
                if (file_exists($imagePath)) {
                    unlink($imagePath);
                }
            }

            $foodItem->image = $req->file('image')->store('FoodItemImage', 'public');
        }

        $foodItem->save();

        $discount = Discount::findOrFail($id);
        $discount->food_category = $req->input('food_category');
        $discount->food_name = $req->input('food_name');
        $discount->actual_price = $req->input('price'); // Set actual_price to the same as price
        // If offer_status is true, calculate after_discount based on discount percentage
        if ($discount->offer_status) {
            $discountPercentage = $discount->discount; // Assuming you have discount percentage in your request
            $actualPrice = $req->input('price');
            $afterDiscount = $actualPrice - ($actualPrice * ($discountPercentage / 100));

            $discount->after_discount = $afterDiscount;
        } else {
            // Set after_discount to be the same as the actual price
            $discount->after_discount = $req->input('price');
        }

        // Save the discount record
        $discount->save();
    }
}
