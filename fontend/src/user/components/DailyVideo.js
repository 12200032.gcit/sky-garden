import React, { useState, useRef, useEffect } from "react";
import "../styles/Daily.css";


function DailyVideo() {
    const [isMuted, setIsMuted] = useState(true);
    const videoRef = useRef(null);
    const [specialVideoUrl, setSpecialVideoUrl] = useState(null);

    const toggleMute = () => {
        setIsMuted(!isMuted);
        if (videoRef.current) {
            videoRef.current.muted = !isMuted;
        }
    };

    useEffect(() => {
        fetchVideos();
    }, []);

    const fetchVideos = async () => {
        try {
            const response = await fetch("https://sky-garden.ugyen1234.serv00.net/api/get-videos");
            if (response.ok) {
                const data = await response.json();
                setSpecialVideoUrl(data.special_video);
            } else {
                throw new Error('Failed to fetch videos.');
            }
        } catch (error) {
            console.error('Failed to fetch videos. Please try again.');
        }
    };


    return (
        <div className="video-daily-main">
            <div className="video-daily-container d-flex">
                <div className="blue-mark"></div>
                <div className="video-daily-title">
                    <p>Daily Special Video</p>
                </div>
            </div>
            <div className="daily-video d-flex">
                <div className="dot-img"> <img src="/img/dot.png"  alt="a logo" className="dot-group" /></div>
                <div>
                    <video
                        ref={videoRef}
                        autoPlay
                        muted
                        controls
                        onClick={toggleMute}
                        className="daily-video-upload"
                        src={`https://sky-garden.ugyen1234.serv00.net${specialVideoUrl}`} type="video/mp4"
                    >

                    </video>


                </div>
                <div className="dot-img"> <img src="/img/dot.png" alt="a logo" className="dot-group2" /></div>
                <div className="text-daily">
                    <div>
                        <span className="sky-text3">Sky</span><span className="sky-text4">Garden</span>-
                        <span className="just-text">Just A Click Away!!</span>

                    </div>
                    <div className="video-text">Welcome to <span className="sky-text1">Sky</span><span className="sky-text2">Garden</span>, where every day is a culinary adventure! Join us as we
                        unveil our chef's specially curated daily specials, each crafted with the freshest ingredients and bursting
                        with unique flavors.Enjoy a perfect pairing with our sommelier's recommended wines and beverages. Whether you're a foodie
                        or simply seeking a memorable meal, our daily specials are designed to tantalize your taste buds and
                        leave you craving more. Tune in daily to see what's at <span className="sky-text1">Sky</span><span className="sky-text2">Garden</span>– your next favorite dish is just
                        a click away!
                    </div>

                </div>


            </div>
        </div>
    );
}
export default DailyVideo;
