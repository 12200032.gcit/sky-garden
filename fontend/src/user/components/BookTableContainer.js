import React from "react";
import "../styles/nav.css";
import Navbar from "./Navbar";
import Marquee from "./SlidingWord";
import Table from "./BookTable";
import MobileNavbar from "./MoblieNavbar";
import Footer from "./Footer";
import Whatsapp from "./WhatsAppIcon";




const BookTableForm= () => {
  return (
    <div>
      <Marquee />
      <Navbar />
      <MobileNavbar />
      <div style={{ position: "fixed", top: "40vw", right: "7vw", zIndex: "50" }}>
        <Whatsapp />
      </div>
      <Table/>
      <Footer/>
    </div>
  );
};
export default BookTableForm;
