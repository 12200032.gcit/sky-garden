<?php

namespace App\Http\Controllers;

use App\Models\Video;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class VideoController extends Controller
{
    public function uploadSpecialVideo(Request $request)
    {
        $request->validate([
            'video' => 'required|mimes:mp4,mov,avi,flv', // max 200MB
        ]);

        $path = $request->file('video')->store('special-video', 'public');

        // Delete the old special video if it exists
        $video = Video::first();
        if ($video && $video->special_video) {
            Storage::delete($video->special_video);
            Storage::disk('public')->delete($video->special_video);
        }

        // Update or create the special video record
        Video::updateOrCreate([], ['special_video' => $path]);

        return response()->json(['message' => 'Special video uploaded successfully']);
    }

    public function uploadPromotionalVideo(Request $request)
    {
        $request->validate([
            'video' => 'required|mimes:mp4,mov,avi,flv', // max 200MB
        ]);

        $path = $request->file('video')->store('promotional-video', 'public');

        // Delete the old promotional video if it exists
        $video = Video::first();
        if ($video && $video->promotion_video) {
            Storage::delete($video->promotion_video);
            Storage::disk('public')->delete($video->promotion_video);
        }

        // Update or create the promotional video record
        Video::updateOrCreate([], ['promotion_video' => $path]);

        return response()->json(['message' => 'Promotional video uploaded successfully']);
    }

    public function getVideos()
    {
        $video = Video::first();
        return response()->json([
            'special_video' => $video ? Storage::url($video->special_video) : null,
            'promotion_video' => $video ? Storage::url($video->promotion_video) : null,
        ]);
    }
}
