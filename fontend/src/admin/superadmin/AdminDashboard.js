import React, { useState, useEffect } from "react";
import SideBar from "./AdminSidebar";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus, faQuestion } from "@fortawesome/free-solid-svg-icons";
import "../manager/css/Table.css";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import { Link } from 'react-router-dom';
import Topbar from "../Topbar";
import "./css/addmanager.css";

function AdminDashboard() {
  const [managers, setManagers] = useState([]);
  const [showRejectModal, setShowRejectModal] = useState(false);
  const [selectedManager, setSelectedManager] = useState(null);
  const [loading, setLoading] = useState(false);


  const fetchManagers = async () => {
    try {
      const response = await fetch("https://sky-garden.ugyen1234.serv00.net/api/get-all-manager", {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
        },
      });
      if (!response.ok) {
        toast.error('Failed to fetch managers');
      }
      const data = await response.json();
      setManagers(data); // assuming your API response has a 'managers' key
    } catch (error) {
      toast.error("Failed to fetch managers");
    }
  };

  useEffect(() => {
    fetchManagers();
  }, []);

  const handleClose = () => {
    setShowRejectModal(false);
    setSelectedManager(null);
  };

  const handleRejectShow = (manager) => {
    setSelectedManager(manager);
    setShowRejectModal(true);
  };

  const handleRemove = async (e) => {
    e.preventDefault();
    setLoading(true);
    if (selectedManager) {
      try {

        const response = await fetch(`https://sky-garden.ugyen1234.serv00.net/api/delete-manager/${selectedManager.id}`, {
          method: 'DELETE',
          headers: {
            'Content-Type': 'application/json',
          },
        });
        if (!response.ok) {
          toast.error('Failed to delete manager');
        }
        if (response.ok) {
          toast.success("Manager deleted successfully")
          handleClose();
          fetchManagers();
        }
      } catch (error) {
        toast.error("Failed to delete manager!");
        handleClose();
      } finally {
        setLoading(false);
      }
    }
  };


  return (
    <div className="d-flex">
      <SideBar />
      <div className="p-4" style={{ width: "93%" }}>
        <Topbar />
        <div className="title_bar">
          <p>Manager</p>
        </div>
        <div>
          <div className="add-manager">
            <p className="topic">Add Manager</p>
            <Link to="/add-manager" className="col-md-6 adding-bar">
              <FontAwesomeIcon className="icon" icon={faPlus} />
            </Link>
          </div>
          <div className="tab-content p-3" id="nav-tabContent">
            <p className="topic">List of Managers</p>
            <div className="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
              <table className="pending-requests-table">
                <thead>
                  <tr>
                    <th>Sl.No</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Contact</th>
                    <th style={{ textAlign: "center" }}>Action</th>
                  </tr>
                </thead>
                <tbody>
                  {managers && managers.map((manager, index) => (
                    <tr key={manager.id}>
                      <td>{index + 1}</td>
                      <td>{manager.name}</td>
                      <td>{manager.email}</td>
                      <td>{manager.contact}</td>
                      <td style={{ textAlign: "center" }}>
                        <button onClick={() => handleRejectShow(manager)}>
                          Delete
                        </button>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </div>

          <div className="d-flex justify-content-center align-items-center">
            <Modal
              show={showRejectModal}
              onHide={handleClose}
              centered
              backdrop="static"
              keyboard={false}
              size="sm"
            >
              <Modal.Body
                className="text-center"
                style={{ paddingInline: "30px", paddingTop: "40px" }}
              >
                <FontAwesomeIcon
                  style={{ color: "rgba(0, 160, 177, 1)", fontSize: "50px" }}
                  className="pb-3"
                  icon={faQuestion}
                />
                <p
                  style={{
                    color: "rgba(160, 152, 174, 1)",
                    fontSize: "13px",
                    fontWeight: "600",
                    letterSpacing: "1px",
                  }}
                >
                  Are you sure you want to delete this manager?
                </p>
              </Modal.Body>
              <Modal.Footer
                className="d-flex justify-content-evenly pt-0"
                style={{ border: "none", paddingBottom: "25px" }}
              >
                <Button
                  style={{
                    backgroundColor: "rgba(244, 155, 63, 1)",
                    borderRadius: "20px",
                    border: "none",
                    fontSize: "13px",
                    fontWeight: "600",
                    letterSpacing: "1px",
                    paddingInline: "15px",
                    paddingBlock: "8px",
                  }}
                  onClick={handleClose}
                >
                  Cancel
                </Button>
                <Button
                  style={{
                    backgroundColor: "rgba(244, 155, 63, 1)",
                    borderRadius: "20px",
                    border: "none",
                    fontSize: "13px",
                    fontWeight: "600",
                    letterSpacing: "1px",
                    paddingInline: "15px",
                    paddingBlock: "8px",
                  }}
                  disabled={loading}
                  onClick={handleRemove}
                >
                  {loading ? 'Deleting...' : 'Delete'}
                </Button>
              </Modal.Footer>
            </Modal>
          </div>
        </div>
      </div>
    </div>
  );
}

export default AdminDashboard;
