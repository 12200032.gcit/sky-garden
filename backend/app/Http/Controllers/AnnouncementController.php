<?php

namespace App\Http\Controllers;

use App\Models\Announcement;
use Illuminate\Http\Request;

class AnnouncementController extends Controller
{
    public function getAnnouncement()
    {
        return Announcement::all();
    }

    public function getActiveAnnouncement()
    {
        return Announcement::where('status', true)->get();
    }
    
    


    public function storeAnnouncement(Request $request)
    {
        $request->validate([
            'announcement' => 'required|string',
        ]);

        $announcement = Announcement::create([
            'announcement' => $request->announcement,
            'status' => false,
        ]);

        return response()->json($announcement, 201);
    }

    public function updateStatus($id)
    {
        // Set all announcements to false
        Announcement::query()->update(['status' => false]);

        // Set the selected announcement to true
        $announcement = Announcement::findOrFail($id);
        $announcement->status = true;
        $announcement->save();

        return response()->json($announcement);
    }

    public function deleteAnnouncement($id)
    {
        try {
            // Find the announcement by ID
            $announcement = Announcement::findOrFail($id);

            // Check if the status is true
            if ($announcement->status) {
                return response()->json(['message' => 'Active announcement cannot be deleted'], 400);
            }

            // Delete the announcement
            $announcement->delete();

            return response()->json(['message' => 'Announcement deleted successfully'], 200);
        } catch (\Exception $e) {
            // Return error response if announcement not found or deletion fails
            return response()->json(['error' => 'Failed to delete announcement'], 500);
        }
    }
}
