import React, { useState } from 'react';
import './superadmin/css/login.css';
import { toast, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { useNavigate } from 'react-router-dom';

function ResetPassword() {
    const [email, setEmail] = useState('');
    const [otp, setOtp] = useState('');
    const [newPassword, setNewPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');
    const [step, setStep] = useState(1); // Step 1: Send OTP, Step 2: Verify OTP, Step 3: Set New Password
    const [loading, setLoading] = useState(false);
    const navigate = useNavigate();

    const handleSendOtp = async (e) => {
        e.preventDefault();
        setLoading(true);
        try {
            const response = await fetch('https://sky-garden.ugyen1234.serv00.net/api/send-otp', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({ email }),
            });

            if (response.ok) {
                toast.success('OTP sent successfully');
                setStep(2); // Move to OTP verification step
            } else if (response.status === 404) {
                toast.error('Email does not exist');
            } else {
                toast.error('An error occurred while sending the OTP');
            }
        } catch (error) {
            toast.error('An error occurred while sending the OTP');
        } finally {
            setLoading(false);
        }
    };

    const handleVerifyOtp = async (e) => {
        e.preventDefault();

        // Check if the OTP is exactly 6 digits long
        if (otp.length !== 6) {
            toast.error('OTP must be 6 digits');
            return;
        }

        setLoading(true);

        try {
            const response = await fetch('https://sky-garden.ugyen1234.serv00.net/api/verify-otp', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({ email, otp }),
            });

            if (response.ok) {
                toast.success('OTP verified successfully');
                setStep(3); // Move to set new password step
            } else if (response.status === 422) {
                const result = await response.json();
                toast.error(result.message);
            } else {
                toast.error('An error occurred while verifying the OTP');
            }
        } catch (error) {
            toast.error('An error occurred while verifying the OTP');
        } finally {
            setLoading(false);
        }
    };

    const handleSetNewPassword = async (e) => {
        e.preventDefault();

        // Check if new passwords match
        if (newPassword !== confirmPassword) {
            toast.error('Passwords do not match');
            return;
        }
        if (newPassword.length < 6 || !/[a-zA-Z]/.test(newPassword) || !/[0-9]/.test(newPassword)) {
            toast.error('New password must be at least 6 characters long and contain both letters and numbers');
            return;
        }

        setLoading(true);

        try {
            const response = await fetch('https://sky-garden.ugyen1234.serv00.net/api/set-new-password', {
                method: 'put',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({ email, newPassword }),
            });

            if (response.ok) {
                toast.success('Password reset successfully');
                setTimeout(() => {
                    navigate('/');
                }, 2000);
            } else {
                toast.error('An error occurred while setting the new password');
            }
        } catch (error) {
            toast.error('An error occurred while setting the new password');
        } finally {
            setLoading(false);
        }
    };

    return (
        <div className="login-container" style={{ marginTop: "130px" }}>
            <ToastContainer />
            <div className='login-img'>
                <img src={'img/logo.jpg'} alt="Logo" style={{ width: '100%', height: '100%', objectFit: 'cover' }} />
            </div>
            {step === 1 ? (
                <>
                    <h2>Check your email</h2>
                    <form onSubmit={handleSendOtp}>
                        <div className="form-group">
                            <label htmlFor="email">Email</label>
                            <input
                                type="email"
                                id="email"
                                className="login-input"
                                placeholder="Enter email address"
                                value={email}
                                onChange={(e) => setEmail(e.target.value)}
                                autoFocus
                                required
                            />
                        </div>
                        <button type="submit" className="login-btn mt-5" disabled={loading}>
                            {loading ? 'Sending...' : 'Send OTP'}
                        </button>
                    </form>
                </>
            ) : step === 2 ? (
                <>
                    <h2>Check your email</h2>
                    <p style={{ fontSize: "13px", fontWeight: "600", letterSpacing: "1px", color: "rgba(92, 87, 87, 1)" }}>Enter the 6-digit code we just sent to your mail.</p>
                    <form onSubmit={handleVerifyOtp}>
                        <div className="form-group">
                            <label htmlFor="otp">Verification code</label>
                            <input
                                type="text"
                                id="otp"
                                className="login-input"
                                placeholder="Enter OTP"
                                value={otp}
                                onChange={(e) => setOtp(e.target.value)}
                                autoFocus
                                required
                            />
                        </div>
                        <button type="submit" className="login-btn mt-5" disabled={loading}>
                            {loading ? 'Verifying...' : 'Verify OTP'}
                        </button>
                    </form>
                </>
            ) : (
                <>
                    <h2>Set New Password</h2>
                    <form onSubmit={handleSetNewPassword}>
                        <div className="form-group">
                            <label htmlFor="newPassword">New Password</label>
                            <input
                                type="password"
                                id="newPassword"
                                className="login-input"
                                placeholder="Enter new password"
                                value={newPassword}
                                autoFocus
                                onChange={(e) => setNewPassword(e.target.value)}
                                required
                            />
                        </div>
                        <div className="form-group">
                            <label htmlFor="confirmPassword">Confirm Password</label>
                            <input
                                type="password"
                                id="confirmPassword"
                                className="login-input"
                                placeholder="Confirm new password"
                                value={confirmPassword}
                                onChange={(e) => setConfirmPassword(e.target.value)}
                                required
                            />
                        </div>
                        <button type="submit" className="login-btn mt-5" disabled={loading}>
                            {loading ? 'Setting...' : 'Set Password'}
                        </button>
                    </form>
                </>
            )}
        </div>
    );
}

export default ResetPassword;
