import React, { useState, useEffect } from 'react';
import { Tabs as MuiTabs, createTheme, ThemeProvider, Button } from '@mui/material'; // Import createTheme
import Tab from '@mui/material/Tab';
import RestaurantIcon from '@mui/icons-material/Restaurant';
import DeliveryDiningIcon from '@mui/icons-material/DeliveryDining';
import UserFoodGallary from './UserContent';

import "../styles/userTab.css";


const theme = createTheme({ // Define the theme variable
  components: {
    MuiTabs: {
      styleOverrides: {
        indicator: {
          backgroundColor: '#F49B3F',
        },
      },
    },
  },
});

const UserTabs = () => {
  const [data, setData] = useState([]);
  const [value, setValue] = useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const renderTabContent = (index) => {
    switch (index) {
      case 0:
        return <UserFoodGallary selectedData={data.filter(item => item.dinning_in === 1)} />;
      case 1:
        return <UserFoodGallary selectedData={data.filter(item => item.take_away === 1)} />;
      default:
        return null;
    }
  };

  async function getData() {
    let response = await fetch("https://sky-garden.ugyen1234.serv00.net/api/categorylist");
    let result = await response.json();
    setData(result);
  }

  useEffect(() => {
    getData();
  }, []);

  return (
    <ThemeProvider theme={theme}>
      <div className='tab-container user-tab-container'>
        <MuiTabs
          value={value}
          onChange={handleChange}
          aria-label="icon label tabs example"
          indicatorColor="primary"
          className='user-individual-container'
        >
          <Tab
            icon={<RestaurantIcon />}
            label="Dining In"
            sx={{
              '&.Mui-selected': {
                color: '#00A0B1',
              },
            }}
          />
          <Tab
            icon={<DeliveryDiningIcon />}
            label="Take Away"
            sx={{
              '&.Mui-selected': {
                color: '#00A0B1',
              },
            }}
          />
        </MuiTabs>

        <div>{renderTabContent(value)}</div>
      </div>
    </ThemeProvider>
  );
}

export default UserTabs;
