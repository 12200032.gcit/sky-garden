<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Mail\OtpEmail;
use App\Mail\PasswordEmail;
use Carbon\Carbon;
use Dotenv\Exception\ValidationException;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class UserController extends Controller
{
    function getAllManager()
    {
        return User::where('role', 'manager')->get();
    }

    function adminProfile()
    {
        return User::where('role', 'superadmin')->get();
    }


    public function deleteManager($id)
    {
        try {
            $user = User::findOrFail($id);
            $user->delete();
            return response()->json(['message' => 'manager deleted successfully'], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Failed manager deleted1!'], 500);
        }
    }

    public function addManager(Request $request)
    {
        // Validate the request data
        try {
            $request->validate([
                'name' => 'required|string',
                'email' => 'required|string|email|max:255|unique:users',
                'contact' => 'nullable|string|max:255',
            ]);

            // Generate a strong password
            $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?';
            $password = substr(str_shuffle($chars), 0, 10);
            $email = $request->input('email');
            $name = $request->input('name');

            // Attempt to send the email with the new password
            try {
                // Assuming you have a mailable class called PasswordEmail
                Mail::to($email)->send(new PasswordEmail($name, $password));
            } catch (Exception $e) {
                return response()->json(['message' => 'Failed to send email: ' . $e->getMessage()], 500);
            }
            // Create a new user
            User::create([
                'name' => $request->name,
                'email' => $request->email,
                'contact' => $request->contact,
                'password' => Hash::make($password),
            ]);

            return response()->json(['message' => 'Manager added successfully and password sent to email'], 200);
        } catch (ValidationException $e) {
            return response()->json(['message' => 'Validation error', 'errors' => $e], 422);
        } catch (Exception $e) {
            // Handle any other errors
            if ($e->getMessage() == "The email has already been taken.") {
                return response()->json(['error' => $e->getMessage()], 409);
            } else {
                return response()->json(['message' => $e->getMessage()], 500);
            }
        }
    }
    public function updateSetting(Request $request)
    {
        // Validate the request data
        try {
            $request->validate([
                'email' => 'required|string|email|max:255',
                'old_password' => 'required|string',
                'new_password' => 'nullable|string|min:6',
                'name' => 'nullable|string',
                'contact' => 'nullable|string|max:255',
            ]);

            // Find the user by email
            $user = User::where('email', $request->email)->first();

            // Check if user exists
            if (!$user) {
                return response()->json(['error' => 'User not found'], 404);
            }

            // Check if old password matches
            if (!Hash::check($request->old_password, $user->password)) {
                return response()->json(['error' => 'Incorrect old password'], 401);
            } else {
                $userData = [];
                if ($request->filled('name')) {
                    $userData['name'] = $request->name;
                }
                if ($request->filled('email')) {
                    $userData['email'] = $request->email;
                }
                if ($request->filled('contact')) {
                    $userData['contact'] = $request->contact;
                }
                if ($request->filled('new_password')) {
                    $userData['password'] = Hash::make($request->new_password);
                }

                // Update user data
                $user->update($userData);

                return response()->json([
                    'message' => 'Updated successfully',
                    'user' => $user
                ], 200);
            }
        } catch (ValidationException $e) {
            return response()->json(['message' => 'Validation error', 'errors' => $e], 422);
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()], 500);
        }
    }


    public function sendOtp(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
        ]);

        $email = $request->input('email');

        try {
            $user = User::where('email', $email)->firstOrFail();
        } catch (ModelNotFoundException $e) {
            return response()->json(['message' => 'Email does not exist'], 404);
        }

        $otp = mt_rand(100000, 999999); // Generate OTP
        $user->otp = $otp; // Store OTP in user record
        $user->otp_created_at = Carbon::now(); // Store OTP creation timestamp
        $user->save();

        // Send email with OTP
        Mail::to($email)->send(new OtpEmail($user, $otp));

        return response()->json(['message' => 'OTP sent successfully'], 200);
    }

    public function verifyOtp(Request $request)
    {
        $request->validate([
            'email' => 'required|email|exists:users,email',
            'otp' => 'required|digits:6',
        ]);

        $email = $request->input('email');
        $otp = $request->input('otp');

        $user = User::where('email', $email)->first();

        if (!$user) {
            return response()->json(['message' => 'User not found'], 404);
        }

        if ($user->otp !== $otp) {
            return response()->json(['message' => 'Invalid OTP'], 422);
        }

        $otpCreationTime = Carbon::parse($user->otp_created_at);
        $now = Carbon::now();
        $otpExpirationMinutes = 10; // Set OTP expiration time (e.g., 10 minutes)

        if ($otpCreationTime->diffInMinutes($now) > $otpExpirationMinutes) {
            return response()->json(['message' => 'OTP has expired'], 422);
        }

        // Clear OTP from user record after successful verification
        $user->otp = null;
        $user->otp_created_at = null;
        $user->save();

        // Log the user in or generate JWT token if applicable

        return response()->json(['message' => 'OTP verified successfully'], 200);
    }

    public function setNewPassword(Request $request)
    {
        $request->validate([
            'email' => 'required|email|exists:users,email',
            'newPassword' => 'required|string|min:6',
        ]);

        $email = $request->input('email');
        $newPassword = $request->input('newPassword');

        // Find the user by email
        $user = User::where('email', $email)->first();

        // Update user's password
        $user->password = Hash::make($newPassword);
        $user->save();

        return response()->json(['message' => 'Password reset successfully'], 200);
    }
}
