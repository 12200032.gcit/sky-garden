import React, { useState, useEffect } from "react";
import "../styles/nav.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBars, faTimes } from "@fortawesome/free-solid-svg-icons";
import { NavLink } from "react-router-dom";

const MobileNavbar = () => {
    const [mobileNavIsOpen, setMobileNavIsOpen] = useState(false);

    // Toggle the Mobile Nav open / close
    const handleMobileNavClick = () => {
        setMobileNavIsOpen(!mobileNavIsOpen);
    };

    useEffect(() => {
        const handleResize = () => {
            if (window.innerWidth > 600) {
                setMobileNavIsOpen(false);
            }
        };

        window.addEventListener("resize", handleResize);
        return () => window.removeEventListener("resize", handleResize);
    }, []);

    return (
        <nav className="mobileNav_container">
            <div className="d-flex">
                <div className="logo"> <img src="../img/logo.jpg" width="100%" alt="a logo" /></div>
                <div className="logotext"><span className="logotext1">Sky</span><span className="logotext2">Garden</span></div>
            </div>
            <div>
                <FontAwesomeIcon
                    className="menu_icon"
                    icon={mobileNavIsOpen ? faTimes : faBars}
                    onClick={handleMobileNavClick}
                    style={{ zIndex: "200", fontSize: "3vw", width: "6vw" }}
                />
            </div>

            <div className="mobileNav_menu_container">
                <ul
                    className={`${mobileNavIsOpen ? "mobileNav_open" : "mobileNav_closed"} mobileNav_menu_list`}
                >
                    <li> <NavLink to="/nav" style={{ color: "inherit", textDecoration: "none" }}>Home</NavLink></li>
                    <li> <NavLink to="/menu" style={{ color: "inherit", textDecoration: "none" }}>Menu</NavLink></li>
                    <li><NavLink to="/event" style={{ color: "inherit", textDecoration: "none" }}>Events</NavLink></li>
                    <li><NavLink to="/book-table" style={{ color: "inherit", textDecoration: "none" }}>Book Table</NavLink></li>
                </ul>
            </div>
        </nav>
    );
};

export default MobileNavbar;
