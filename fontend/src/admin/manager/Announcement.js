import React, { useState, useEffect } from 'react';
import SideBar from './SideBar';
import Topbar from '../Topbar';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import './css/E&A.css';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowLeft, faDeleteLeft } from '@fortawesome/free-solid-svg-icons';

const Announcement = () => {
    const [announcement, setAnnouncement] = useState('');
    const [announcements, setAnnouncements] = useState([]);

    useEffect(() => {
        fetchAnnouncements();
    }, []);

    const fetchAnnouncements = async () => {
        try {
            const response = await fetch('https://sky-garden.ugyen1234.serv00.net/api/get-announcements');
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            const data = await response.json();
            setAnnouncements(data);
        } catch (error) {
            console.error('Error fetching announcements:', error);
        }
    };

    const addAnnouncement = async () => {
        if (announcement.trim() === '') {
            toast.error('Announcement cannot be empty');
            return;
        }

        const requestBody = { announcement };

        try {
            const response = await fetch(`https://sky-garden.ugyen1234.serv00.net/api/store-announcements`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(requestBody),
            });

            if (!response.ok) {
                const errorData = await response.json();
                toast.error(`Error: ${errorData.message}`);
                return;
            }

            const newAnnouncement = await response.json();
            setAnnouncements([...announcements, newAnnouncement]);
            setAnnouncement('');
            toast.success('Announcement added successfully!');
        } catch (error) {
            toast.error('Error adding announcement');
            console.error('Error adding announcement:', error);
        }
    };

    const updateStatus = async (id) => {
        try {
            const response = await fetch(`https://sky-garden.ugyen1234.serv00.net/api/update-announcements/${id}/status`, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                },
            });

            if (!response.ok) {
                toast.error("Error updating status");
                return;
            }

            fetchAnnouncements();
            toast.success('Announcement status updated!');
        } catch (error) {
            toast.error('Error updating status');
        }
    };

    const handleDelete = async (id) => {
        try {
           
            const response = await fetch(`https://sky-garden.ugyen1234.serv00.net/api/delete-announcement/${id}`, {
                method: 'DELETE',
            });

            if (response.status === 400) {
                toast.error("Active announcement cannot be deleted", {
                    autoClose: 3000,
                });
                return;
            }

            if (!response.ok) {
                toast.error("Failed to delete announcement");
                return;
            }

            setAnnouncements(announcements.filter(ann => ann.id !== id));
            toast.success('Announcement deleted successfully!');
        } catch (error) {
            toast.error('Failed to delete announcement');
        }
    };

    return (
        <div className="d-flex">
            <SideBar />
            <div className="p-4" style={{ width: "93%" }}>
                <Topbar />
                <div className="border title_bar">
                    <p>Announcement</p>
                </div>
                <div className="input-group">
                    <input
                        type="text"
                        value={announcement}
                        onChange={(e) => setAnnouncement(e.target.value)}
                        className="announcement-input"
                        placeholder="Enter announcement"
                    />
                    <button onClick={addAnnouncement} className="add-button">
                        Add Announcement
                    </button>
                </div>

                <ul className="announcements-list">
                    {announcements.map((ann) => (
                        <li key={ann.id} className="announcement-item d-flex justify-content-between">
                            <div>
                                <input
                                    type="radio"
                                    checked={ann.status}
                                    onChange={() => updateStatus(ann.id)}
                                    className="status-radio"
                                />
                                {ann.announcement}
                            </div>

                            <FontAwesomeIcon
                                style={{ cursor: "pointer", fontSize: "20px", marginLeft: "20px", color: "rgba(255, 0, 0, 1)", display: "end" }}
                                icon={faDeleteLeft}
                                onClick={() => { handleDelete(ann.id) }}
                            />
                        </li>
                    ))}
                </ul>
                <div className="col-md-9" style={{ marginTop: "20px" }}>
                    <Link to="/events-and-anouncement" style={{ textDecorationLine: "none" }}>
                        <span className='go-back'>
                            <FontAwesomeIcon className='icon' icon={faArrowLeft} />
                            Go back
                        </span>
                    </Link>
                </div>
            </div>
        </div>
    );
};

export default Announcement;
