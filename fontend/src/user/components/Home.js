import React from 'react';
import UserTabs from './UserTabs';
import "../styles/userTab.css";



function Home() {
  return (
    <div>
      <div className='d-flex user-tab'>
        <div className='p-4' style={{ width: '100%' }}>
          <div><UserTabs /></div>
        </div>

      </div>

    </div>
  );
}

export default Home;
