import React from "react";
import "../styles/singlefood.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faWhatsapp } from "@fortawesome/free-brands-svg-icons";
import Footer from "./Footer";

function SingleFoodCard({ data }) {
    const { food_name, price, description, image, discount, offer_status, after_discount } = data;

    const handleOrderClick = () => {
        const phoneNumber = "97517485966"; // Replace with your phone number in international format
        const message = `Hello, I would like to order ${food_name} for Nu. ${price} `;
        const whatsappUrl = `https://wa.me/${phoneNumber}?text=${encodeURIComponent(message)}`;
        window.location.href = whatsappUrl;
    };

    return (
        <>
            <div className="d-flex col-lg-12 singleFood-mainContainer">
                <div className="single-food-Container1 col-lg-6">
                    <img src={`https://sky-garden.ugyen1234.serv00.net/storage/${image}`} style={{ width: "100%" }} alt={food_name} />
                </div>
                <div className="single-food-Container2 col-lg-6">
                    <div className="d-flex singleFood-des1" style={{ height: "20%" }}>
                        <div className="singleFood-name" style={{ marginLeft: '60px' }}>
                            {offer_status && discount > 0 ? (
                                <div className="discount-label">
                                    <p>{discount}% off</p>
                                </div>
                            ) : null}
                            <p>{food_name}</p>
                        </div>

                        <div className="singleFood-price">
                            {offer_status ? (
                                <div className="d-flex">
                                    <p><span>Nu</span>{after_discount}</p>
                                    <s style={{ marginLeft: '1vw', marginTop:'2.5vw', color:'white' }}>Nu{price}</s>
                                </div>
                            ) : (
                                <p><span>Nu</span>{price}</p>
                            )}
                        </div>
                    </div>

                    <div style={{ height: "20%" }} className="d-flex singleFood-des2">
                        <div className="singleFood-description"><p>{description}</p></div>
                    </div>

                    <div style={{ height: "40%" }} className="d-flex singleFood-step">
                        <div className="singleFood-step1" style={{ width: "30%", marginLeft: "1vw" }}>
                            <div><span className="singleFood-step-no">01</span></div>
                            <div><p>Tap on the whatsapp icon and place order</p></div>
                            <div className="singleFood-step1-text"><p>Enjoy your favorite dish by simply clicking on the WhatsApp icon to place your order</p></div>
                        </div>

                        <div className="singleFood-step2" style={{ width: "30%", marginLeft: "1vw" }}>
                            <div><span className="singleFood-step-no">02</span></div>
                            <div><p>Provide your phone number and address</p></div>
                            <div className="singleFood-step1-text"><p>Kindly specify the quantity and name of the dish, and then provide your accurate location.</p></div>
                        </div>

                        <div className="singleFood-step3" style={{ width: "30%", marginLeft: "1vw" }}>
                            <div><span className="singleFood-step-no">03</span></div>
                            <div><p>Enjoy your favorite food at home</p></div>
                            <div className="singleFood-step1-text"><p>Indulge in your dish from the comfort of your home.</p></div>
                        </div>
                    </div>

                    <div className="d-flex order-button" style={{ width: "60%", height: "15%" }} onClick={handleOrderClick}>
                        <div className="order-button-text-container"><p className="order-button-text">Order Now</p></div>
                        <div style={{ fontSize: "5vw", color: "white", paddingLeft: "2vw" }}><FontAwesomeIcon icon={faWhatsapp} /></div>
                    </div>
                </div>
            </div>
            <div style={{ marginLeft: "1vw", marginRight: "3vw" }}><Footer /></div>
        </>
    );
}

export default SingleFoodCard;
