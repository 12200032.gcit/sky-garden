import React from 'react';
import { Link, useLocation } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUsersGear, faGears } from '@fortawesome/free-solid-svg-icons';
import "../manager/css/SideBar.css";

function AdminSideBar() {
    const location = useLocation();

    // Function to determine if the given path matches the current URL
    const isActive = (path) => {
        return location.pathname === path;
    };

    return (
        <div className='sidebar'>
            <ul style={{ listStyle: 'none', paddingInline: "14px" }}>
                <div className='pb-4 pt-3'>
                    <img style={{ width: "75px" }} alt='sky garden' src='/img/logo.jpg'></img>
                </div>
                <Link to="/admin-dashboard" className="sidebar-link">
                    <li li className={isActive("/admin-dashboard") || isActive("/add-manager")  ? "active" : ""}>
                        <FontAwesomeIcon className='icon' icon={faUsersGear} />Manager
                    </li>
                </Link>

                <Link to="/admin-setting" className="sidebar-link">
                    <li className={isActive("/admin-setting") ? "active" : ""}>
                        <FontAwesomeIcon className='icon' icon={faGears} /><br />Setting
                    </li>
                </Link>

            </ul>

        </div>
    )
}

export default AdminSideBar;
