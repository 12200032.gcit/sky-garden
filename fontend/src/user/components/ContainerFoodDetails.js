import React from "react";
import Navbar from "./Navbar";
import MobileNavbar from "./MoblieNavbar";
import Marquee from "./SlidingWord";
import SingleFoodMark from "./SingleFoodMark";
import Whatsapp from "./WhatsAppIcon";




const ContainerFoodDetails = (props) => {
  return (
    <div>
      <Marquee />
      <Navbar />
      <MobileNavbar />
      <div style={{ position: "fixed", top: "40vw", right: "7vw", zIndex: "50" }}>
        <Whatsapp />
      </div>
      <SingleFoodMark name={props.itemName}/>
     

    </div>
  );
};
export default ContainerFoodDetails;
