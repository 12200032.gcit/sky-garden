import React from "react";
import { NavLink, useLocation } from "react-router-dom";
import "../styles/nav.css";

const Navbar = () => {
  const location = useLocation();
  const currentPath = location.pathname;

  return (
    <nav className="navbar">
      <div className="d-flex">
        <div className="logo">
          <img src="../img/logo.jpg" width="100%" alt="a logo" />
        </div>
        <div className="logotext">
          <span className="logotext1">Sky</span><span className="logotext2">Garden</span>
        </div>
      </div>
      <div className="nav_links">
        <ul className="menu_list">
          <li className={currentPath === "/nav" ? "active" : ""}>
            <NavLink to="/nav" style={{ color: "inherit", textDecoration: "none" }}>Home</NavLink>
          </li>
          <li className={currentPath === "/menu" ? "active" : ""}>
            <NavLink to="/menu" style={{ color: "inherit", textDecoration: "none" }}>Menu</NavLink>
          </li>
          <li className={currentPath === "/event" ? "active" : ""}>
            <NavLink to="/event" style={{ color: "inherit", textDecoration: "none" }}>Events</NavLink>
          </li>
          <li className={`book_table ${currentPath === "/book-table" ? "active" : ""}`}>
            <NavLink to="/book-table" style={{ color: "inherit", textDecoration: "none" }}>Book Table</NavLink>
          </li>
        </ul>
      </div>
    </nav>
  );
};

export default Navbar;
