import React, { useEffect, useState } from 'react';
import { BrowserRouter as Router, Routes, Route, Navigate, useLocation } from 'react-router-dom';
import Food from './admin/manager/Food';
import EventAnounce from './admin/manager/EventAnounce';
import Table from './admin/manager/Table';
import Video from './admin/manager/Video';
import AddEventOffer from './admin/manager/AddEventOffer';
import EditEventOffer from './admin/manager/EditEventOffer';
import Edit from './admin/manager/components/Edit';
import EditFoodItem from './admin/manager/components/EditItems';
import AdminDashboard from './admin/superadmin/AdminDashboard';
import AdminSetting from './admin/superadmin/AdminSettings';
import AddManager from './admin/superadmin/AddManager';
import ProfileSetting from './admin/manager/ProfileSetting';
import DiscountTable from './admin/manager/components/DiscountTable';
import AddFoodCategory from './admin/manager/components/AddFoodCategory';
import AddFoodItem from './admin/manager/components/AddFoodItem';
import Login from './admin/Login';
import Home from './user/components/Home';
import NavContainer from './user/components/NavContainer';
import ResetPassword from './admin/ResetPassword';
import ContainerMenu from './user/components/ContainerMenu';
import FoodDetails from './user/components/FoodDetails';
import SingleEventDetails from './user/components/SingleEventDetail';
import AllEventDetails from './user/components/AllEvents';
import BookTableForm from './user/components/BookTableContainer';
import Announcement from './admin/manager/Announcement';

function App() {
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const [isAdminLoggedIn, setIsAdminLoggedIn] = useState(false);
  const [currentPath, setCurrentPath] = useState('/food'); // Initial state set to /food
  const token = localStorage.getItem('token');
  const user = JSON.parse(localStorage.getItem('user'));

  const location = useLocation(); // Use useLocation to get the current path

  useEffect(() => {
    handleLogin();
  }, [token]); // Only re-run the effect when the token changes

  useEffect(() => {
    // Redirect to appropriate paths based on user login status and current path
    if (isLoggedIn && location.pathname === "/" && !isAdminLoggedIn) {
      setCurrentPath('/food'); // Redirect to /food if logged in, not admin, and current path is /
    } else if (isAdminLoggedIn && location.pathname === "/") {
      setCurrentPath('/admin-dashboard'); // Redirect to /admin-dashboard if admin and current path is /
    } else {
      setCurrentPath(location.pathname);
    }
  }, [isLoggedIn, isAdminLoggedIn, location.pathname]);

  const handleLogin = () => {
    if (token) {
      if (user && user.email === "skygarden.adm7@gmail.com" && user.role === "superadmin") {
        setIsAdminLoggedIn(true);
      }
      setIsLoggedIn(true);
    } else {
      setIsLoggedIn(false);
      setIsAdminLoggedIn(false);
    }
  };

  return (
    <Routes>
      <Route path="/nav" element={<NavContainer />} />
      <Route path="/menu" element={<ContainerMenu />} />
      <Route path="/event" element={<AllEventDetails />} />
      <Route path="/book-table" element={<BookTableForm />} />
      <Route path="/event-details/:id" element={<SingleEventDetails />} />
      <Route path="/get-food-details/:id" element={<FoodDetails />} />

      {/* Admin routes */}
      <Route
        path="/"
        element={
          isLoggedIn ? (
            isAdminLoggedIn ? <Navigate to={currentPath} /> : <Navigate to={currentPath} />
          ) : (
            <Login onLogin={handleLogin} />
          )
        }
      />
      <Route path="/admin-dashboard" element={isAdminLoggedIn ? <AdminDashboard /> : <Navigate to="/" />} />
      <Route path="/admin-setting" element={isAdminLoggedIn ? <AdminSetting /> : <Navigate to="/" />} />
      <Route path="/add-manager" element={isAdminLoggedIn ? <AddManager /> : <Navigate to="/" />} />
      <Route
        path="/forgot-password-email-&-opt"
        element={
          isLoggedIn ? (
            isAdminLoggedIn ? <Navigate to="/admin-dashboard" /> : <Navigate to="/food" />
          ) : (
            <ResetPassword />
          )
        }
      />

      {/* Manager routes */}
      <Route path="/food" element={isLoggedIn ? <Food /> : <Navigate to="/" />} />
      <Route path="/category-edit/:id" element={isLoggedIn ? <Edit /> : <Navigate to="/" />} />
      <Route path="/edit-food-items/:id" element={isLoggedIn ? <EditFoodItem /> : <Navigate to="/" />} />
      <Route path="/events-and-anouncement" element={isLoggedIn ? <EventAnounce /> : <Navigate to="/" />} />
      <Route path="/table" element={isLoggedIn ? <Table /> : <Navigate to="/" />} />
      <Route path="/video" element={isLoggedIn ? <Video /> : <Navigate to="/" />} />
      <Route path="/profile-setting" element={isLoggedIn ? <ProfileSetting /> : <Navigate to="/" />} />
      <Route path="/add-event-and-offer" element={isLoggedIn ? <AddEventOffer /> : <Navigate to="/" />} />
      <Route path="/edit-event-and-offer/:id" element={isLoggedIn ? <EditEventOffer /> : <Navigate to="/" />} />
      <Route path="/discount" element={isLoggedIn ? <DiscountTable /> : <Navigate to="/" />} />
      <Route path="/addfoodcategory" element={isLoggedIn ? <AddFoodCategory /> : <Navigate to="/" />} />
      <Route path="/addfoodItem" element={isLoggedIn ? <AddFoodItem /> : <Navigate to="/" />} />
      <Route path="/home" element={<Home />} />
      <Route path="/announcement" element={isLoggedIn ? <Announcement /> : <Navigate to="/" />} />
    </Routes>
  );
}

function AppWrapper() {
  return (
    <Router>
      <App />
    </Router>
  );
}

export default AppWrapper;
