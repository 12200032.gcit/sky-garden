import React, { useState } from "react";
import SideBar from "./AdminSidebar";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowLeft } from "@fortawesome/free-solid-svg-icons";
import "./css/addmanager.css";
import "react-toastify/dist/ReactToastify.css";
import { Link, useNavigate } from 'react-router-dom';
import Topbar from "../Topbar";
import { toast } from "react-toastify";

function AddManager() {
  const [name, setName] = useState("");
  const [contact, setContact] = useState("");
  const [email, setEmail] = useState("");
  const navigate = useNavigate();
  const [loading, setLoading] = useState(false);

  const validatePhoneNumber = (phoneNumber) => {
    const regex = /^(17|77)\d{6}$/;
    return regex.test(phoneNumber);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    if (!validatePhoneNumber(contact)) {
      toast.error("Contact number must be 8 digits and start with 17 or 77");
      return;
    }
    setLoading(true);
    try {
      const response = await fetch('https://sky-garden.ugyen1234.serv00.net/api/add-manager', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          name,
          contact,
          email,
        }),
      });

      if (response.ok) {
        toast.success("Manager added successfully!");
        setName("");
        setContact("");
        setEmail("");
        navigate("/add-manager");
      } else if (response.status === 409) {
        toast.error("Email already exists. Please use a different email.");
      } else {
        toast.error('Failed to add manager');
      }
    } catch (error) {
      toast.error('Failed to add manager');
    }
    finally {
      setLoading(false);
    }
  };

  return (
    <div className='d-flex'>
      <SideBar />
      <div className='p-4' style={{ width: '93%' }}>
        <Topbar />
        <div className='border title_bar'>
          <p>Add Manager</p>
        </div>
        <form onSubmit={handleSubmit}>
          <div className="form">
            <div className="row">
              <div className="col-md-12">
                <div className='d-flex'>
                  <div className='col-md-6' style={{ marginRight: "15px" }}>
                    <label>Name<span style={{ color: "red" }}>*</span></label>
                    <input
                      type="text"
                      autoFocus
                      placeholder='Enter Name'
                      className="form-control"
                      value={name}
                      onChange={(e) => setName(e.target.value)}
                      required
                    />
                  </div>
                  <div className='col-md-6' >
                    <label>Contact<span style={{ color: "red" }}>*</span></label>
                    <input
                      type='text'
                      placeholder='Enter Contact'
                      className="form-control"
                      value={contact}
                      onChange={(e) => setContact(e.target.value)}
                      required
                    />
                  </div>
                </div>
                <div className='d-flex' style={{ paddingBottom: "15px" }}>
                  <div className='col-md-8'>
                    <label>Email <span style={{ color: "red" }}>*</span></label>
                    <input
                      type="email"
                      placeholder='Enter Email'
                      className="form-control"
                      value={email}
                      onChange={(e) => setEmail(e.target.value)}
                      required
                    />
                  </div>
                </div>
              </div>
            </div>
            <div className="row mt-4">
              <div className="col-md-10">
                <Link to="/admin-dashboard" style={{ textDecorationLine: "none" }}>
                  <span className='go-back'>
                    <FontAwesomeIcon className='icon' icon={faArrowLeft} />
                    Go back
                  </span>
                </Link>
              </div>
              <div className="col-md-2 d-flex justify-content-between">
                <button type="button" className="btn reset" onClick={() => { setName(''); setContact(''); setEmail(''); }}>Reset</button>
                <button type="submit" className="btn add" disabled={loading}>  {loading ? 'Adding...' : 'Add'}</button>

              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
}

export default AddManager;
