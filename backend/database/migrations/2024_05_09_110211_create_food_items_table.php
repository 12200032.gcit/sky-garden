<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFoodItemsTable extends Migration
{
    public function up()
    {
        Schema::create('food_items', function (Blueprint $table) {
            $table->id();
            $table->string('food_category');
            $table->string('food_name');
            $table->integer('price');
            $table->text('description')->nullable();
            $table->string('image')->nullable();
          
        });
    }

    public function down()
    {
        Schema::dropIfExists('food_items');
    }
}
