import { useState, useEffect } from 'react';
import Carousel from 'react-bootstrap/Carousel';
import HorizontalCard from './HorizontalCard';
import "../styles/carousel.css"
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

function ControlledCarousel() {
    const [index, setIndex] = useState(0);
    const [events, setEvents] = useState([]);


    useEffect(() => {
        fetchEvents();
    }, []);

    const fetchEvents = async () => {
        try {
            const response = await fetch("https://sky-garden.ugyen1234.serv00.net/api/get-all-events");
            if (!response.ok) {
                throw new Error("Failed to fetch events");
            }
            const data = await response.json();
            const formattedEvents = data.map((event) => {
                const date = new Date(event.date).toLocaleDateString("en-US", {
                    year: "numeric",
                    month: "short",
                    day: "numeric",
                });
                const timeParts = event.time.split(":");
                let hours = parseInt(timeParts[0]);
                const minutes = timeParts[1];
                const ampm = hours >= 12 ? "PM" : "AM";
                hours = hours % 12 || 12;
                const formattedTime = `${hours}:${minutes} ${ampm}`;
                return {
                    ...event,
                    date,
                    time: formattedTime,
                };
            });
            if (response.ok) {
                setEvents(formattedEvents);
            }
        } catch (error) {
            toast.error("Failed to fetch data", {
                autoClose: 3000,
            });
        }
    };

    const handleSelect = (selectedIndex, e) => {
        setIndex(selectedIndex);
    };

    return (
        <Carousel activeIndex={index} onSelect={handleSelect} indicators={false}>
            {events.map((event) => (<Carousel.Item>
                <HorizontalCard
                    imageSrc={"https://sky-garden.ugyen1234.serv00.net/storage/" + event.image}
                    title={event.title}
                    text={event.description}
                    subtitle={event.title}
                    id={event.id}
                />
            </Carousel.Item>))}
        </Carousel>
    );
}

export default ControlledCarousel;
