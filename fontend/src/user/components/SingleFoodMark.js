import React from "react";
import "../styles/mark.css";
import "../styles/menuMark.css";




function SingleFoodMark(props) {
    return (
        <>
        <div>
            <div className="Menu-mark-container">
                <div className="menu-text1">
                    <p>CUSTOMER FAVOURITES </p>
                </div>
                <div className="menu-text2">
                    <p>{props.name}</p>
                </div>
            </div>
            <div className="daily-container d-flex" style={{marginTop:"-4vw"}}>
                <div className="blue-mark" style={{marginTop:"-13vw"}}></div>
                <div className="mark-daily-title" style={{marginTop:"-13vw"}}>
                    <p>Details</p>
                </div>
                <div className="pattern-img">
                    <img src="/img/pattern.png" alt="a logo" className="pattern"  style={{width:"17vw", marginTop:"-6vw"}} />
                </div>
            </div>
        </div>
        </>
    );
}

export default SingleFoodMark;
