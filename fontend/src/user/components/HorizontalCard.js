import React from 'react';
import { Card, CardImg, CardBody, CardTitle, CardText, Button } from 'reactstrap';
import "../styles/horizontalCard.css";
import { Link } from "react-router-dom";

// Utility function to truncate text
function truncateText(text, maxLength) {
    if (text.length > maxLength) {
        return text.substring(0, maxLength) + '...';
    }
    return text;
}

function HorizontalCard({ imageSrc, title, text, id }) {
    const maxLength = 100; // Set your desired max length for truncation

    return (
        <Card className="my-2 d-flex flex-row" style={{ height: '20vw', border: 'none' }}>
            <CardImg
                alt="Card image cap"
                src={imageSrc}
                style={{
                    width: '50%',
                    height: '100%',
                    objectFit: 'cover'
                }}
            />
            <CardBody className="d-flex flex-column justify-content-between" style={{ marginLeft: '3vw' }}>
                <div>
                    <CardTitle className='offer-title'>{title}</CardTitle>
                    <CardText className='offer-card-text'>{truncateText(text, 250)}</CardText>
                </div>
                <Link to={`/event-details/${id}`} style={{textDecoration:"none"}}>
                    <div className="d-flex justify-content-center">
                        <Button className='learn-more'>View All Post</Button>
                    </div>
                </Link>
            </CardBody>
        </Card>
    );
}

export default HorizontalCard;
