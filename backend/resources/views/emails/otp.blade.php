<!DOCTYPE html>
<html>
<head>
    <title>Your OTP Code</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f4f4f4;
            margin: 0;
            padding: 0;
        }
        .email-container {
            max-width: 600px;
            margin: 20px auto;
            background-color: #ffffff;
            padding: 20px;
            border-radius: 10px;
            box-shadow: 0 0 15px rgba(0, 0, 0, 0.1);
        }
        .header {
            text-align: center;
            padding: 20px 0;
        }
        .header img {
            max-width: 180px;
        }
        .content {
            padding: 20px;
            color: #333333;
        }
        .content p {
            font-size: 16px;
            line-height: 1.6;
            margin: 15px 0;
            color: #333333;
        }
        .content .otp {
            font-size: 18px;
            font-weight: bold;
            color: #d9534f;
        }
        .footer {
            text-align: center;
            padding: 20px 0;
            border-top: 1px solid #eaeaea;
            font-size: 14px;
            color: #777777;
        }
        .footer a {
            color: #777777;
            text-decoration: none;
        }
    </style>
</head>
<body>
    <div class="email-container">
        <div class="header">
            <a href="https://ibb.co/2Sg49MM"><img src="https://i.ibb.co/2Sg49MM/sky-garden.jpg" alt="sky-garden" border="0"></a>
        </div>
        <div class="content">
            <p>Dear {{ $user->name }},</p>
            <p>We have received a request to verify your email address. Please use the following OTP to complete your verification process:</p>
            <p class="otp">{{ $otp }}</p>
            <p>This OTP is valid for a limited time. If you did not request this OTP, please ignore this email.</p>
            <p>If you have any questions or need further assistance, feel free to reach out to our support team.</p>
            <p>Best regards,<br>The Sky Garden Team</p>
        </div>

    </div>
</body>
</html>
