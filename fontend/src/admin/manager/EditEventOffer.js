import React, { useState, useRef, useEffect } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons';
import SideBar from './SideBar';
import "./css/Form.css"
import { Link, useParams } from 'react-router-dom';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Topbar from '../Topbar';

function EditEventOffer() {
  const { id } = useParams();
  const [eventTitle, setEventTitle] = useState('');
  const [date, setDate] = useState('');
  const [time, setTime] = useState('');
  const [description, setDescription] = useState('');
  const [previewURL, setPreviewURL] = useState(null);
  const [backPreviewURL, setBackPreviewURL] = useState(null);
  const [loading, setLoading] = useState(false);
  const fileInputRef = useRef(null);

  const fetchEventData = async () => {

    try {
      const response = await fetch(`https://sky-garden.ugyen1234.serv00.net/api/get-events/${id}`);
      if (!response.ok) {
        throw new Error('Failed to fetch data');
      }
      const eventData = await response.json();
      setEventTitle(eventData.title);
      setDate(eventData.date);
      setTime(eventData.time);
      setDescription(eventData.description);
      setBackPreviewURL(eventData.image);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    fetchEventData();
  }, []);

  const handleFileChange = (event) => {
    const file = event.target.files[0];
    previewFile(file);
  };

  const previewFile = (file) => {
    if (file) {
      const filePreviewURL = URL.createObjectURL(file);
      setPreviewURL(filePreviewURL);
      console.log(previewURL)
    } else {
      setPreviewURL(null);
    }
  };

  const handleReset = () => {
    setEventTitle(eventTitle);
    setDate(date);
    setTime(time);
    setDescription(description);
    setPreviewURL(null);
    fileInputRef.current.value = '';
    fetchEventData()
  };

  const handleDrop = (event) => {
    event.preventDefault();
    const file = event.dataTransfer.files[0];
    previewFile(file);
  };

  const handleEditEvent = async (e) => {
    e.preventDefault();
    setLoading(true);
    try {
      const file = fileInputRef.current.files[0];
      const formData = new FormData();
      formData.append('title', eventTitle);
      formData.append('date', date);
      formData.append('time', time);
      formData.append('description', description);
      formData.append('image', file);

      const response = await fetch(`https://sky-garden.ugyen1234.serv00.net/api/update-events/${id}`, {
        method: 'POST',
        body: formData
      });

      if (!response.ok) {
        throw new Error('Failed to edit event');
      }

      toast.success("Edited successfully!", {
        autoClose: 3000,
      });
      fetchEventData();

    } catch (error) {
      toast.error("Failed to edit event!", {
        autoClose: 3000,
      });
    }
    finally {
      setLoading(false);
    }
  };

  const getTodayDate = () => {
    const now = new Date();
    const year = now.getFullYear();
    const month = String(now.getMonth() + 1).padStart(2, '0');
    const day = String(now.getDate()).padStart(2, '0');
    return `${year}-${month}-${day}`;
  };

  const getCurrentTime = () => {
    const now = new Date();
    return now.toTimeString().slice(0, 5);
  };

  const today = getTodayDate();
  const currentTime = getCurrentTime();

  return (
    <div className='d-flex'>
      <SideBar />
      <div className='p-4' style={{ width: '93%' }}>
        <Topbar />
        <div className='border title_bar'>
          <p>Edit Event and Announcement</p>
        </div>
        <form className="form" onSubmit={handleEditEvent}>
          <div className="row">
            <div className="col-md-8">
              <div className='d-flex'>
                <div className='col-md-12'>
                  <label htmlFor="eventTitle">Event Title <span style={{ color: "red" }}>*</span></label>
                  <input
                    type="text"
                    autoFocus
                    id="eventTitle"
                    value={eventTitle}
                    placeholder='Enter title of the events'
                    onChange={(e) => setEventTitle(e.target.value)}
                    className="form-control"
                    required
                  />
                </div>

              </div>
              <div className='d-flex'>
                <div className='col-md-6' style={{ paddingRight: "15px" }}>
                  <label htmlFor="date">Date <span style={{ color: "red" }}>*</span></label>
                  <input
                    type='date'
                    id="date"
                    value={date}
                    onChange={(e) => setDate(e.target.value)}
                    className="form-control"
                    required
                    min={today}
                  />
                </div>
                <div className='col-md-6' style={{ paddingLeft: "15px" }}>
                  <label htmlFor="time">Time <span style={{ color: "red" }}>*</span></label>
                  <input
                    type="time"
                    id="time"
                    value={time}
                    onChange={(e) => setTime(e.target.value)}
                    className="form-control"
                    required
                    min={date === today ? currentTime : "00:00"}
                  />
                </div>
              </div>
              <label htmlFor="description">Description <span style={{ color: "red" }}>*</span></label>
              <textarea
                id="description"
                value={description}
                placeholder='Write the description of the event'
                onChange={(e) => setDescription(e.target.value)}
                className="form-control"
                required
              ></textarea>
            </div>
            <div className="col-md-4" style={{ paddingLeft: "70px", paddingBlock: "20px" }}>
              <label htmlFor="imageUpload">Upload Image <span style={{ color: "red" }}>*</span></label>
              <div
                className="upload-area"
                onDrop={handleDrop}
                style={{ cursor: "pointer" }}
                onDragOver={(event) => event.preventDefault()}
                onClick={() => fileInputRef.current.click()}
              >
                {previewURL ? (
                  <img
                    src={previewURL}
                    alt="New Preview"
                    className="preview-image"
                  />
                ) : (
                  backPreviewURL && (
                    <img
                      src={`https://sky-garden.ugyen1234.serv00.net/storage/${backPreviewURL}`}
                      alt="Initial Preview"
                      className="preview-image"
                    />
                  )
                )}
                <input
                  type="file"
                  id="imageUpload"
                  ref={fileInputRef}
                  accept="image/*"
                  onChange={handleFileChange}
                  style={{ display: 'none' }}
                />
              </div>
            </div>
          </div>
          <div className="row mt-4">
            <div className="col-md-10">
              <Link to="/events-and-anouncement" style={{ textDecorationLine: "none" }}>
                <span className='go-back'>
                  <FontAwesomeIcon className='icon' icon={faArrowLeft} />
                  Go back
                </span>
              </Link>
            </div>
            <div className="col-md-2 d-flex justify-content-between">
              <button className="btn reset" type="button" onClick={handleReset}>Reset</button>
              <button className="btn add" type='submit' disabled={loading}> {loading ? 'Saving...' : 'Save'}</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
}

export default EditEventOffer;
