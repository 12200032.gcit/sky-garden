import React from "react";
import "../styles/nav.css";
import Navbar from "./Navbar";
import MobileNavbar from "./MoblieNavbar";
import CalltoAction from "./CallToAction";
import Marquee from "./SlidingWord";
import DailyVideo from "./DailyVideo";
import Home from "./Home";
import Mark from "./Mark";
import Whatsapp from "./WhatsAppIcon";

const NavContainer = () => {
  return (
    <div style={{ position: "relative" }}> {/* Make the container relative */}
      <Marquee />
      <Navbar />
      <MobileNavbar />
      <CalltoAction />
      <div style={{ position: "fixed", top: "40vw", right: "7vw", zIndex: "50" }}>
        <Whatsapp />
      </div>
      <DailyVideo />
      <Mark />
      <Home />
    </div>
  );
};

export default NavContainer;
