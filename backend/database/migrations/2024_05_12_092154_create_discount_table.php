<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDiscountTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() : void
    {
        Schema::create('discounts', function (Blueprint $table) {
            $table->id();
            $table->string('food_category');
            $table->string('food_name');
            $table->integer('discount')->default(0);
            $table->integer('actual_price');
            $table->integer('after_discount');
            $table->boolean('offer_status')->default(false);
            $table->timestamps();
        });
    }

    public function down() : void
    {
        Schema::dropIfExists('discount');
    }
}
