
import React from 'react';
import SideBar from './SideBar';
import CustomTabs from './components/Tab';
import Topbar from '../Topbar';

function Food() {
  return (
    <div className='d-flex'>
      <SideBar />
      <div className='p-4' style={{ width: '93%' }}>
        <Topbar />
        <div className='border title_bar'>
          <p>Foods</p>
        </div>
        <CustomTabs />
      </div>
    </div>
  );
}

export default Food;
