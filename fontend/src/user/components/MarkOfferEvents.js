import React from "react";
import "../styles/offerMark.css";
import ControlledCarousel from "./EventsAndOffer";


function MarkOfferEvents() {
    return (
        <div className="offer-mainContainer">
            <div className="offer-container">
                <div className="offer-blue-mark"></div>
                <div className="offer-title">
                    <p>OFFERS & EVENTS</p>
                </div>
            </div>
            <div className="offer-slider">
                <ControlledCarousel/>
               
            </div>
        </div>
    );
}

export default MarkOfferEvents;
