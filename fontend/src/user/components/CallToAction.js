import React from "react";
import "../styles/cta.css";
import { NavLink } from "react-router-dom";

const CalltoAction = () => {
    return (
        <div className="cta">
            <div className="cta-background"></div>
            <div className="cta-content">
                <NavLink to="/book-table" style={{ color: "inherit", textDecoration: "none" }}> <button className="cta-button">Book Table</button></NavLink>
                <NavLink to="/menu" style={{ color: "inherit", textDecoration: "none" }}>  <button className="cta-button">Explore Menu</button></NavLink>
            </div>
        </div>
    );
};

export default CalltoAction;
