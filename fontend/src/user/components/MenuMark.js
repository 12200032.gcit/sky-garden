import React from "react";
import "../styles/mark.css";
import "../styles/menuMark.css";



function MenuMark() {
    return (
        <div>
            <div className="Menu-mark-container">
                <div className="menu-text1">
                    <p>CUSTOMER FAVOURITES </p>
                </div>
                <div className="menu-text2">
                    <p>Popular Categories</p>
                </div>
            </div>
            <div className="daily-container d-flex" style={{marginTop:"-8vw"}}>
                <div className="blue-mark"></div>
                <div className="mark-daily-title">
                    <p>Menu</p>
                </div>
                <div className="pattern-img">
                    <img src="/img/pattern.png" alt="a logo" className="pattern" />
                </div>
            </div>
        </div>
    );
}

export default MenuMark;
