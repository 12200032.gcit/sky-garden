<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Discount;
use App\Models\FoodItems;

class CategoryController extends Controller
{
    function addCategory(Request $req)
    {
        $category = new Category();
        $category->category_name = $req->input('category_name');
        $category->dinning_in = $req->input('dinning_in') == '1';
        $category->take_away = $req->input('take_away') == '1';

        if ($req->hasFile('image')) {
            $file_path = $req->file('image')->store('categoryImage', 'public');
            $category->image = $file_path;
        }

        $category->save();
        return $category;
    }
    function editCategory(Request $req, $id)
    {
        $category = Category::findOrFail($id);
        $category->category_name = $req->input('category_name');
        $category->dinning_in = $req->input('dinning_in');
        $category->take_away = $req->input('take_away');

        if ($req->hasFile('image')) {
            error_log("has image");
            // Delete the old image if it exists
            if ($category->image) {
                $imagePath = storage_path('app/public/' . $category->image);
                if (file_exists($imagePath)) {
                    unlink($imagePath);
                }
            }

            // Store the new image
            $file_path = $req->file('image')->store('categoryImage', 'public');
            $category->image = $file_path;
        }

        $category->save();
        return $category;
    }

    function categoryList()
    {
        return Category::all();
    }

    // function dinningIn()
    // {
    //     return Category::where('dinning_in', 1)->get();
    // }

    function getCategories()
    {
        return Category::pluck('category_name')->toArray();
    }

    public function get_category($id)
    {
        $category = Category::find($id);

        if (!$category) {
            return response()->json(['message' => 'category not found'], 404);
        }
        return $category;
    }

    function deleteCategories($id)
    {
        // Find the category to be deleted
        $category = Category::find($id);

        // If category not found, return failure
        if (!$category) {
            return response()->json(['message' => 'Category not found'], 404);
        }

        // Delete related food items and their images
        $foodItems = FoodItems::where('food_category', $category->category_name)->get();
        foreach ($foodItems as $foodItem) {
            // Delete the image associated with the food item
            $imagePath = storage_path('app/public/' . $foodItem->image);
            if (file_exists($imagePath)) {
                unlink($imagePath);
            }
            $discount = Discount::where('food_name', $foodItem->food_name)
                ->where('food_category', $foodItem->food_category)
                ->first();
            if ($discount) {
                $discount->delete();
            }
            $foodItem->delete();
            // Delete the food item itself

        }

        // Delete the image file associated with the category
        $imagePath = storage_path('app/public/' . $category->image);
        if (file_exists($imagePath)) {
            unlink($imagePath);
        }

        $category->delete();

        // Return success message
        return response()->json(['result' => 'Category and related food items have been deleted'], 200);
    }
}
