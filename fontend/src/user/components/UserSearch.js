import React, { useState, useEffect } from 'react';
import "../styles/foodcard.css";
import "react-toastify/dist/ReactToastify.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSnowflake } from "@fortawesome/free-solid-svg-icons";

const SearchFoodItem = ({ setSearchResult }) => {
  const [query, setQuery] = useState('');

  const truncateText = (text, maxLength) => {
    if (text.length <= maxLength) {
      return text;
    }
    return text.substring(0, maxLength) + "...";
  };

  async function search() {
    if (query.trim() === "") {
      setSearchResult(null); // Reset search result if input is empty
      return;
    }
    console.warn(query);
    try {
      let result = await fetch("https://sky-garden.ugyen1234.serv00.net/api/search/" + query);
      result = await result.json();
      if (result.length === 0) {
        setSearchResult([]); // Set empty array if no results
      } else {
        setSearchResult(result); // Update search result state in parent component
      }
      console.warn(result);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  }

  useEffect(() => {
    const timer = setTimeout(() => {
      search(); // Trigger search after a delay when query changes
    }, 500); // Adjust the delay as needed
    return () => clearTimeout(timer); // Clear the timer on component unmount
  }, [query]);

  const handleInputChange = (e) => {
    setQuery(e.target.value); // Update query state on input change
  };

  return (
    <div>
      <input
        type="text"
        value={query}
        onChange={handleInputChange}
        className='form-control search-item'
        placeholder="Search for a food item..."
      />
      <br />
    </div>
  );
};

export default SearchFoodItem;
