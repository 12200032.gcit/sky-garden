import React from "react";
import "../styles/nav.css";
import Navbar from "./Navbar";

import Marquee from "./SlidingWord";
import EventDetails from "./EventDetails";
import MobileNavbar from "./MoblieNavbar";
import Whatsapp from "./WhatsAppIcon";




const SingleEventDetails = () => {
  return (
    <div>
      <Marquee />
      <Navbar />
      <MobileNavbar/>
      <div style={{ position: "fixed", top: "40vw", right: "7vw", zIndex: "50" }}>
        <Whatsapp />
      </div>
      
      <EventDetails/>
    </div>
  );
};
export default SingleEventDetails;
