import React, { useState, useEffect } from "react";
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import Footer from "./Footer";
import "../styles/event.css";

function EventList() {
    const [events, setEvents] = useState([]);

    const fetchEventData = async () => {
        const toastId = toast.loading("Fetching data...");
        try {
            const response = await fetch("https://sky-garden.ugyen1234.serv00.net/api/get-all-events");
            if (!response.ok) {
                throw new Error('Failed to fetch data');
            }
            const eventData = await response.json();
            setEvents(eventData);

            toast.update(toastId, {
                render: "Data fetched successfully",
                type: "success",
                isLoading: false,
                autoClose: 3000,
            });
        } catch (error) {
            toast.update(toastId, {
                render: "Failed to fetch data",
                type: "error",
                isLoading: false,
                autoClose: 3000,
            });
        }
    };

    useEffect(() => {
        fetchEventData();
    }, []);

    const formatEventDate = (dateString) => {
        const eventDate = new Date(dateString);
        const year = eventDate.getFullYear();
        const month = eventDate.toLocaleString('default', { month: 'short' });
        const day = eventDate.getDate();
        return { year, month, day };
    };

    return (
        <>
            <div>
                <div className="Menu-mark-container">
                    <div className="menu-text1">
                        <p>UPCOMING EVENTS </p>
                    </div>
                  
                </div>
                <div className="daily-container d-flex" style={{ marginTop: "-4vw" }}>
                    <div className="blue-mark"></div>
                    <div className="mark-daily-title">
                        <p>Events</p>
                    </div>
                    <div className="pattern-img">
                        <img src="/img/pattern.png" alt="a logo" className="pattern" />
                    </div>
                </div>
            </div>

            <div className="offer-mainContainer" style={{ marginTop: "4vw" }}>
                {events.map(event => {
                    const { year, month, day } = formatEventDate(event.date);
                    return (
                        <div className="px-5 py-4" key={event.id}>
                            <div className="border shadow ">
                                <div >
                                    <img src={"https://sky-garden.ugyen1234.serv00.net/storage/" + event.image} className="img-fluid"
                                        style={{
                                            zIndex: "-1",
                                            width: "100%",
                                            height: "42vw"
                                        }} alt="Card" />

                                    <div className="rounded py-3 px-3 text-white text-start"
                                        style={{
                                            width: "10vw",
                                            backgroundColor: "rgba(244, 155, 63, 0.88)",
                                            marginLeft: "5vh",
                                            marginTop: "-10vh",
                                            zIndex: "100",
                                            position: "relative",
                                        }}
                                    >
                                        <p style={{ fontSize: "2vw" }}>{year}</p>
                                        <p style={{ fontSize: "1.5vw" }}>{month} {day}</p>
                                    </div>
                                </div>

                                <div className="mt-5 mb-5">
                                    <div className="title d-flex justify-content-center align-items-center">
                                        <h1 className="eventText-Title">{event.title}</h1>
                                    </div>
                                    <div className="eventText">

                                        <p className="text-start p-5" style={{color:"#767681"}}>
                                            {event.description}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    );
                })}
            </div>
            <Footer/>
        </>
    );
}

export default EventList;
