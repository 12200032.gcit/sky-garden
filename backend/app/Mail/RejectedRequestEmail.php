<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Queue\SerializesModels;

class RejectedRequestEmail extends Mailable
{
    use Queueable, SerializesModels;
    public $reason;
    public $table;
    /**
     * Create a new message instance.
     */
    public function __construct($table,$reason)
    {
        $this->reason = $reason;
        $this->table = $table;
    }

    /**
     * Get the message envelope.
     */
    public function envelope(): Envelope
    {
        return new Envelope(
            subject: 'Table Request Rejected',
        );
    }

    /**
     * Get the message content definition.
     */
    public function content(): Content
    {
        return new Content(
            markdown: 'emails.reject',
        );
    }

    /**
     * Get the attachments for the message.
     *
     * @return array<int, \Illuminate\Mail\Mailables\Attachment>
     */
    public function attachments(): array
    {
        return [];
    }


    public function build()
    {
        return $this->view('emails.reject')
            ->subject('Your Request has been rejected')
            ->with([
                'table' => $this->table,
                'reason' => $this->reason,
            ]);
    }
}
