import React, { useState } from "react";
import Modal from "react-bootstrap/Modal";

import "../../admin/manager/css/Table.css"
import "../styles/user_table.css";




import "../styles/form.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faLock } from "@fortawesome/free-solid-svg-icons";
import Button from "react-bootstrap/Button";

const Table = () => {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [phone, setPhone] = useState("");
  const [otp, setOtp] = useState("");
  const [date, setDate] = useState("");
  const [people, setPeople] = useState("");
  const [request, setRequest] = useState("");
  const [errorMessage, setErrorMessage] = useState("");
  const [successMessage, setSuccessMessage] = useState("");
  const [errorMessage2, setErrorMessage2] = useState("");
  const [successMessage2, setSuccessMessage2] = useState("");
  const [showAcceptModal, setShowAcceptModal] = useState(false);
  const [loading, setLoading] = useState(false);
  const [loading2, setLoading2] = useState(false);

  const handleClose = () => {
    setShowAcceptModal(false);
  };

  const handleAcceptShow = () => setShowAcceptModal(true);

  const verifyOtp = async (e) => {
    e.preventDefault();
    setErrorMessage("");
    setSuccessMessage("");
    setLoading2(true);
    try {
      const response = await fetch("https://sky-garden.ugyen1234.serv00.net/api/verify-table-otp", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ email, otp }),
      });

      const data = await response.json();

      if (!response.ok) {
        throw new Error(data.message);
      }

      if (response.ok) {
        handleRequest()
        handleClose()
      }
    } catch (error) {
      setErrorMessage2(error.message);
      setSuccessMessage2("");
      setTimeout(() => {
        setErrorMessage2("");
      }, 20000); // Vanish error message after 20 seconds
    }
    finally {
      setLoading2(false); // Always stop loading
    }
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    setLoading(true);

    const phoneRegex = /^(77|17)\d{6}$/;
    if (!phoneRegex.test(phone)) {
      setErrorMessage("Please provide a valid phone number!");
      setSuccessMessage("");
      setLoading(false); // Stop loading
      return;
    }

    if (isNaN(people) || people < 1) {
      setErrorMessage("Number of people must be at least 1.");
      setSuccessMessage("");
      setLoading(false); // Stop loading
      return;
    }

    try {
      const response = await fetch('https://sky-garden.ugyen1234.serv00.net/api/send-table-otp', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ name, email }),
      });

      if (response.ok) {
        handleAcceptShow();
        setSuccessMessage2("OTP has been sent successfully.");
        setErrorMessage(""); // Clear any previous error messages
      } else {
        setErrorMessage('We encountered an issue while sending the OTP. Please try again.');
        setSuccessMessage(""); // Clear any previous success messages
      }
    } catch (error) {
      setErrorMessage('We encountered an issue while sending the OTP. Please try again.');
      setSuccessMessage(""); // Clear any previous success messages
    } finally {
      setLoading(false); // Always stop loading
    }
  };

  const handleRequest = async () => {
    try {
      const formData = {
        name,
        email,
        phone,
        date,
        people,
        request,
      };

      const response = await fetch("https://sky-garden.ugyen1234.serv00.net/api/table-request", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(formData),
      });

      if (!response.ok) {
        throw new Error("Failed to submit the form. Please try again.");
      }

      // Clear form fields on successful submission
      setName("");
      setEmail("");
      setPhone("");
      setDate("");
      setPeople("");
      setRequest("");
      setOtp(""); // Clear OTP field
      setSuccessMessage("Table booked successfully! We'll be in touch via WhatsApp.");

      setTimeout(() => {
        setSuccessMessage("");
      }, 20000); // Vanish success message after 20 seconds
    } catch (error) {
      setErrorMessage2("Unable to submit the form. Please try again later.");

      setTimeout(() => {
        setErrorMessage2("");
      }, 20000); // Vanish error message after 20 seconds
    }
  }

  const getCurrentDate = () => {
    const today = new Date();
    const year = today.getFullYear();
    const month = String(today.getMonth() + 1).padStart(2, "0");
    const day = String(today.getDate()).padStart(2, "0");
    return `${year}-${month}-${day}`;
  };

  return (
    <div>
      <form onSubmit={handleSubmit} className="form p-2">
        <div className="row mt-5">
          <div className="col-md-6 hide-on-small">
            <div className="m-3">
              <img src="/img/img.png" className="img-fluid" style={{ height: "35vw", width: "45vw" }} alt="Content" />
            </div>
          </div>
          <div className="col-md-6 p-3" style={{ marginTop: "2vw" }}>
            <div className="table-container">
              <div style={{ paddingInline: "20px" }}>
                {errorMessage && <div className="error-message">{errorMessage}</div>}
                {successMessage && <div className="success-message">{successMessage}</div>}
              </div>
              <div className="row px-4 py-3">
                <div className="col-md-4 p-2">
                  <input
                    className="form-control"
                    type="text"
                    placeholder="Your Name *"
                    required
                    style={{ backgroundColor: "rgba(245, 245, 245, 1)" }}
                    value={name}
                    onChange={(e) => setName(e.target.value)}
                  />
                </div>
                <div className="col-md-4 p-2">
                  <input
                    className="form-control"
                    type="date"
                    placeholder="Date"
                    required
                    style={{ backgroundColor: "rgba(245, 245, 245, 1)" }}
                    value={date}
                    onChange={(e) => setDate(e.target.value)}
                    min={getCurrentDate()} // Set min attribute to current date
                  />
                </div>
                <div className="col-md-4 p-2">
                  <input
                    className="form-control"
                    type="number"
                    placeholder="Number of people *"
                    required
                    style={{ backgroundColor: "rgba(245, 245, 245, 1)" }}
                    value={people}
                    onChange={(e) => setPeople(e.target.value)}
                    min="1" // Ensure the minimum value is 1
                  />
                </div>
              </div>
              <div className="row px-4 py-3">
                <div className="col-lg-6 p-2">
                  <input
                    className="form-control"
                    type="email"
                    placeholder="Your Email *"
                    required
                    style={{ backgroundColor: "rgba(245, 245, 245, 1)" }}
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                  />
                </div>
                <div className="col-lg-6 p-2">
                  <input
                    className="form-control"
                    type="text"
                    placeholder="Your Phone number *"
                    required
                    style={{ backgroundColor: "rgba(245, 245, 245, 1)" }}
                    value={phone}
                    onChange={(e) => setPhone(e.target.value)}
                  />
                </div>
              </div>
              <div className="row px-4 py-3">
                <div className="col-md-12 p-2">
                  <textarea
                    className="form-control"
                    placeholder="Additional Request or message *"
                    style={{ height: "20vh", backgroundColor: "rgba(245, 245, 245, 1)" }}
                    required
                    value={request}
                    onChange={(e) => setRequest(e.target.value)}
                  />
                </div>
              </div>
              <div className="p-2 d-flex justify-content-center">
                <button className="book-button" type="submit" disabled={loading}>
                  {loading ? 'Processing...' : 'Book A Table'}
                </button>
              </div>
            </div>
          </div>
        </div>
      </form>
      <div
        className="d-flex justify-content-center align-items-center"
      >
        <Modal
          show={showAcceptModal}
          onHide={handleClose}
          centered
          backdrop="static"
          keyboard={false}
          size="md"
        >
          <Modal.Body
            className="text-center"
            style={{ paddingInline: "30px", paddingTop: "40px" }}
          >
            {errorMessage2 && <div className="error-message">{errorMessage2}</div>}
            {successMessage2 && <div className="success-message">{successMessage2}</div>}
            <FontAwesomeIcon
              style={{ color: "rgba(0, 160, 177, 1)", fontSize: "50px" }}
              className="pb-3"
              icon={faLock}
            />
            <p className="confirm-text">
              Please enter the OTP sent to your email to verify your request:
            </p>
            <input
              className="form-control my-3"
              placeholder="Enter OTP"
              value={otp}
              onChange={(e) => setOtp(e.target.value)}
              style={{
                border: '1px solid #ccc',
                borderRadius: '4px',
                padding: '10px',
                width: '100%',
              }}
            />
          </Modal.Body>
          <Modal.Footer
            className="d-flex justify-content-evenly pt-0"
            style={{ border: "none", paddingBottom: "25px" }}
          >
            <Button className="model-button" onClick={handleClose}>
              Cancel
            </Button>
            <Button className="model-button" onClick={verifyOtp} disabled={loading}>
              {loading ? 'Confirming...' : 'Confirm'}
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    </div>
  );
};

export default Table;
