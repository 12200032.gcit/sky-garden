import React, { useState, useEffect } from "react";
import SideBar from "./SideBar";

import Modal from "react-bootstrap/Modal";
import { Link } from "react-router-dom";
import "./css/E&A.css";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faCalendarCheck,
  faTrashCan,
  faPlus,
  faQuestion,
} from "@fortawesome/free-solid-svg-icons";
import Topbar from "../Topbar";
import { Button } from "reactstrap";

function EventAnounce() {
  const [events, setEvents] = useState([]);
  const [show, setShow] = useState(false);
  const [eventIdToDelete, setEventIdToDelete] = useState(null);
  const [loading, setLoading] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = (eventId) => {
    setShow(true);
    setEventIdToDelete(eventId);
  };

  useEffect(() => {
    fetchEvents();
  }, []);

  const fetchEvents = async () => {
    try {
      const response = await fetch("https://sky-garden.ugyen1234.serv00.net/api/get-all-events");

      const data = await response.json();
      const formattedEvents = data.map((event) => {
        const date = new Date(event.date).toLocaleDateString("en-US", {
          year: "numeric",
          month: "short",
          day: "numeric",
        });
        const timeParts = event.time.split(":");
        let hours = parseInt(timeParts[0]);
        const minutes = timeParts[1];
        const ampm = hours >= 12 ? "PM" : "AM";
        hours = hours % 12 || 12;
        const formattedTime = `${hours}:${minutes} ${ampm}`;
        return {
          ...event,
          date,
          time: formattedTime,
        };
      });
      setEvents(formattedEvents);
    } catch (error) {

    }
  };

  const handleRemove = async (e) => {
    e.preventDefault();
    setLoading(true);
    try {
      const response = await fetch(`https://sky-garden.ugyen1234.serv00.net/api/delete-events/${eventIdToDelete}`, {
        method: "DELETE",
      });
      if (!response.ok) {
        throw new Error("Failed to delete announcement");
      }
      const data = await response.json();
      if (data.message === 'Event deleted successfully') {
        toast.success("Announcement deleted successfully", {
          autoClose: 3000,
        });
        handleClose();
        fetchEvents();
      } else {
        toast.error("Failed to delete announcement", {
          autoClose: 3000,
        });
        handleClose();
      }
    } catch (error) {
      toast.error("Failed to delete announcement", {
        autoClose: 3000,
      });
      handleClose();
    } finally {
      setLoading(false);
    }
  };

  return (
    <div className="d-flex">
      <SideBar />
      <div className="p-4" style={{ width: "93%" }}>
        <Topbar />
        <div className="border title_bar">
          <p>Event and Announcement</p>
        </div>
        <div className="d-flex justify-content-between">
          <div className="d-flex event-offer">
            <FontAwesomeIcon className="icon" icon={faCalendarCheck} />
            <p>Events & Announcement</p>
            <div className="count">{events.length}</div>
          </div>
          <div className="col-md-4 align-content-center">
            <Link
              to="/announcement"
              className="short-announcement"
            >
              Add Announcement
            </Link>
          </div>
        </div>
        <div className="row event-card m-0">
          <Link
            to="/add-event-and-offer"
            className="card p-0 col-md-2 d-flex align-items-center justify-content-center"
            style={{ cursor: "pointer", height: "300px" }}
          >
            <FontAwesomeIcon
              style={{ fontSize: "60px", color: "rgba(244, 155, 63, 1)" }}
              icon={faPlus}
            />
          </Link>

          {events.map((event) => (
            <div key={event.id} style={{ height: "300px" }} className="card p-0 col-md-2">
              <div style={{ height: "100%" }}>
                <img
                  className="card-img-top"
                  src={"https://sky-garden.ugyen1234.serv00.net/storage/" + event.image}
                  alt="event"
                  style={{
                    width: "100%",
                    maxHeight: "180px",
                    objectFit: "cover",
                  }}
                />
              </div>
              <div className="card-body">
                <div className="d-flex justify-content-between">
                  <h5 className="card-title" style={{ overflow: "hidden", textOverflow: "ellipsis", whiteSpace: "nowrap", maxWidth: "calc(100% - 20px)" }}>{event.title}</h5>
                  <FontAwesomeIcon
                    style={{ color: "rgba(255, 0, 0, 0.88)", cursor: "pointer" }}
                    icon={faTrashCan}
                    onClick={() => handleShow(event.id)}
                  />
                </div>
                <p className="card-text" style={{ overflow: "hidden", textOverflow: "ellipsis", display: "-webkit-box", WebkitLineClamp: 1, WebkitBoxOrient: "vertical" }}>{event.description}</p>
                <div className="d-flex justify-content-between">
                  <p className="date">{event.date}</p>
                  <p className="time">{event.time}</p>
                  <Link onClick={() => {
                    localStorage.removeItem('eventId'); // Remove existing value
                    localStorage.setItem('eventId', event.id); // Set new value
                  }}
                    style={{ textDecorationLine: "none" }} to={`/edit-event-and-offer/${event.id}`}>
                    <span>
                      <button className="button">Edit</button>
                    </span>
                  </Link>
                </div>
              </div>
            </div>
          ))}

          <Modal
            show={show}
            onHide={handleClose}
            centered
            backdrop="static"
            keyboard={false}
            size="sm"
          >
            <Modal.Body
              className="text-center"
              style={{ paddingInline: "30px", paddingTop: "40px" }}
            >
              <FontAwesomeIcon
                style={{ color: "rgba(0, 160, 177, 1)", fontSize: "50px" }}
                className="pb-3"
                icon={faQuestion}
              />
              <p
                style={{
                  color: "rgba(160, 152, 174, 1)",
                  fontSize: "13px",
                  fontWeight: "600",
                  letterSpacing: "1px",
                }}
              >
                Are you sure you want to delete this announcement?
              </p>
            </Modal.Body>
            <Modal.Footer
              className="d-flex justify-content-evenly pt-0"
              style={{ border: "none", paddingBottom: "25px" }}
            >
              <Button
                style={{
                  backgroundColor: "rgba(244, 155, 63, 1)",
                  borderRadius: "20px",
                  border: "none",
                  fontSize: "13px",
                  fontWeight: "600",
                  letterSpacing: "1px",
                  paddingInline: "15px",
                  paddingBlock: "8px",
                }}
                onClick={handleClose}
              >
                Cancel
              </Button>
              <Button
                style={{
                  backgroundColor: "rgba(244, 155, 63, 1)",
                  borderRadius: "20px",
                  border: "none",
                  fontSize: "13px",
                  fontWeight: "600",
                  letterSpacing: "1px",
                  paddingInline: "15px",
                  paddingBlock: "8px",
                }}
                onClick={handleRemove}
                disabled={loading}
              >
                {loading ? 'Deleting...' : 'Delete'}
              </Button>
            </Modal.Footer>
          </Modal>
        </div>
      </div>
    </div >
  );
}

export default EventAnounce;
