import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowRightFromBracket } from '@fortawesome/free-solid-svg-icons';
import { Link, useNavigate } from 'react-router-dom';
import { toast, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

function Topbar() {
    const user = JSON.parse(localStorage.getItem('user'));
    const navigate = useNavigate();

    const refreshToken = async () => {
        try {
            const response = await fetch('https://sky-garden.ugyen1234.serv00.net/api/refresh', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                }
            });

            if (response.ok) {
                const data = await response.json();
                localStorage.setItem('token', data.authorisation.token);
                return true;
            } else {
                toast.error('Failed to refresh token');
                return false;
            }
        } catch (error) {
            toast.error('Error refreshing token');
            return false;
        }
    };

    const handleLogout = async () => {
        const toastId1 = toast.loading('Logging out...');
        try {
            let response = await fetch('https://sky-garden.ugyen1234.serv00.net/api/logout', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                },
            });

            if (response.status === 401) {
                const tokenRefreshed = await refreshToken();
                if (tokenRefreshed) {
                    response = await fetch('https://sky-garden.ugyen1234.serv00.net/api/logout', {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json',
                            'Authorization': `Bearer ${localStorage.getItem('token')}`
                        },
                    });
                }
            }

            if (response.ok) {
                localStorage.removeItem('token');
                localStorage.removeItem('user');
                toast.update(toastId1, { render: 'Logout successful!', type: 'success', isLoading: false, autoClose: 3000 });
                navigate("/");
                window.location.reload();
                return;
            } else {
                toast.update(toastId1, { render: 'Failed to logout', type: 'error', isLoading: false, autoClose: 3000 });
                window.location.reload()
            }
        } catch (error) {
            toast.update(toastId1, { render: 'Error logging out', type: 'error', isLoading: false, autoClose: 3000 });
            window.location.reload()
        }
        
    };

    const handleLogoutClick = async () => {
        await handleLogout();
    };

    return (
        <div className="d-flex name_logout_bar">
            <ToastContainer />
            {user && user.name ? (
                <p>Hello, <span>{user.name}</span></p>
            ) : (
                <p>Hello, <span>Guest</span></p>
            )}

            <Link onClick={handleLogoutClick} className="ms-auto logout">
                <FontAwesomeIcon icon={faArrowRightFromBracket} />
            </Link>
        </div>
    );
}

export default Topbar;
