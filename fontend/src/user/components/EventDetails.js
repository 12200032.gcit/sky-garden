import React, { useState, useEffect } from "react";
import { useParams } from 'react-router-dom';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Footer from "./Footer";

function EventDetails() {
  const { id } = useParams();
  const [data, setData] = useState({});
  const [formattedDate, setFormattedDate] = useState({ year: '', month: '', day: '' });

  const fetchEventData = async () => {
    const toastId = toast.loading("Fetching data...");
    try {
      const response = await fetch(`https://sky-garden.ugyen1234.serv00.net/api/get-events/${id}`);
      if (!response.ok) {
        throw new Error('Failed to fetch data');
      }
      const eventData = await response.json();
      setData(eventData);

      const eventDate = new Date(eventData.date);
      const year = eventDate.getFullYear();
      const month = eventDate.toLocaleString('default', { month: 'short' });
      const day = eventDate.getDate();

      setFormattedDate({ year, month, day });

      toast.update(toastId, {
        render: "Data fetched successfully",
        type: "success",
        isLoading: false,
        autoClose: 3000,
      });
    } catch (error) {
      toast.update(toastId, {
        render: "Failed to fetch data",
        type: "error",
        isLoading: false,
        autoClose: 3000,
      });
    }
  };

  useEffect(() => {
    fetchEventData();
  }, [id]);

  return (
    <>
      <div>
        <div className="Menu-mark-container">
          <div className="menu-text1">
            <p>UPCOMING EVENTS </p>
          </div>
         
        </div>
        <div className="daily-container d-flex" style={{ marginTop: "-4vw" }}>
          <div className="blue-mark"></div>
          <div className="mark-daily-title">
            <p>Events</p>
          </div>
          <div className="pattern-img">
            <img src="/img/pattern.png" lt="a logo" className="pattern" />
          </div>
        </div>
      </div>

      <div className="offer-mainContainer" style={{ marginTop: "4vw" }}>

        <div className="px-5 py-4">
          <div className="border shadow ">
            <div >
              <img src={"https://sky-garden.ugyen1234.serv00.net/storage/" + data.image} className="img-fluid"
                style={
                  {
                    zIndex: "-1",
                    width: "100%",

                    height: "42vw"
                  }} alt="Card" />

              <div className="rounded py-3 px-3 text-white text-start"
                style={{
                  width: "10vw",

                  backgroundColor: "rgba(244, 155, 63, 0.88)",
                  marginLeft: "5vh",
                  marginTop: "-10vh",
                  zIndex: "100",
                  position: "relative",

                }}
              >
                <p style={{ fontSize: "2vw" }}>{formattedDate.year}</p>
                <p style={{ fontSize: "1.5vw" }}>{formattedDate.month} {formattedDate.day}</p>
              </div>
            </div>

            <div className="mt-5 mb-5">
              <div className="title d-flex justify-content-center align-items-center">
                <h1 className="eventText-Title">{data.title}</h1>
              </div>
              <div className="eventText">

                <p className="text-start p-5" style={{ color: "#767681" }}>
                  {data.description}
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Footer />


    </>
  );
}
export default EventDetails;





