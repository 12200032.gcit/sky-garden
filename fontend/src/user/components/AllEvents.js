import React from "react";
import "../styles/nav.css";
import Navbar from "./Navbar";
import MobileNavbar from "./MoblieNavbar";

import Marquee from "./SlidingWord";
import EventList from "./EventList";
import Whatsapp from "./WhatsAppIcon";




const AllEventDetails = () => {
  return (
    <div>
      <Marquee />
      <Navbar />
      <MobileNavbar />
      <div style={{ position: "fixed", top: "40vw", right: "7vw", zIndex: "50" }}>
        <Whatsapp />
      </div>
      <EventList/>
    </div>
  );
};
export default AllEventDetails;
