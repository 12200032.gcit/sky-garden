import React, { useState, useRef, useEffect } from "react";
import "../styles/promotional.css"

function PromotionalVideo() {
    const [isMuted, setIsMuted] = useState(true);
    const videoRef = useRef(null);
    const [promotionalVideoUrl, setPromotionalVideoUrl] = useState(null);

    const toggleMute = () => {
        setIsMuted(!isMuted);
        if (videoRef.current) {
            videoRef.current.muted = !isMuted;
        }
    };
    useEffect(() => {
        fetchVideos();
    }, []);

    const fetchVideos = async () => {
        try {
            const response = await fetch("https://sky-garden.ugyen1234.serv00.net/api/get-videos");
            if (response.ok) {
                const data = await response.json();
                setPromotionalVideoUrl(data.promotion_video);
            } else {
                throw new Error('Failed to fetch videos.');
            }
        } catch (error) {
            console.error('Failed to fetch videos. Please try again.');
        }
    };
    return (
        <div className="promotional-video-container">
            <video className="full-width-video"
                autoPlay
                muted
                controls
                onClick={toggleMute}
                src={`https://sky-garden.ugyen1234.serv00.net${promotionalVideoUrl}`} type="video/mp4"
            >
            </video>
        </div>
    );
}
export default PromotionalVideo;


