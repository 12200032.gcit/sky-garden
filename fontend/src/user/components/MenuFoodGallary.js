import React, { useEffect, useState } from 'react';
import Isotope from 'isotope-layout';
import Slider from 'react-slick';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import "../styles/slider.css";
import "../styles/isotope.css";
import "../styles/UserCategory.css";
import UserFoodCard from './UserFoodCard';
import UserCategoriesCard from './UserCategoryCard';
import SearchFoodItem from './UserSearch';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSnowflake } from "@fortawesome/free-solid-svg-icons";
import "../styles/Search.css";
import Footer from './Footer';

const MenuFoodGallery = (props) => {
  const [iso, setIso] = useState();
  const [activeCategory, setActiveCategory] = useState('');
  const [data, setData] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [searchResult, setSearchResult] = useState(null);
  const itemsPerPage = 12;

  const settings = {
    infinite: true,
    slidesToShow: 9,
    slidesToScroll: 1,
    arrows: true,
    prevArrow: <button className="slick-prev "></button>,
    nextArrow: <button className="slick-next"></button>,
  };

  const categories = props.selectedData;

  useEffect(() => {
    if (categories.length > 0) {
      setActiveCategory(categories[0].category_name);
    }
  }, [categories]);

  useEffect(() => {
    const $container = document.querySelector('.portfolioContainer');
    if ($container) {
      const iso = new Isotope($container, {
        itemSelector: '.portfolio-item',
        layoutMode: 'fitRows'
      });
      setIso(iso);
      return () => {
        iso.destroy();
      };
    }
  }, []);

  async function getData() {
    try {
      let response = await fetch("https://sky-garden.ugyen1234.serv00.net/api/getfooditem");
      if (!response.ok) {
        throw new Error('Failed to fetch data');
      }
      let result = await response.json();
      setData(result);
    } catch (error) {
      console.error('Error fetching data:', error);
    }
  }

  useEffect(() => {
    getData();
  }, []);

  useEffect(() => {
    if (iso) {
      iso.arrange({ filter: '.' + activeCategory });
    }
  }, [iso, activeCategory]);

  const categoryItems = data.filter(item => item.food_category === activeCategory);
  const categoryItemCount = categoryItems.length;

  const indexOfLastItem = currentPage * itemsPerPage;
  const indexOfFirstItem = indexOfLastItem - itemsPerPage;
  const currentItems = categoryItems.slice(indexOfFirstItem, indexOfLastItem);

  const paginate = pageNumber => setCurrentPage(pageNumber);

  return (
    <>
      <div className='search-container'>
        <SearchFoodItem setSearchResult={setSearchResult} />
      </div>
      <section className="food-content">
        <div>
          {!searchResult && (
            <div className="category-count user-category-count">
              <span style={{ color: '#F49B3F' }}>Total Menu:</span>
              <span style={{ color: '#00A0B1', }}>{categories.length}</span>
            </div>
          )}

          {searchResult ? (
            searchResult.length === 0 ? (
              <div className='no-item'><p className='no-item-text'>No food item</p></div>
            ) : (
              <div className="search-result-container d-flex">
                {searchResult.map((item) => (
                  <div className="card foodCard mx-2 mb-2" key={item.id}>
                    <button className="delete-button">
                      <FontAwesomeIcon
                        style={{ color: 'white', fontSize: "23px" }}
                        className="p-3"
                        icon={faSnowflake}
                      />
                    </button>
                    <img
                      className="card-img-top food-img"
                      alt='Food'
                      width={100}
                      src={"https://sky-garden.ugyen1234.serv00.net/storage/" + item.image}
                    />
                    <div className="card-body">
                      <h5 className="card-title food-title">{item.food_name}</h5>
                      <p className="card-text food-description">{item.description}</p>
                      <p className="card-cost">Nu: {item.price}</p>
                    </div>
                  </div>
                ))}
              </div>
            )
          ) : (
            <>
              <Slider {...settings} className='slider-container user-slider'>
                {categories.map((category) => (
                  <div className="mt-10 category-card-container" key={category.id}>
                    <div className="col-lg-12">
                      <div className="text-center individual-cards">
                        <ul className="container-filter portfolioFilter list-unstyled mb-0" id="filter">
                          <UserCategoriesCard
                            category={category.category_name}
                            active={category.category_name === activeCategory}
                            onClick={() => {
                              setActiveCategory(category.category_name);
                              setCurrentPage(1);
                            }}
                            image={category.image}
                            id={category.id}
                          />
                        </ul>
                      </div>
                    </div>
                  </div>
                ))}
              </Slider>

              <div className="row">
                <div className="portfolioContainer">
                  <div className='category-title'>CATEGORY
                    <span className='category-text' style={{ display: 'block' }}>{activeCategory}:{categoryItemCount}</span>
                  </div>
                  {categoryItemCount === 0 ? (
                    <div className='no-item'><p className='no-item-text'>NO FOOD ITEM</p></div>
                  ) : (
                    <div className="column-wrapper food-cards-container">
                      {currentItems.map((item) => (
                        <div className={`portfolio-item ${item.food_category}`} key={item.id}>
                          <div className="item-box">
                          <UserFoodCard
                              key={item.id}
                              id={item.id}
                              foodName={item.food_name}
                              description={item.description}
                              price={item.price}
                              after_discount={item.after_discount}
                              image={item.image}
                              discount={item.discount}
                              status={item.offer_status}
                            />
                          </div>
                        </div>
                      ))}
                    </div>
                  )}
                  {categoryItemCount > itemsPerPage && (
                    <div className="pagination mt-5 pb-5" style={{ marginLeft: "50%" }}>
                      {Array.from({ length: Math.ceil(categoryItemCount / itemsPerPage) }, (_, i) => (
                        <button
                          key={i}
                          onClick={() => paginate(i + 1)}
                          className={`page-link ${currentPage === i + 1 ? 'orange' : ''}`}
                          style={{ margin: '0 5px' }}
                        >
                          {i + 1}
                        </button>
                      ))}

                    </div>

                  )}
                  <div style={{ marginLeft: "-0.6vw", marginRight: "1.3vw" }}>  <Footer /></div>
                </div>

              </div>
            </>
          )}
        </div>
      </section>
    </>
  );
};

export default MenuFoodGallery;
