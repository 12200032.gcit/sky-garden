<?php

namespace App\Http\Controllers;

use App\Models\Discount;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DiscountController extends Controller
{
    // Function to fetch all discounts
    public function getAllDiscount()
    {
        return Discount::where('offer_status', false)->get();
    }

    // public function getDetails($id)
    // {
       
    //     $discount = Discount::find($id);
    

    //     if (!$discount) {
    //         return response()->json([
    //             'message' => 'Discount item not found'
    //         ], 404);
    //     }
    
       
    //     return response()->json($discount, 200);
    // }
    
    public function getAllDiscountOffered()
    {
        return Discount::where('offer_status', true)->get();
    }

    public function updateDiscounts(Request $request)
    {
        $data = $request->input('data'); // Assuming 'data' is the key for the array of discount data

        try {
            $ids = array_column($data, 'id'); // Extract all IDs from the data array
            $existingDiscounts = Discount::whereIn('id', $ids)->get();
            $existingIds = $existingDiscounts->pluck('id')->toArray();

            $nonExistingIds = array_diff($ids, $existingIds);
            if (!empty($nonExistingIds)) {
                return response()->json(['message' => 'Some IDs do not exist', 'non_existing_ids' => $nonExistingIds], 404);
            }

            foreach ($data as $discountData) {
                if (isset($discountData['discount']) && $discountData['discount'] > 0) {
                    $discount = $existingDiscounts->firstWhere('id', $discountData['id']);
                    if ($discount) {
                        $discount->discount = $discountData['discount'];
                        $discount->after_discount = $discountData['after_discount'];
                        $discount->offer_status = true;
                        $discount->save();
                    }
                }
            }

            return response()->json(['message' => 'Discounts updated successfully'], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Failed to update discounts', 'error' => $e->getMessage()], 500);
        }
    }
    public function deleteOffer($id)
    {
        try {
            $discount = Discount::findOrFail($id);
            $discount->offer_status = false;
            $discount->discount = 0;
            $discount->after_discount = $discount->actual_price;
            $discount->save();
            return response()->json(['message' => 'offer has been successfully removed?'], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Failed to remove offer'], 500);
        }
    }

    public function deleteMarkedOffer(Request $request)
    {
        $ids = $request->input('ids');

        try {
            $existingIds = Discount::find($ids)->pluck('id')->toArray();

            $nonExistingIds = array_diff($ids, $existingIds);
            if (!empty($nonExistingIds)) {
                return response()->json(['message' => 'Some IDs do not exist'], 404);
            }

            if (!empty($existingIds)) {
                // Use update method to update multiple records
                Discount::whereIn('id', $existingIds)
                    ->update([
                        'offer_status' => false,
                        'discount' => 0,
                        'after_discount' => DB::raw('actual_price') // Assuming you want to set after_discount to actual_price
                    ]);

                return response()->json(['message' => 'All marked offers have been removed successfully'], 200);
            } else {
                return response()->json(['message' => 'No offers found to remove'], 200);
            }
        } catch (\Exception $e) {
            return response()->json(['message' => 'Failed to remove offers'], 500);
        }
    }


    public function deleteAllOffer()
    {
        try {
            $discounts = Discount::where('offer_status', true)->get();
            if ($discounts->count() > 0) {
                foreach ($discounts as $discount) {
                    $discount->offer_status = false;
                    $discount->discount = 0;
                    $discount->after_discount = $discount->actual_price;
                    $discount->save();
                }
                return response()->json(['message' => 'All offers have been removed successfully.'], 200);
            } else {
                return response()->json(['message' => 'No offers found to remove.'], 200);
            }
        } catch (\Exception $e) {
            return response()->json(['message' => 'Failed to remove offers.'], 500);
        }
    }
}
