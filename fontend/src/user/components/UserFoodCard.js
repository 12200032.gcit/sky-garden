import React from 'react';
import "../styles/foodcard.css";
import "react-toastify/dist/ReactToastify.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSnowflake } from "@fortawesome/free-solid-svg-icons";
import { Link } from 'react-router-dom';

const UserFoodCard = (props) => {

  const truncateText = (text, maxLength) => {
    if (text.length <= maxLength) {
      return text;
    }
    return text.substring(0, maxLength) + "...";
  };

  return (
    <>
      <Link to={`/get-food-details/${props.id}`} style={{ textDecoration: "none" }}>
        <div className="card foodCard" style={{ backgroundColor: 'white' }}>
          <button className="delete-button">
            <FontAwesomeIcon
              style={{ color: 'white', fontSize: "23px" }}
              className="p-3"
              icon={faSnowflake}
            />
          </button>
          {props.status ? (
            <p
              className=''
              style={{
                position: 'absolute',
                top: 25,
                fontWeight: "500",
                letterSpacing: "1px",
                borderTopRightRadius: "10px",
                borderBottomRightRadius: "10px",
                backgroundColor: 'rgba(235, 87, 87, 1)',
                padding: '5px 10px',
                color: "#fff",
                zIndex: 1
              }}
            >
              {props.discount}% off
            </p>
          ) : (
            <p></p>
          )}
          <img className="card-img-top food-img" alt='imag' width={100} src={"https://sky-garden.ugyen1234.serv00.net/storage/" + props.image} />
          <div className="card-body">
            <h5 className="card-title food-title">{props.foodName}</h5>
            <p className="card-text food-description">{truncateText(props.description, 25)}</p>
            {props.status ? (
              <div className="price-container">
                <p className="after-discount">Nu: {props.after_discount}</p>
                <p className="original-price"><s>Nu: {props.price}</s></p>
              </div>
            ) : (
              <p className="card-cost">Nu: {props.price}</p>
            )}
          </div>
        </div>
      </Link>
    </>
  );
};

export default UserFoodCard;
