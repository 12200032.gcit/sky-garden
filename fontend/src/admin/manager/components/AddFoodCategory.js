import React, { useState, useRef } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowLeft, faCloudArrowUp } from '@fortawesome/free-solid-svg-icons';
import SideBar from '../SideBar';
import "./styles/editfood.css"
import { Link } from 'react-router-dom';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Topbar from '../../Topbar';

// Functional component AddFoodCategory
function AddFoodCategory(props) {
  const [categoryTitle, setCategoryTitle] = useState('');
  const [dineIn, setDineIn] = useState(true);
  const [takeAway, setTakeAway] = useState(false);
  const [selectedFile, setSelectedFile] = useState(null);
  const [previewURL, setPreviewURL] = useState(null);

  const fileInputRef = useRef(null);

  const handleCategoryTitleChange = (event) => {
    setCategoryTitle(event.target.value);
  };

  const handleDineInChange = (event) => {
    setDineIn(event.target.checked);
  };

  const handleTakeAwayChange = (event) => {
    setTakeAway(event.target.checked);
  };

  const handleFileChange = (event) => {
    const file = event.target.files[0];
    setSelectedFile(file);

    if (file) {
      const filePreviewURL = URL.createObjectURL(file);
      setPreviewURL(filePreviewURL);
    } else {
      setPreviewURL(null);
    }
  };

  const handleDrop = (event) => {
    event.preventDefault();
    const file = event.dataTransfer.files[0];
    setSelectedFile(file);

    if (file) {
      const filePreviewURL = URL.createObjectURL(file);
      setPreviewURL(filePreviewURL);
    } else {
      setPreviewURL(null);
    }
  };

  const handleReset = () => {
    setCategoryTitle('');
    setDineIn(true);
    setTakeAway(false);
    setSelectedFile(null);
    setPreviewURL(null);
    fileInputRef.current.value = '';
  };

  const handleImagePreviewClick = () => {
    fileInputRef.current.click();
  };

  async function addProduct() {
    // Check if categoryTitle and selectedFile are empty
    if (!categoryTitle.trim()) {
      toast.error("Please enter the Category Title");
      return;
    }

    if (!selectedFile) {
      toast.error("Please select an Image");
      return;
    }

    const formData = new FormData();

    formData.append('category_name', categoryTitle);
    formData.append('dinning_in', dineIn ? '1' : '0');
    formData.append('take_away', takeAway ? '1' : '0');
    formData.append('image', selectedFile);

    try {
      let response = await fetch("https://sky-garden.ugyen1234.serv00.net/api/addcategory", {
        method: 'POST',
        body: formData,
      });

      if (response.ok) {
        // Show success toast
        toast.success("Categeory has been added successfully");

        // Reset all field states to initial values
        handleReset();
      } else {
        throw new Error('Failed add Category');
      }
    } catch (error) {
      toast.error("Failed to save data. Please try again.");
    }
  }

  // JSX
  return (
    <>

      <div className='d-flex'>
        <SideBar />
        <div className='p-4' style={{ width: '93%' }}>
          <Topbar />
          <div className='border title_bar'>
            <p>Add Food Category</p>
          </div>
          <div className="form">
            <div className="row">
              <div className="col-md-8">
                <div className='col-md-10' style={{ paddingRight: "15px", position: "relative" }}>
                  <label htmlFor="category-title" style={{ fontWeight: "600", color: "#555555" }}>Category Title <span style={{ color: "red" }}>*</span></label>
                  <input
                    id='category-title'
                    value={categoryTitle}
                    onChange={handleCategoryTitleChange}
                    className='form-control'
                    placeholder='Category Title'
                    required
                  />
                </div>
                <div className='d-flex justify-content-start py-4 px-2'>
                  <div className=' d-flex justify-content-start align-items-center'>
                    <input
                      className="border"
                      type="checkbox"
                      id="flexCheckDineIn"
                      checked={dineIn}
                      onChange={handleDineInChange}
                      style={{ accentColor: 'black', width: "20px", height: '20px' }}
                    />
                    <label className=" form-check-label ms-2" htmlFor="flexCheckDineIn">
                      Dinning In
                    </label>
                  </div>

                  <div className=' d-flex ms-5 justify-content-start align-items-center'>
                    <input
                      className=""
                      type="checkbox"
                      id="flexCheckTakeAway"
                      checked={takeAway}
                      onChange={handleTakeAwayChange}
                      style={{ accentColor: 'black', width: "20px", height: '20px' }}
                    />
                    <label className="form-check-label ms-2" htmlFor="flexCheckTakeAway">
                      Take away
                    </label>
                  </div>

                </div>

              </div>
              <div className="col-md-4" style={{ paddingLeft: "70px", paddingBlock: "20px" }}>
                <label htmlFor="imageUpload">Upload Image <span style={{ color: "red" }}>*</span></label>
                <div
                  className="upload-area"
                  style={{ cursor:"pointer" }}
                  onDrop={handleDrop}
                  onDragOver={(event) => event.preventDefault()}
                  onClick={() => fileInputRef.current.click()}
                >
                  {previewURL ? (
                    <img src={previewURL} alt="Preview" className="preview-image" />
                  ) : (
                    <div style={{ textAlign: "center" }}>
                      <p className='upload-text'><FontAwesomeIcon icon={faCloudArrowUp} className='upload-icon' /><br />Drag and drop or click here to select image</p>
                    </div>
                  )}
                  <input
                    type="file"
                    id="imageUpload"
                    ref={fileInputRef}
                    accept="image/*"
                    onChange={handleFileChange}
                    style={{ display: 'none' }}
                  />
                </div>
              </div>
            </div>
            <div className="row mt-4">
              <div className="col-md-10">
                <Link to="/food" style={{ textDecorationLine: "none" }}>
                  <span className='go-back'>
                    <FontAwesomeIcon className='icon' icon={faArrowLeft} />
                    Go back
                  </span>
                </Link>
              </div>
              <div className="col-md-2 d-flex justify-content-between">
                <button className="btn reset" onClick={handleReset}>Reset</button>
                <button className="btn add" onClick={addProduct}>Save</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default AddFoodCategory;
