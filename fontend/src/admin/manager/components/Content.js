import React, { useEffect, useState } from 'react';
import Isotope from 'isotope-layout';
import Slider from 'react-slick';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import "./styles/slider.css";
import './styles/isotope.css';
import FoodCard from './FoodCard';
import CategoriesCard from './CategoryCard';
import "react-toastify/dist/ReactToastify.css";

const FoodGallary = (props) => {
  const [iso, setIso] = useState();
  const [activeCategory, setActiveCategory] = useState('');
  const [data, setData] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const itemsPerPage = 8;

  const settings = {
    infinite: true,
    slidesToShow: 9,
    slidesToScroll: 1,
    arrows: true,
    prevArrow: <button className="slick-prev "></button>,
    nextArrow: <button className="slick-next"></button>,
  };

  const categories = props.selectedData;

  useEffect(() => {
    if (categories.length > 0) {
      setActiveCategory(categories[0].category_name);
    }
  }, [categories]);

  useEffect(() => {
    const $container = document.querySelector('.portfolioContainer');
    const iso = new Isotope($container, {
      itemSelector: '.portfolio-item',
      layoutMode: 'fitRows'
    });
    setIso(iso);
    return () => {
      iso.destroy();
    };
  }, []);

  useEffect(() => {
    if (iso) {
      iso.arrange({ filter: '.' + activeCategory });
    }
  }, [iso, activeCategory]);


  async function getData() {
    try {
      let response = await fetch("https://sky-garden.ugyen1234.serv00.net/api/getfooditem");
      if (!response.ok) {
        throw new Error('Failed to fetch data');
      }
      let result = await response.json();
      setData(result);
    } catch (error) {
      console.error('Failed to fetch data');
    }
  }

  useEffect(() => {
    getData();
  }, []);

  const categoryItemCount = data.filter(item => item.food_category === activeCategory).length;

  const indexOfLastItem = currentPage * itemsPerPage;
  const indexOfFirstItem = indexOfLastItem - itemsPerPage;
  const currentItems = data.filter(item => item.food_category === activeCategory).slice(indexOfFirstItem, indexOfLastItem);

  const paginate = pageNumber => setCurrentPage(pageNumber);

  return (
    <section className="food-content">
      <div>
        <div className="category-count">
          <span style={{ color: '#F49B3F' }}>Total Categories : </span>
          <span style={{ color: '#00A0B1', }}>{categories.length}</span>
        </div>
        <Slider {...settings} className='slider-container'>
          {categories.map((category) => (
            <div className="mt-10 category-card-container" key={category.id}>
              <div className="col-lg-12">
                <div className="text-center individual-cards">
                  <ul className="container-filter portfolioFilter list-unstyled mb-0" id="filter">
                    <CategoriesCard
                      category={category.category_name}
                      active={category.category_name === activeCategory}
                      onClick={() => {
                        setActiveCategory(category.category_name);
                        setCurrentPage(1); // Reset pagination when category changes
                      }}
                      image={category.image}
                      id={category.id}
                    />
                  </ul>
                </div>
              </div>
            </div>
          ))}
        </Slider>

        <div className="row">
          <div className="portfolioContainer">
            <div className='category-title'>
              <span className='category-text' style={{ display: 'block' }}>{activeCategory} : {categoryItemCount}</span>
            </div>
            {categoryItemCount === 0 ? (
              <div className='no-item'> <p className='no-item-text'>NO FOOD ITEM</p></div>
            ) : (
              <div className="column-wrapper food-cards-container">
                {currentItems.map((item) => (
                  <div className={`portfolio-item ${item.food_category}`} key={item.id}>
                    <div className="item-box">
                      <FoodCard
                        key={item.id}
                        id={item.id}
                        foodName={item.food_name}
                        description={item.description}
                        price={item.price}
                        after_discount={item.after_discount}
                        image={item.image}
                        discount={item.discount} // You may want to pass this to FoodCard as well
                        status={item.offer_status} // Flag to indicate discount
                      />
                    </div>
                  </div>
                ))}
              </div>
            )}
            {/* Pagination */}
            <div className="pagination mt-5 pb-5" style={{ marginLeft: "50%" }}>
              {Array.from({ length: Math.ceil(categoryItemCount / itemsPerPage) }, (_, i) => (
                <button
                  key={i}
                  onClick={() => paginate(i + 1)}
                  className={`page-link ${currentPage === i + 1 ? 'orange' : ''}`}
                  style={{ margin: '0 5px' }} // Add margin between buttons
                >
                  {i + 1}
                </button>
              ))}
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default FoodGallary;
