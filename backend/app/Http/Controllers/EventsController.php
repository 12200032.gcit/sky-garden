<?php

namespace App\Http\Controllers;

use App\Models\Events;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class EventsController extends Controller
{
    function addEvents(Request $req)
    {
        $event = new Events;
        $event->title = $req->input('title');
        $event->date = $req->input('date');
        $event->time = $req->input('time');
        $event->description = $req->input('description');
        if ($req->hasFile('image')) {
            $file_path = $req->file('image')->store('events-and-announcement', 'public');
            $event->image = $file_path;
        }
        $event->save();
        return $event;
    }
    function getEvents()
    {
        return Events::all();
    }
    function delete_events($id)
    {
        $event = Events::find($id);

        if (!$event) {
            return response()->json(['message' => 'Event not found'], 404);
        }

        $imagePath = storage_path('app/public/' . $event->image);

        if (file_exists($imagePath)) {
            unlink($imagePath);
            $event->delete();
            return response()->json(['message' => 'Event deleted successfully'], 200);
        } else {
            return response()->json(['message' => 'Image not found'], 404);
        }
    }
    public function get_events($id)
    {
        $event = Events::find($id);

        if (!$event) {
            return response()->json(['message' => 'Events not found'], 404);
        }
        return $event;
    }

    public function updateEvents(Request $req, $id)
    {
        try {
            $event = Events::findOrFail($id);

            $event->title = $req->input('title');
            $event->date = $req->input('date');
            $event->time = $req->input('time');
            $event->description = $req->input('description');

            // error_log($req->input('title'));

            // If there's a new image file, process it
            if ($req->hasFile('image')) {
                error_log("has image");
                // Delete the old image if it exists
                if ($event->image) {
                    $imagePath = storage_path('app/public/' . $event->image);
                    if (file_exists($imagePath)) {
                        unlink($imagePath);
                    }
                }

                // Store the new image
                $file_path = $req->file('image')->store('events-and-announcement', 'public');
                $event->image = $file_path;
            }

            // Save the updated event
            $event->save();

            return response()->json(['message' => 'Event updated successfully'], 200);
        } catch (\Exception $e) {
            // Handle any exceptions
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }
}
