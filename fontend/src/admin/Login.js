import React, { useState } from 'react';
import './superadmin/css/login.css';
import { Link } from 'react-router-dom';
import { toast, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

function Login({ onLogin }) {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const handleSubmit = async (e) => {
    e.preventDefault();

    const toastId = toast.loading('Logging in...');

    try {
      const response = await fetch('https://sky-garden.ugyen1234.serv00.net/api/login', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          email,
          password,
        }),
      });

      if (response.ok) {
        const data = await response.json();
        localStorage.setItem('token', data.authorisation.token);
        localStorage.setItem('user', JSON.stringify(data.user)); // Convert user object to string
        onLogin();
        toast.update(toastId, { render: 'Login successful!', type: 'success', isLoading: false, autoClose: 3000 });
        window.location.reload();
      } else {
        const errorData = await response.json();
        toast.update(toastId, { render: errorData.message, type: 'error', isLoading: false, autoClose: 3000 });
      }
    } catch (error) {
      toast.update(toastId, { render: 'An error occurred. Please try again later.', type: 'error', isLoading: false, autoClose: 3000 });
    }
  };

  return (
    <div className="login-container">
      <ToastContainer />
      <div className='login-img'>
        <img src={'img/logo.jpg'} alt="Logo" style={{ width: '100%', height: '100%', objectFit: 'cover' }} />
      </div>
      <h2>Login</h2>
      <form onSubmit={handleSubmit}>
        <div className="form-group">
          <label htmlFor="email" className='text-align-content-lg-start'>Email</label>
          <input
            type="email"
            id="email"
            className="login-input"
            placeholder="Enter email address"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            required
          />
        </div>
        <div className="form-group">
          <label htmlFor="password" className='text-align-content-lg-start'>Password</label>
          <input
            type="password"
            id="password"
            className="login-input"
            placeholder="Enter password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            required
          />
        </div>
        <div className="form-group">
          <Link to="/forgot-password-email-&-opt" className="forgot-password">Forgot Password?</Link>
        </div>
        <button type="submit" className="login-btn">Login</button>
      </form>
    </div>
  );
}

export default Login;
