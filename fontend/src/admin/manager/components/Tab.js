import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { Tabs as MuiTabs, createTheme, ThemeProvider, Button } from '@mui/material'; // Import createTheme
import Tab from '@mui/material/Tab';
import RestaurantIcon from '@mui/icons-material/Restaurant';
import DeliveryDiningIcon from '@mui/icons-material/DeliveryDining';
import FoodGallary from './Content';

const theme = createTheme({ // Define the theme variable
  components: {
    MuiTabs: {
      styleOverrides: {
        indicator: {
          backgroundColor: '#F49B3F',
        },
      },
    },
  },
});

const CustomTabs = () => {
  const [data, setData] = useState([]);
  const [value, setValue] = useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const renderTabContent = (index) => {
    switch (index) {
      case 0:
        return <FoodGallary selectedData={data.filter(item => item.dinning_in === 1)} />;
      case 1:
        return <FoodGallary selectedData={data.filter(item => item.take_away === 1)} />;
      default:
        return null;
    }
  };


  // useEffect(() => {
  //   getData();
  // }, []);

  async function getData() {
    try {
      let response = await fetch("https://sky-garden.ugyen1234.serv00.net/api/categorylist");
      if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`);
      }
      let result = await response.json();
      setData(result);
    } catch (error) {
      console.error("Failed to fetch data ");
      return;
      // Handle error accordingly, e.g., set an error state
    }
  }

  useEffect(() => {
    getData();
  }, []);

  return (
    <div>
      <ThemeProvider theme={theme}>
        <div className='tab-container'>
          <MuiTabs
            value={value}
            onChange={handleChange}
            aria-label="icon label tabs example"
            indicatorColor="primary"
          >
            <Tab
              icon={<RestaurantIcon />}
              label="Dining In"
              sx={{
                '&.Mui-selected': {
                  color: '#00A0B1',
                },
              }}
            />

            <Tab
              icon={<DeliveryDiningIcon />}
              label="Take Away"
              sx={{
                '&.Mui-selected': {
                  color: '#00A0B1',
                },
              }}
            />
          </MuiTabs>

          <div style={{ display: 'flex', justifyContent: 'flex-end', paddingRight: '10px', marginRight: "100px", }}>
            <Button
              variant="outlined"
              sx={{
                color: 'orange',
                border: "1px solid orange",
                '&:hover': {
                  backgroundColor: 'orange',
                  color: 'white',
                  border: "1px solid orange",
                },
              }}
            >
              <Link to="/addfoodItem" style={{ textDecoration: 'none', color: 'inherit' }}>Add Food Item</Link>
            </Button>

            <div style={{ display: 'flex', justifyContent: 'flex-end', paddingRight: '10px', marginLeft: "20px" }}>
              <Button
                variant="outlined"
                sx={{
                  color: 'orange',
                  border: "1px solid orange",
                  '&:hover': {
                    backgroundColor: 'orange',
                    color: 'white',
                    border: "1px solid orange",
                  },
                }}
              >
                <Link to="/addfoodcategory" style={{ textDecoration: 'none', color: 'inherit' }}>Add Food Category</Link>
              </Button>
            </div>
          </div>

          <div>{renderTabContent(value)}</div>
        </div>
      </ThemeProvider>
    </div>
  );
}

export default CustomTabs;
