import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSquareWhatsapp } from "@fortawesome/free-brands-svg-icons";

function Whatsapp() {
    const whatsappLink = "https://wa.me/97517485966"; // Replace "yourphonenumber" with the actual phone number

    return (
        <a href={whatsappLink} target="_blank" rel="noopener noreferrer">
            <FontAwesomeIcon icon={faSquareWhatsapp} style={{ fontSize: "6vw", color: "#00A0B1" }} />
        </a>
    );
}

export default Whatsapp;
