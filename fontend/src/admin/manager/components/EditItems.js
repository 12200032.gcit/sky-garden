// Import necessary dependencies and components
import React, { useState, useRef, useEffect } from 'react';
import SideBar from '../SideBar';
import "../css/Form.css"
import { Link, useParams } from 'react-router-dom';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCloudArrowUp, faArrowLeft } from '@fortawesome/free-solid-svg-icons';
import Topbar from '../../Topbar';

// Functional component EditItems
function EditItems() {
  // State variables
  const { id } = useParams();
  const [foodCategory, setFoodCategory] = useState('');
  const [price, setPrice] = useState('');
  const [foodName, setFoodName] = useState('');
  const [description, setDescription] = useState('');
  const [selectedFile, setSelectedFile] = useState(null);
  const [previewURL, setPreviewURL] = useState(null);
  const [totalCategories, setTotalCategories] = useState([]);

  const fetchFoodItemsData = async () => {
    try {
      const response = await fetch(`https://sky-garden.ugyen1234.serv00.net/api/get_food_items/${id}`);
      if (!response.ok) {
        throw new Error('Failed to fetch data');
      }
      const eventData = await response.json();
      setFoodCategory(eventData.food_category);
      setPrice(eventData.price);
      setFoodName(eventData.food_name);
      setDescription(eventData.description);
      setSelectedFile(eventData.image);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    fetchFoodItemsData();
  }, []);

  // Ref for file input
  const fileInputRef = useRef(null);

  // Function to handle file change
  const handleFileChange = (event) => {
    const file = event.target.files[0];
    setSelectedFile(file);
    previewFile(file);
  };

  // Function to preview file
  const previewFile = (file) => {
    if (file) {
      const filePreviewURL = URL.createObjectURL(file);
      setPreviewURL(filePreviewURL);
    } else {
      setPreviewURL(null);
    }
  };

  // Function to handle drop
  const handleDrop = (event) => {
    event.preventDefault();
    const file = event.dataTransfer.files[0];
    setSelectedFile(file);
    previewFile(file);
  };

  // Function to handle form reset
  const handleReset = () => {
    setPreviewURL(null);
    fileInputRef.current.value = '';
    fetchFoodItemsData()
  };

  // Function to add food item
  const EditFoodItem = async (e) => {
    e.preventDefault();
    if (!selectedFile) {
      toast.error("Please select an image");
      return;
    }

    if (!description.trim()) {
      toast.error("Please enter a description");
      return;
    }

    if (!foodName.trim()) {
      toast.error("Please enter the name of the food");
      return;
    }

    const formData = new FormData();
    formData.append('food_category', foodCategory);
    formData.append('food_name', foodName);
    formData.append('price', price);
    formData.append('description', description);
    formData.append('image', selectedFile);

    try {
      let response = await fetch(`https://sky-garden.ugyen1234.serv00.net/api/edit_food_item/${id}`, {
        method: 'POST',
        body: formData,
      });

      if (response.ok) {
        toast.success("Food item has been edited successfully");
        handleReset();
      } else {
        throw new Error('Failed to edit Food Item');
      }
    } catch (error) {
      toast.error("Failed to save data. Please try again.");
    }
  };

  // Function to handle input change for price
  const handlePriceChange = (event) => {
    const inputValue = event.target.value;
    // Allow only numeric input
    const numericInput = inputValue.replace(/\D/g, '');
    setPrice(numericInput);
  };

  // Effect hook to fetch data
  useEffect(() => {
    getData();
  }, []);

  // Function to fetch data
  async function getData() {
    try {
      let response = await fetch("https://sky-garden.ugyen1234.serv00.net/api/getCategory");
      if (!response.ok) {
        throw new Error('Failed to fetch data');
      }
      let result = await response.json();
      setTotalCategories(result);
    } catch (error) {
      console.error('Error fetching data:', error);
      // Handle error, show error message or retry fetching data
    }
  }

  // JSX
  return (
    <>

      <div className='d-flex'>
        <SideBar />
        <div className='p-4' style={{ width: '93%' }}>
          <Topbar />
          <div className='border title_bar'>
            <p>Edit Food Item</p>
          </div>
          <form className="form" onSubmit={EditFoodItem}>
            <div className="row">
              <div className="col-md-8">
                <div className='d-flex'>
                  <div className='col-md-12' style={{ paddingRight: "15px", position: "relative" }}>
                    <label htmlFor="category" style={{ fontWeight: "600", color: "#555555" }}>Category</label>
                    <div style={{ position: "relative" }}>
                      <select
                        id="category"
                        value={foodCategory}
                        onChange={(e) => setFoodCategory(e.target.value)}
                        className="form-control"
                        required
                        style={{ marginTop: "20px" }}
                      >

                        {totalCategories.map((category, index) => (
                          <option key={index} value={category}>{category}</option>
                        ))}
                      </select>
                      <div style={{ position: "absolute", top: "50%", right: "10px", transform: "translateY(-50%)" }}>
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          width="16"
                          height="16"
                          fill="currentColor"
                          className="bi bi-chevron-down"
                          viewBox="0 0 16 16"
                        >
                          <path d="M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 1 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z" />
                        </svg>
                      </div>
                    </div>
                  </div>
                </div>
                <div className='d-flex'>
                  <div className='col-md-6' style={{ paddingRight: "15px" }}>
                    <label htmlFor="foodName">FoodName <span style={{ color: "red" }}>*</span></label>
                    <input
                      type="text"
                      id="foodName"
                      placeholder='Name of the food Item'
                      value={foodName}
                      onChange={(e) => setFoodName(e.target.value)}
                      className="form-control"
                      required
                    />
                  </div>
                  <div className='col-md-6' style={{ paddingRight: "15px" }}>
                    <label htmlFor="price">Price <span style={{ color: "red" }}>*</span></label>
                    <input
                      type="text"
                      id="price"
                      placeholder='Price of the Item'
                      value={price}
                      onChange={handlePriceChange}
                      className="form-control"
                      required
                    />
                  </div>
                </div>
                <label htmlFor="description">Description <span style={{ color: "red" }}>*</span></label>
                <textarea
                  id="description"
                  value={description}
                  placeholder='Write the description of the event'
                  onChange={(e) => setDescription(e.target.value)}
                  className="form-control"
                  required
                ></textarea>
              </div>
              <div className="col-md-4" style={{ paddingLeft: "70px", paddingBlock: "20px" }}>
                <label htmlFor="imageUpload">Upload Image <span style={{ color: "red" }}>*</span></label>
                <div
                  className="upload-area "
                  style={{ cursor: "pointer" }}
                  onDrop={handleDrop}
                  onDragOver={(event) => event.preventDefault()}
                  onClick={() => fileInputRef.current.click()}
                >
                  {previewURL ? (
                    <img src={previewURL} alt="Preview" className="preview-image" />
                  ) : (
                    selectedFile && (
                      <img
                        src={`https://sky-garden.ugyen1234.serv00.net/storage/${selectedFile}`}
                        alt="Initial Preview"
                        className="preview-image"
                      />
                    )
                  )}
                  <input
                    type="file"
                    id="imageUpload"
                    ref={fileInputRef}
                    accept="image/*"
                    onChange={handleFileChange}
                    style={{ display: 'none' }}
                  />
                </div>
              </div>
            </div>
            <div className="row mt-4">
              <div className="col-md-10">
                <Link to="/food" style={{ textDecorationLine: "none" }}>
                  <span className='go-back'>
                    <FontAwesomeIcon className='icon' icon={faArrowLeft} />
                    Go back
                  </span>
                </Link>
              </div>
              <div className="col-md-2 d-flex justify-content-between">
                <button className="btn reset" type='button' onClick={handleReset}>Reset</button>
                <button className="btn add" type='submit' >Save</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </>
  );
}

export default EditItems;
