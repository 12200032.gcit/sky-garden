import React from "react";
import "../styles/mark.css";

function Mark() {
    return (
        <div>
            <div className="daily-container d-flex">
                <div className="blue-mark"></div>
                <div className="mark-daily-title">
                    <p>Menu Item</p>
                </div>
                <div className="pattern-img">
                    <img src="/img/pattern.png" alt="a logo" className="pattern" />
                </div>
            </div>
        </div>
    );
}

export default Mark;
