<?php

use App\Http\Controllers\EventsController;
use Illuminate\Support\Facades\Route;

Route::get('/events',[EventsController::class,'events']);

Route::get('/', function () {
    return view('welcome');
});
